#!/bin/bash

# Basic run of the algorithm

#output filename
d=`date '+%d%m_%H%M%S'`
outputfile="ee_"$d".txt" 

#reinitialisation du fichier avec les entêtes des colonnes
echo "Method len ql ws it fmin time MaxRSS" > $outputfile

len=100

src=../code/build/src/
outdir="./output_"$d

mkdir $outdir

for ql in 30 # 20 10
do
	for ws in 60 #70 80 90
	do
		# Nombre d'iteration :
		for it in 1 2 3 4 5
		do
	        # Generation d'un jeu de donnees au format IBM :
	        ${src}/Comparator/generator -l $len -n $ql -b -p 0.03 -o $outdir/we_cmp_${it}.seq
	        
			#Lancement de l'algorithme avec différents paramètres sur le jeu de données
			for f in 4 # 5 7  #6 8 10 12 18 10 15
			do
				echo $len $ql $ws $it $f
				# l'option -n évite le saut de ligne en fin (plus facile a gérer ensuite dans une feuille excel !
				echo -n SP $len $ql $ws $it $f >> $outputfile
				/usr/bin/time -a -o $outputfile -f " %U %M" ${src}/SeqStreamMiner/seqstream -w $ws -f $f -o $outdir/history_${it}.out -ibm $outdir/we_cmp_${it}.seq
			done
		done
	done
done


