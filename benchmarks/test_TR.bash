#!/bin/bash

# test de comparaison de l'algorithme
# la taille de la série temporelle est la même que la taille de la séquence (on ne fait que de l'incrémental, sans suppression !)

# Fonction permettant de tuer récursivement tous les processus d'un processus parent
# cette fonction est necessaire dans la mesure où on lance un processus "time" qui est parent du processus
# de la tâche !!
killtree() {
    local _pid=$1
    local _sig=${2-TERM}
    kill -stop ${_pid} # needed to stop quickly forking parent from producing child between child killing and parent killing
    for _child in $(ps -o pid --no-headers --ppid ${_pid}); do
        killtree ${_child} ${_sig}
    done
    kill -${_sig} ${_pid}
}

# Fonction permettant de tester si un processus (dont on passe la PID en paramètre)
# fonctionne depuis (trop) longtemps, 2 minutes par défaut) ou en troisième paramètres
# optionnel (nombre de minutes).
# Si le processus tâche dépasse le temps limite, alors il est tué et le fichier
# (second argument) est complété
function try_shutdown() {
	local maxmin=2
	if [ $# == 3 ]; then
		maxmin=$3
	fi
	local end_time=$(( $(date '+%s') + (60*$maxmin) ))
	local tokill=1
	while (( $(date '+%s') < end_time )); do
		ps|grep $1 >/dev/null
	  	if [ $? != 0 ]; then
			tokill=0
			break
	  	fi
	  sleep 1
	done

	# okay, we timed out; stop the background process that's trying to shut down nicely
	if [ $tokill == 1 ]; then
		echo "too long : killed"
		echo " 0 0 killed" >> $2
		killtree $1
	fi
}


#Formatage du nom de la sortie avec des éléments de date et d'heure (unique !)
d=`date '+%d%m_%H%M%S'`
outputfile="ee_"$d".txt" 

#reinitialisation du fichier avec les entêtes des colonnes
echo "Method len ql ws it fmin time MaxRSS" > $outputfile

len=1000

for ql in 30 # 20 10
do
	# Generation d'un jeu de donnees au format IBM :
	./build/src/Comparator/generator -l $len -n $ql -b -p 0.03 -o we_cmp.seq

	for ws in 60 70 80 90
	do
		# Nombre d'iteration :
		for it in 1 #2 3 4 5
		do
			

			#Lancement de l'algorithme avec différents paramètres sur le jeu de données
			for f in 4 # 5 7  #6 8 10 12 18 10 15
			do
				echo $len $ql $ws $it $f
				# l'option -n évite le saut de ligne en fin (plus facile a gérer ensuite dans une feuille excel !
				echo -n SP $len $ql $ws $it $f >> $outputfile
				/usr/bin/time -a -o $outputfile -f " %U %M" ./build/src/SeqStreamMiner/seqstream -w $ws -f $f -ibm we_cmp.seq &
				child_pid=$!
				try_shutdown $child_pid $outputfile 5
				
				echo -n SPTR $len $ql $ws $it $f >> $outputfile
				/usr/bin/time -a -o $outputfile -f " %U %M" ./build_TR/src/SeqStreamMiner/seqstream -w $ws -f $f -ibm we_cmp.seq &
				child_pid=$!
				try_shutdown $child_pid $outputfile 5
			done
		done
	done
done


#a_program $file &

#while [ $(ps -p $! -o etime= | tr -d ": ") -lt 10000 ]
#do
#  sleep 60
#done 2> /dev/null

#kill -s SIGHUP $!
