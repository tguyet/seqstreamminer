#!/bin/bash

# test de comparaison de l'algorithme avec MINEPI (Mannila, Toivonen et Verkamo)
# la taille de la série temporelle est la même que la taille de la séquence (on ne fait que de l'incrémental, sans suppression !)


#Formatage du nom de la sortie avec des éléments de date et d'heure (unique !)
d=`date '+%d%m_%H%M%S'`
outputfile="test_OMP_"$d".txt" 

#reinitialisation du fichier avec les entêtes des colonnes
echo "Method len ql ws it fmin time MaxRSS" > $outputfile

len=5000

for ws in 700
do
	for ql in 30 20 10
	do
		# Nombre d'iteration :
		for it in 1 #2 3 4 5
		do
			# Generation d'un jeu de donnees au format IBM :
			./build/src/Comparator/generator -l $len -n $ql -b -p 0.03 -o we_cmp.seq

			#Lancement de l'algorithme avec différents paramètres sur le jeu de données
			for f in 15  #6 8 10 12 18 10 15 20
			do
				for nbt in 2 4 8 12
				do
					export OMP_NUM_THREADS=$nbt
					# l'option -n évite le saut de ligne en fin (plus facile a gérer ensuite dans une feuille excel !
					echo -n $len $ql $ws $it $f $nbt >> $outputfile
					/usr/bin/time -a -o $outputfile -f " %U %M" ./build_OMP/src/SeqStreamMiner/seqstream -w $ws -f $f -ibm we_cmp.seq
				done
			done
		done
	done
done


#a_program $file &

#while [ $(ps -p $! -o etime= | tr -d ": ") -lt 10000 ]
#do
#  sleep 60
#done 2> /dev/null

#kill -s SIGHUP $!
