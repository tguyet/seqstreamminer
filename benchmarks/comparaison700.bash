#!/bin/bash

# test de comparaison de l'algorithme avec MINEPI (Mannila, Toivonen et Verkamo)
# la taille de la série temporelle est la même que la taille de la séquence (on ne fait que de l'incrémental, sans suppression !)


#Formatage du nom de la sortie avec des éléments de date et d'heure (unique !)
d=`date '+%d%m_%H%M%S'`
outputfile="ee_"$d".txt" 

#reinitialisation du fichier avec les entêtes des colonnes
echo "Method len ql ws it fmin time MaxRSS" > $outputfile

len=1000 #5000

generator=../code/build/src/Comparator/generator
binpath=../code/build/src/SeqStreamMiner

for ws in 50 100 200 400 800
do
	len=$((500 + $ws))
	for ql in 30 20 10
	do
		# Nombre d'iteration :
		for it in 1 2 3 4 5
		do
			# Generation d'un jeu de donnees au format IBM :
			${generator} -l $len -n $ql -b -p 0.03 -o we_cmp.seq

			#Lancement de l'algorithme avec différents paramètres sur le jeu de données
			for f in 6 8 10 12 18 10 15
			do
				# l'option -n évite le saut de ligne en fin (plus facile a gérer ensuite dans une feuille excel !
				echo -n SP $len $ql $ws $it $f >> $outputfile
				/usr/bin/time -a -o $outputfile -f " %U %M" ${binpath}/seqstream -o History_Inc.txt -w $ws -f $f -ibm we_cmp.seq
				
				echo -n NSP $len $ql $ws $it $f >> $outputfile
				/usr/bin/time -a -o $outputfile -f " %U %M" ${binpath}/seqstream -o History_NoInc.txt -sm -w $ws -f $f -ibm we_cmp.seq
			done
		done
	done
done


#a_program $file &

#while [ $(ps -p $! -o etime= | tr -d ": ") -lt 10000 ]
#do
#  sleep 60
#done 2> /dev/null

#kill -s SIGHUP $!
