/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ agrocampus-ouest fr (remove space and add dots)
 */

/**
 * \file config.h
 * \author Guyet Thomas, AGROCAMPUS-OUEST
 * \brief Default version configuration
 * \date 23 octobre 2012
 */


#define VERSION "@PROJECT_VERSION@"

/**
 * \def TIME_PROBE Profiling activation
 * If 1, it activates the tracking of windows processing time and size (per batch for PSStream of per slide for SeqStream)
 */
#ifndef TIME_PROBE
#define TIME_PROBE 0
#endif


/**
 * \def QFREQ Quasi-frequent activation
 * If defined, it activates the quasi-frequent optimisation
 */
#ifndef QFREQ
#define QFREQ 0
#endif

/**
 * \def TREEREDUCE Tree reduction activation
 * If defined to 1, it activates the reduction of the tree to merge into the main tree
 */
#ifndef TREEREDUCE
#define TREEREDUCE 0
#endif

/**
 * \def ORDERNODES
 * If defined to 1, it activates the use of order on nodes lists (succession and compostion)
 */
#ifndef ORDERNODES
#define ORDERNODES 0
#endif

/**
 * \def ONEPASS
 * If define to 1, it activates the one pass sequence mining version.
 * <b>This version is not compatible with parallelization.</b>
 */
#ifndef ONEPASS
#define ONEPASS 0
#if ONEPASS
	 #undef _OPENMP
#endif
#endif

