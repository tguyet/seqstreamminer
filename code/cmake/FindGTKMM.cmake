# This module finds cegui-Ogre 
# and determines where the
# include files and libraries are. 
# On Unix/Linux it relies on the output of pkg-config.
##
# This code sets the following variables:
##
# GTKMM_FOUND       = system has cegui-Ogre lib
##
# GTKMM_INCFLAGS   = Cflags used on gcc
##
# GTKMM_INC      = where to find headers 
##
# GTKMM_LIBS     = libs used by cegui-Ogre
##
# GTKMM_LIBFLAGS   = ldflags used on gcc
##
# GTKMM_LIBDIR   = full path(S) to the libraries
##
# Xavier Larrod� 2006
# xlarrode@irisa.fr 
##


IF(WIN32)
  # where can we possibly look to find ogre root path?
  SET(GTKMM_POSSIBLE_ROOT_PATHS
    ${GTKMM_BASEPATH}
    $ENV{GTKMM_BASEPATH}
    "$ENV{ProgramFiles}Gtkmm"
    "C:/GTKMM"
  )
  #MESSAGE("DBG GTKMM_POSSIBLE_ROOT_PATHS= ${GTKMM_POSSIBLE_ROOT_PATHS}")
  
  
  # select just one tree here to avoid mixing different versions: 
  FIND_PATH(GTKMM_ROOT_DIR
    include/gtkmm-3.0/gtkmm.h
    ${GTKMM_POSSIBLE_ROOT_PATHS}
  )
 SET(GTK_POSSIBLE_ROOT_PATHS
    ${GTK_BASEPATH}
    $ENV{GTK_BASEPATH}
    "$ENV{ProgramFiles}Gtk"
    "C:/GTK"
  )
  #MESSAGE("DBG GTKMM_POSSIBLE_ROOT_PATHS= ${GTKMM_POSSIBLE_ROOT_PATHS}")
  
  
  # select just one tree here to avoid mixing different versions: 
  FIND_PATH(GTK_ROOT_DIR
    include/gtk-3.0/gtk/gtk.h
    ${GTK_POSSIBLE_ROOT_PATHS}
  )
 #MESSAGE("DBG GTKMM_ROOT_DIR= ${GTKMM_ROOT_DIR}")
  
  # if found
  IF(GTKMM_ROOT_DIR AND GTK_ROOT_DIR) 
    #SET(GTKMM_DEFINITIONS... )
    SET(GTKMM_INC 
      "${GTKMM_ROOT_DIR}/include/gtkmm-3.0"
      "${GTKMM_ROOT_DIR}/lib/gtkmm-3.0/include"
      "${GTKMM_ROOT_DIR}/include/gdkmm-3.0"
      "${GTKMM_ROOT_DIR}/lib/gdkmm-3.0/include"
      "${GTKMM_ROOT_DIR}/include/glibmm-3.0"
      "${GTKMM_ROOT_DIR}/lib/glibmm-3.0/include"
      "${GTKMM_ROOT_DIR}/include/libglademm-3.0"
      "${GTKMM_ROOT_DIR}/lib/libglademm-3.0/include"
      "${GTKMM_ROOT_DIR}/include/sigc++-2.0"
      "${GTKMM_ROOT_DIR}/lib/sigc++-2.0/include"
      
      "${GTKMM_ROOT_DIR}/include/atkmm-1.6"
      "${GTKMM_ROOT_DIR}/include/cairomm-1.0"
      "${GTKMM_ROOT_DIR}/include/pangomm-1.4"
      #GTK
                 
      "${GTK_ROOT_DIR}/include/gtk-3.0"
      "${GTK_ROOT_DIR}/lib/gtk-3.0/include"
      "${GTK_ROOT_DIR}/include/glib-3.0"
      "${GTK_ROOT_DIR}/lib/glib-3.0/include"

      "${GTK_ROOT_DIR}/include/cairo"
      "${GTK_ROOT_DIR}/include/atk-1.0"
      "${GTK_ROOT_DIR}/include/freetype2"
      "${GTK_ROOT_DIR}/include/pango-1.0"
      "${GTK_ROOT_DIR}/include/libxml2"
      
      CACHE PATH "Gtkmm include dir(s)")
    
    SET(GTKMM_LIBS 
      gtkmm-3.0
      gdkmm-3.0
      atkmm-1.6  
      pangomm-1.4 
      glibmm-2.4 
      atk-1.0 
      gdk_pixbuf-2.0 
      pango-1.0 
      gobject-2.0 
      gmodule-2.0  
      glib-2.0 
      sigc-2.0
      gdk-win32-2.0
      )
    
    SET(GTKMM_LIBDIR 
      "${GTKMM_ROOT_DIR}/lib" 
      "${GTK_ROOT_DIR}/lib" 
      CACHE PATH "Ogre link directories")
    
    ADD_DEFINITIONS(${GTKMM_DEFINITIONS})
    
    SET (GTKMM_FOUND ON)
    
  ENDIF(GTKMM_ROOT_DIR AND GTK_ROOT_DIR)
  
ENDIF(WIN32)


# --------------------------------------------------------------

IF(UNIX) 
  INCLUDE(UsePkgConfig)
  PKGCONFIG("gtkmm-3.0"
    GTKMM_INCLUDE_DIR GTKMM_LIB_DIR GTKMM_LDFLAGS GTKMM_CFLAGS) 
  IF(GTKMM_INCLUDE_DIR)
    SET(GTKMM_LIBDIR_EXTRACT_REGEX "[-][l]([a-zA-Z0-9/._+-]*)")
    SET(GTKMM_LIBS_EXTRACT_REGEX "[-][L]([ a-zA-Z0-9/._+-]*)")

    #add new delimiters % and handle -I /titi and -L /titi 
    STRING(REGEX REPLACE "-L " "-L" GTKMM_LDFLAGS ${GTKMM_LDFLAGS})
    STRING(REGEX REPLACE " " "%" GTKMM_LDFLAGS ${GTKMM_LDFLAGS})
    STRING(REGEX REPLACE "-I " "-I" GTKMM_CFLAGS ${GTKMM_CFLAGS})
    STRING(REGEX REPLACE " " "%" GTKMM_CFLAGS ${GTKMM_CFLAGS})
    #Get GTKMM_LINK Property : LIBFLAGS LIBDIR LIBS 
    ##  LDFLAGS_MINUS_LIB  :   erase -l*
    STRING(REGEX REPLACE "${GTKMM_LIBDIR_EXTRACT_REGEX}" ""
      GTKMM_LDFLAGS_MINUS_LIB "${GTKMM_LDFLAGS}")
    ### GTKMM_LIBFLAGS = GTKMM_LDFLAGS_MINUS_LIB MINUS LIBDIR -space
    #erase -L and space
    STRING(REGEX REPLACE "${GTKMM_LIBS_EXTRACT_REGEX}" ""
      GTKMM_LIBFLAGS "${GTKMM_LDFLAGS_MINUS_LIB}")
    STRING(REGEX REPLACE "%" "" GTKMM_LIBFLAGS "${GTKMM_LIBFLAGS}")
    #Handle if there is no libFlags
    IF(GTKMM_LIBFLAGS)
    ELSE(GTKMM_LIBFLAGS)
      SET(GTKMM_LIBFLAGS " ")
    ENDIF(GTKMM_LIBFLAGS)
    ### GTKMM_LIBDIR = GTKMM_LDFLAGS_MINUS_LIB - GTKMM_LIBFLAGS -space
    #erase libFlags - space -L
    STRING(REGEX REPLACE "${GTKMM_LIBFLAGS}" ""
      GTKMM_LIBDIR "${GTKMM_LDFLAGS_MINUS_LIB}")
    STRING(REGEX REPLACE "%" "" GTKMM_LIBDIR "${GTKMM_LIBDIR}")
    STRING(REGEX REPLACE "-L" ";" GTKMM_LIBDIR "${GTKMM_LIBDIR}")
    #add PATH done by pkgconfig
    SET(GTKMM_LIBDIR 
      ${GTKMM_LIBDIR}
      ${GTKMM_LIB_DIR})

    ### GTKMM_LIBS = GTKMM_LDFLAGS - LIBS -libFlags -space -l 
    
    STRING(REGEX REPLACE "${GTKMM_LIBS_EXTRACT_REGEX}" ""
      GTKMM_LIBS "${GTKMM_LDFLAGS}")
    STRING(REGEX REPLACE "${GTKMM_LIBFLAGS}" ""
      GTKMM_LIBS "${GTKMM_LIBS}")
    STRING(REGEX REPLACE "%" "" GTKMM_LIBS "${GTKMM_LIBS}")
    STRING(REGEX REPLACE "-l" ";" GTKMM_LIBS "${GTKMM_LIBS}")

    SET(GTKMM_INCDIR_EXTRACT_REGEX "[-][I]([ a-zA-Z0-9/._+-]*)")
    #extract flags and include dir
    IF("${GTKMM_CFLAGS}" MATCHES "${GTKMM_INCDIR_EXTRACT_REGEX}")
      STRING(REGEX REPLACE "${GTKMM_INCDIR_EXTRACT_REGEX}" ""
        GTKMM_INCFLAGS "${GTKMM_CFLAGS}")
      STRING(REGEX MATCHALL "${GTKMM_INCDIR_EXTRACT_REGEX}"
        GTKMM_INC "${GTKMM_CFLAGS}")
      #carefull space make strnge \
      STRING(REGEX REPLACE "%" " " GTKMM_INC "${GTKMM_INC}")
      STRING(REGEX REPLACE "-I" "" GTKMM_INC "${GTKMM_INC}")
      STRING(REGEX REPLACE "%" " " GTKMM_INCFLAGS "${GTKMM_INCFLAGS}")
    #add PATH done by pkgconfig
    SET(GTKMM_INC 
      ${GTKMM_INC}
      ${GTKMM_INCLUDE_DIR})

    ENDIF("${GTKMM_CFLAGS}" MATCHES "${GTKMM_INCDIR_EXTRACT_REGEX}")
  ENDIF(GTKMM_INCLUDE_DIR)

  IF(GTKMM_INC AND GTKMM_LIBS AND GTKMM_LIBDIR)
    SET(GTKMM_FOUND ON)
  ELSE(GTKMM_INC AND GTKMM_LIBS AND GTKMM_LIBDIR)
    MESSAGE("Error Parsing pkgf-config for Ogre,")
    MESSAGE("Got  GTKMM_INC : ${GTKMM_INC}") 
    MESSAGE("Got  GTKMM_LIBS : ${GTKMM_LIBS}")
    MESSAGE("Got  GTKMM_LIBFLAGS : ${GTKMM_LIBFLAGS}") 
    MESSAGE("Got  GTKMM_LIBDIR : ${GTKMM_LIBDIR}") 
  ENDIF(GTKMM_INC AND GTKMM_LIBS AND GTKMM_LIBDIR)


ENDIF(UNIX)
MARK_AS_ADVANCED(
  GTKMM_FOUND
  GTKMM_INCFLAGS
  GTKMM_INC
  GTKMM_LIBS
  GTKMM_LIBDIR
  GTKMM_LIBFLAGS
  )

