/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file stream.cpp
 * \author Guyet Thomas, AGROCAMPUS-OUEST/IRISA
 * \version 0.3
 * \date 25 nov 2021
 */

#include <iostream>
#include "stream.h"

using namespace std;

int itemset::operator==(const itemset & is) const
{
	if( size() != is.size()) return 0;

	list<int>::const_iterator it=begin();
	list<int>::const_iterator it2=is.begin();
	while(it!=end()) {
		if( *it != *it2 ) return 0;
		it++;
		it2++;
	}
	return 1;
}

int itemset::operator!=(const itemset & is) const {
	return !operator==(is);
}

/*
 * TODO : si les items étaient ordonnés, on pourrait s'arrêter plus tôt !!!
 * NB: seqstream::add(itemset &s) ordonne ses itemsets !
 */
bool itemset::holds(const int i) const
{
	for(const_iterator itis=begin(); itis!=end(); itis++) {
		if( (*itis)==i ) {
			return true;
		}
	}
	return false;
}

bool itemset::holds(const itemset is) const
{
	if(is.size()>size()) return false;

	const_iterator itis=begin();
	for(const_iterator jtis=is.begin(); jtis!=is.end(); jtis++) {
		//ICI : les (jtis-1) premiers items de l'itemset ont été trouvés, on cherche le (jtis)-ieme à partir de l'emplacement itis

		while( (*jtis)!=(*itis) && itis!=end() ) {
			itis++;
		}
		if( (*jtis)!=(*itis) ) {
			return false;
		} else {
			itis++;
		}
		//ICI (*jtis)==(*itis) : on a trouvé le (jtis)-ieme élément de is !
	}
	return true;
}

/**
 * \brief Global function required to overload the itemset streaming operator
 * \date 17/07/2012 change print out format to make history loading easier
 */
ostream& operator<< (ostream & os, const itemset & is)
{
	list<int>::const_iterator it=is.begin();
	if( it!=is.end() ) {
		os << *it;
		it++;
	}
	while(it!=is.end()) {
		os << "-" << *it;
		it++;
	}
	return os;
}

void stream::print() const
{
	list<itemset>::const_iterator it=begin();
	cout << "Stream: ";
	while( it!=end() ) {
		std::cout << (*it) << ", ";
		it++;
	}
	cout <<endl;
}


ostream& operator<< (ostream & os, const stream & s)
{
	list<itemset>::const_iterator it=s.begin();
	int i=1;
	while( it!=s.end() ) {
		os << "1 " << i <<" " << (*it).size() << " ";
		list<int>::const_iterator itit=(*it).begin();
		while( itit!=(*it).end() ) {
			os << (*itit) << " ";
			itit++;
		}
		os <<endl;
		i++;
		it++;
	}
	os <<endl;
	return os;
}

/**
 * \def STATE_ITEMSET First bit to encode Itemset vs sequence state
 * \def STATE_SEQUENCE First bit to encode Itemset vs sequence state
 * \def STATE_COMMENT Second bit to encode comment state
 */
#define STATE_ITEMSET 0
#define STATE_SEQUENCE 1
#define STATE_COMMENT 2

int stream::read(istream &in)
{
	char c;
	int state, val;
	itemset is;

	state = STATE_SEQUENCE;

	if( in.bad() ) {
		cerr << "stream::read : input not ready to read\n";
		return 0;
	}

	c = in.get();
	while (in.good()) { // loop while extraction from file is possible

		if( (state & STATE_COMMENT) ) { // Omit comments
			if( c =='\n' ) {
				state = state & !STATE_COMMENT;
			}
			c = in.get();
			continue;
		}

		switch(c) {
		case ')':
			if( state != STATE_ITEMSET ) {
				cerr << "stream::read : syntax error (1)\n";
				clear();
				return 0;
			} else {
				state = STATE_SEQUENCE;
				push_back(is);
				is.clear();
			}
			break;
		case '(':
			if( state != STATE_SEQUENCE) {
				cerr << "stream::read : syntax error (2)\n";
				clear();
				return 0;
			} else {
				state = STATE_ITEMSET;
				is.clear();
			}
			break;
		case '\n':
		case '\t':
		case ' ':
			// continue
			break;
		case '#':
			state = state | STATE_COMMENT;
			break;
		default:
			if( _symbols.find(c)==_symbols.end() ) {
				val=(int)_symbols.size() + 1;
				_symbols[c]=val;
			} else {
				val=_symbols[c];
			}
			//is is the current itemset
			is.push_back( val );

			if( state == STATE_SEQUENCE ) {
				//singleton
				push_back(is);
				is.clear();
			} else if( state == STATE_ITEMSET ) {
				//nothing !
			}
			break;
		}
		// get next character from file
		c = in.get();
	}
	return 1;
}


int stream::read_ibm(istream &in)
{
	int seq;
	int timestamp;
	int nbitems;
	int i;

	if( in.bad() ) {
		cerr << "stream::read : input not ready to read\n";
		return 0;
	}

	while (in.good()) { // loop while extraction from file is possible
		in >> seq;
		if( in.eof() ) {
			break;
		}
		if(in.fail() ) {
			std::cerr << "stream::read_ibm reading error, invalid sequence" << std::endl;
			return false;
		}
		if(seq!=1) {
			std::cerr << "Warning : IBM dataset with several sequences. Only the first sequence has been considered\n" << std::endl;
			sort();
			return true;
		}

		in >> timestamp;
		if(in.fail() || in.eof()) {
			std::cerr << "stream::read_ibm reading error, invalid timestamp" << std::endl;
			return false;
		}

		itemset I;
		I.set_timestamp(timestamp);

		in >> nbitems;
		if(in.fail() || in.eof()) {
			std::cerr << "stream::read_ibm reading error, invalid nb of items" << std::endl;
			return false;
		}

		//Lecture des items
		for(i=0;i<nbitems;i++) {
			int item;
			in >> item;
			if(in.fail() || in.eof()) {
				std::cerr << "stream::read_ibm reading error, invalid item" << std::endl;
				return false;
			}
			I.push_back(item);
		}

		push_back(I);
	}
	sort();
	return 1;
}

