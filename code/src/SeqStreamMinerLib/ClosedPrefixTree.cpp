/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove spaces and add dots)
 */


/**
 * \file ClosedPrefixTree.cpp
 * \author Guyet Thomas, Inria
 * \version 0.1
 * \date 25 nov 2021
 */

#include "ClosedPrefixTree.h"
#include "debug.h"



/*====================== POSITIONS ========================*/


int ClosedPrefixTree::position::operator==(const position &p) const {
	return _min_pos==p.min_pos() && _max_pos==p.max_pos();
}

int ClosedPrefixTree::position::operator==(position &p) {
	return _min_pos==p.min_pos() && _max_pos==p.max_pos();
}

int ClosedPrefixTree::position::operator<(const position &p) const {
	if( _min_pos==p.min_pos() ) {
		return _max_pos<p.max_pos();
	} else {
		return _min_pos<p.min_pos();
	}
}

int ClosedPrefixTree::position::operator<(position &p) {
	if( _min_pos==p.min_pos() ) {
		return _max_pos<p.max_pos();
	} else {
		return _min_pos<p.min_pos();
	}
}

void ClosedPrefixTree::position::full_print(ostream & os) const {
	if( _prefix!=NULL ) {
		_prefix->full_print(os);
		os<<"," << max_pos();
	} else {
		os<<min_pos();
	}
}

ostream& operator<< (ostream & os, const ClosedPrefixTree::position & is)
{
	os << "(";
	is.full_print(os);
	os << ")";
	return os;
}


/*====================== NODES ========================*/

ClosedPrefixTree::node::node(const node *n): _s(n->_s), _parent(n->_parent), _doclose(false), _tocomplete(n->_tocomplete), _pattern(n->_pattern)
{
	list<node *>::const_iterator it = n->_Childs.begin();
	while( it!= n->_Childs.end() ) {
		append( new node(*it) );
		it++;
	}
	//recopie de la liste des positions
	_lpos=n->_lpos;
}

ClosedPrefixTree::node::~node()
{
	list<node *>::iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		delete( *it );
		it++;
	}
}

void ClosedPrefixTree::node::print_pattern(std::ostream & of) const {
	list<itemset>::const_iterator it=_pattern.begin();
	while( it!=_pattern.end() ) {
		of << *it;
		it++;
	}
}


void ClosedPrefixTree::node::print(int pos, std::ostream & of) const {
	for(int i=0; i<pos; i++) of << " ";
	list<itemset>::const_iterator itpat;
	of << "[";
	print_pattern(of);
	of <<"] ("<< _Childs.size() << "): {";
	list<position>::const_iterator itp=_lpos.begin();
	while(itp!=_lpos.end()) {
		of << (*itp);
		of << ", ";
		itp++;
	}
	of << "}";
	if( _tocomplete ) of << " *";
	of << endl;

	pos++;
	list<node *>::const_iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		of << "f";(*it)->print(pos,of);
		it++;
	}
	pos = pos - 1;
}

void ClosedPrefixTree::node::print_instances(std::ostream & of) const {
	list<itemset>::const_iterator itpat;
	of << "[";
	print_pattern(of);
	of << "]" << endl;

	list<position>::const_iterator itp=_lpos.begin();
	while(itp!=_lpos.end()) {
		of << (*itp) << endl;
		itp++;
	}

	list<node *>::const_iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		(*it)->print_instances(of);
		it++;
	}
}

void ClosedPrefixTree::node::prune(unsigned int fmin) {
	list<ClosedPrefixTree::node *>::iterator it = _Childs.begin();
	while( it != _Childs.end() ) {
		if( (*it)->_lpos.size() < fmin ) {
			ClosedPrefixTree::node *todelete = *it;
			it = _Childs.erase( it );
			delete( todelete );
		} else {
			(*it)->prune(fmin);
			it++;
		}
	}
}

void ClosedPrefixTree::node::nodes(list<node *> &N)
{
	N.push_back(this);
	list<node *>::const_iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		(*it)->nodes(N);
		it++;
	}
}

void ClosedPrefixTree::node::clean()
{
	list<node *>::const_iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		delete(*it);
		it++;
	}
	_Childs.clear();
	_lpos.clear();
	_tocomplete=false;
}


int ClosedPrefixTree::node::size() const {
	int size=_Childs.size();
	if( size==0) {
		//It's a leaf
		return 0;
	} else {
		//C'est un noeud normal
		list<node *>::const_iterator it = _Childs.begin();
		while( it!= _Childs.end() ) {
			size+=(*it)->size();
			it++;
		}
		return size;
	}
}

int ClosedPrefixTree::node::leaves() const {
	if( _Childs.size()==0 ) {
		//It's a leaf
		return 1;
	} else {
		//C'est un noeud normal
		int nb=0;
		list<node *>::const_iterator it = _Childs.begin();
		while( it!= _Childs.end() ) {
			nb+=(*it)->leaves();
			it++;
		}
		return nb;
	}
}


int ClosedPrefixTree::node::maxdepth() const{
	if( _Childs.size()==0 ) {
		//It's a leaf
		return 0;
	} else {
		//C'est un noeud normal
		int maxdepth=0;
		list<node *>::const_iterator it = _Childs.begin();
		while( it!= _Childs.end() ) {
			int nodedepth=(*it)->maxdepth();
			maxdepth=(nodedepth>maxdepth?nodedepth:maxdepth);
			it++;
		}
		return maxdepth+1;
	}
}


void ClosedPrefixTree::node::append(node *n) {
	n->set_parent(this);
	n->_pattern = _pattern;
	n->_pattern.push_back( itemset(n->_s) );
	_Childs.push_back(n);
}


void ClosedPrefixTree::node::prefix(const ClosedPrefixTree::position *prefix)
{
#if DEBUG > 4
	cout << "prefix : "<<prefix << endl;
#endif

	if( !_lpos.empty() ) {
		list<position> newpos;
		list<position>::iterator itp= _lpos.begin();
		while( itp != _lpos.end() ) {
			position p=(*itp);
			p.set_prefix( prefix );
			p.set_min_pos( prefix->min_pos() );
			newpos.push_back( p );
			itp++;
		}
		_lpos=newpos;
	}

	//Recursion
	list<ClosedPrefixTree::node *>::iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		(*it)->prefix( prefix );
		it++;
	}
}

/**
 * Modification du merge pour les clos :
 * <ul>
 * <li>On met _doclose à vrai si le nombre</li>
 * </ul>
 */
void ClosedPrefixTree::node::merge(ClosedPrefixTree::node *T, unsigned int gap_max)
{
	assert( T->_s==-1 || _s==T->_s );

#if DEBUG > 4
	cout << "\t MERGE "; print_pattern(); cout << endl;
#endif

	if( T->_s!=-1 ) { // Si ce n'est pas un noeud racine, alors
		//Fusion des positions du noeud courant (sans répétition)
		list<ClosedPrefixTree::position>::iterator itp2;
		list<ClosedPrefixTree::position> toAdd; //Liste des éléments de T._lpos à ajouter à _lpos : uniquement ceux pour lesquelles il n'existe aucune exemple qui a le même préfixe de taille size()-1.
		itp2= T->_lpos.begin(); //itp2 est une position à ajouter dans la liste
		while( itp2 != T->_lpos.end() ) {
			if( _pattern.size()>=2 ) {
				// Dans le cas des motifs de taille >=2, il ne faut pas ajouter une
				// instance qui briserait la condition 3
				// => la première condition ci-dessous (pas triviale) fonctionne dans le cas
				// des "motifs doublons" !!
				// => la seconde condition correspond à l'ajout d'une contrainte de gap maximum entre
				// les itemsets de l'instance

				if( _lpos.back().max_pos() <= (*itp2).min_pos() && (gap_max==0 || (*itp2).max_pos() <= (*itp2).min_pos()+(int)gap_max)) {
					toAdd.push_back(*itp2);
				}
#if DEBUG > 3
				else {
					print_pattern();
					cout << " : NOT MERGED ";
					cout << "at pos " << _lpos.back() << " try " << (*itp2);
					cout << endl;
				}
#endif
			} else {
				// => contrainte de gap maximum entre
				// les itemsets de l'instance
				if( gap_max==0 || (*itp2).max_pos() <= (*itp2).min_pos()+(int)gap_max ) {
					toAdd.push_back(*itp2);
				}
			}
			itp2++;
		}
		_lpos.merge(toAdd); //< HACK Attention : ceci est une fusion de listes de positions et non un appel récursif

		if( _lpos.size() == _parent->_lpos.size() ) {
			_doclose=true;
		} else {
			_doclose=false;
		}
	}

	//Récursion sur les relations de séquences
	list<ClosedPrefixTree::node*>::iterator it1, it2;
	it2= T->_Childs.begin();
	while( it2 != T->_Childs.end() ) {
		bool found = false;
		it1= _Childs.begin();
		while( it1 != _Childs.end() ) {
			if( *(*it1) == *(*it2) ) {
				found=true;
				break;
			}
			it1++;
		}
		ClosedPrefixTree::node *nn=NULL;
		if( found ) {
			//ICI, le contenu du fils est mergé avec le contenu du fils de r2 (correspondant)
			nn=(*it1);

			//Et on continue ...
			nn->merge( *it2, gap_max);
		} else {
			// ICI, le contenu de r2 n'existe pas : on l'ajoute simplement
			// nn contient des positions (copiées de *it2) qui ont déjà été préfixées
			// lors de l'appel initial de brachement de l'arbre (cf seqstream.cpp,
			// fonction add(itemset s))
			nn=new ClosedPrefixTree::node( *it2 );

			//Indique d'aller rechercher les autres instances de nn dans W
			nn->set_tocomplete();
			append( nn );
		}
		it2++;
	}
}


void ClosedPrefixTree::node::set_tocomplete(bool val) {
	_tocomplete=val;
	list<node *>::const_iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		(*it)->set_tocomplete(val);
		it++;
	}
}


void ClosedPrefixTree::node::complete(vector<itemset> &sequence, int pos, unsigned int gap_max)
{
	//ICI _lpos est ordonné

	if( _parent->isroot() ) {
		//ICI _s_parent est la racine de l'arbre: on cherche tous les singletons

		// On commence par l'avant dernier pour ne pas faire de répétition !
		// On traite la séquence à l'envers pour avoir _lpos ordonné !
		vector<itemset>::reverse_iterator is=sequence.rbegin();

		int i=sequence.size();
		for( ; is!=sequence.rend(); is++) {
			for(itemset::iterator itis=(*is).begin(); itis!= (*is).end(); itis++) {
				if( (*itis)==_s ) {
					position p( i+pos-1 );

					//HACK : on ajoute au début car on parcours la sequence à l'envers :
					_lpos.push_front( p );
					break; //Ca ne sert à rien de parcourir le reste de l'itemset
				} else if( (*itis)>_s ) break; //Ca ne sert à rien de parcourir le reste de l'itemset (ordonné)
			}
			i--;
		}
#if DEBUG > 3
		cout<< "\t 1-itemset : " << _lpos.size() <<endl;
#endif
	} else {
		// !_s_parent->isroot() : ICI _s_parent n'est pas la racine de l'arbre
		// (il existe un itemset précédent l'itemset de l'item courant dans le motif)

#if DEBUG > 3
		cout << "\t positions to extend : " <<_parent->_lpos.size() << endl;
#endif

		//On parcourt toutes les instances du motif du parent
		list<position>::iterator it=_parent->_lpos.begin();
		while( it!=_parent->_lpos.end() ){

			list<position>::iterator jtp=it;
			jtp++;
			unsigned int max=sequence.size();
			if( jtp!=_parent->_lpos.end() ) {
				ClosedPrefixTree::position next_p=*jtp;
				max=next_p.back()-pos+1;
#if DEBUG > 3
				cout << "\t\t max " << max << endl;
#endif
			}
#if DEBUG > 3
			else {
				cout << "\t\t max fin " << max << endl;
			}
#endif

			unsigned int currentpos=(*it).back()-pos; //ICI currentpos est exprimé dans les indices
													  // de la séquence (entre 0 et sequence.size()-1)
			currentpos++; //< on cherche à partir du suivant !

#if DEBUG > 3
			cout << "\t\t complete "<< (*it) << endl;
			cout << "\t\t start pos " << currentpos << "(seq size " << sequence.size() << ")" <<endl;
#endif


			while( (currentpos < max) && (gap_max==0 || currentpos<= ((*it).back()-pos)+gap_max ) ) {
				itemset is = sequence[currentpos];
#if DEBUG > 4
				cout<< "\t\t\t research "<< _s << " in current itemset "<< is<< endl;
#endif
				if( is.holds(_s) ) {
					position p((*it).min_pos(), pos+currentpos );
					p.set_prefix( &(*it) );
					_lpos.push_back( p );
					break;
				}
				currentpos++;
			}

			//On passe à la completion de la position suivante ...
			it++;
		}

#if DEBUG > 3
		cout<< "\t S completed : " << _lpos.size() <<endl;
#endif
	} // _s_parent.isroot()
}


bool ClosedPrefixTree::node::complete(vector<itemset> &sequence, int pos, unsigned int fmin, unsigned gap_max)
{
#if DEBUG > 3
	cout << "COMPLETE : ";
	print_pattern();cout <<"|"<<endl;
#endif

	if( _tocomplete ) { //Lancement de la completion du noeud
		_tocomplete=false;

#if DEBUG > 3
		cout<< "\tis to complete (" << _s << ")" <<endl;
#endif

		assert( _parent!=NULL);

		//Suppression de la dernière instance (ajoutée par MERGE)
		_lpos.pop_back();
		// this a été obtenu par succession d'un item avec le parent :
		// les éléments de _lpos sont donc des éléments de _c_parent->_lpos
		// auquel on ajoute une position
		complete(sequence, pos, gap_max);
	}// to_complete


	// Si _lpos n'est pas assez grand, on retourne false pour faire détruire ce noeud courant!
	// Ceci évite de faire toute la récursion inutile
	if( !isroot() && _lpos.size() < fmin ) {
		return false;
	}

	// Si _lpos est suffisamment grand, il faut faire la récursion pour aller plus loin
	// Récursion après la complétion (vers les feuilles de l'arbre) pour accroitre les tailles des motifs au fur et à mesure (et n'oublier aucune position !)

	list<node *>::iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		bool isfreq=(*it)->complete(sequence, pos, fmin, gap_max);

		//Elagage éventuel
		if( !isfreq ) {
			node *todelete=*it;
			it = _Childs.erase( it );
			delete( todelete );
		} else {
			it++;
		}
	}

	//On regarde si il y a une fermeture possible
	if( !_parent ) {
		_doclose=false;
	} else {
		if( _lpos.size() == _parent->_lpos.size() ) {
			_doclose=true;
		} else {
			_doclose=false;
		}
	}

	//On conserve le noeud
	return true;
}

bool ClosedPrefixTree::node::isToClose()
{
	if( _Childs.empty() ) {
		return false;
	}
	list<node *>::iterator it = _Childs.begin();
	while( it!= _Childs.end() ) {
		if( !(*it)->_doclose ) return false; //if one child has strictly less occurrences, then the node is not closed.
		it++;
	}
	return true;
}


/*====================== TREE =========================*/

/**
 * \pre T is a tree in which the _doclose for each node is up to date
 * \post T is a tree containing only frequent closed patterns
 */
void ClosedPrefixTree::close() {
    list<ClosedPrefixTree::node *> nodes;
    _root.nodes(nodes);

    list<ClosedPrefixTree::node *>::reverse_iterator itn = nodes.rbegin();
    while( itn != nodes.rend() ) {
    	if( close(*itn) ) {
    		delete(*itn);
    	}
        itn++;
    }
}

bool ClosedPrefixTree::close(ClosedPrefixTree::node *n) {

	if( n->isToClose() ) {
		if( n->_pattern.size()== ( n->_pattern.size()+1 ) ) {
			//HERE, the current node is not closed and the parent is the immediat ancestor

			//Childs of itn are now childs of the parent
			list<ClosedPrefixTree::node *>::iterator it=n->_Childs.begin();
			while( it!=n->_Childs.end() ) {
				(*it)->_parent=n->_parent;
				n->_parent->_Childs.push_back( (*it) );
				(*it)->_doclose=n->_doclose;
				it++;
			}
			return true;

		} else {
			//HERE, the current node is not closed and the parent is not the immediat ancestor
			// The node is transformed into the direct ancestor node

			// Pattern modification
			n->_pattern.pop_back();

			// _lpos Transformation : remove one instance
			list<ClosedPrefixTree::position>::iterator itp=n->_lpos.begin();
			while(itp!=n->_lpos.end()) {
				(*itp).pop_back();
				itp++;
			}

			//Complete the positions list : no it's not required for this pattern because it has the same support than n
			// -> non TODO !!
			// -> ??? or maybe todo !

			// Closure possibility checking :
			if( n->_lpos.size()!=n->_parent->_lpos.size() ) {
				n->_doclose=false;
			} else {
				n->_doclose=true;
			}

			//Recursive call
			if( close(n) ) {
				// The non-closed pattern is removed from the tree
				delete(n);
				n=NULL;
			}
			return false;
		}
	} else {
		return false;
	}
	//return false;
}
