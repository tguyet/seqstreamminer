/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */
#ifndef TEST_RULEEXTRACTOR_CPP_
#define TEST_RULEEXTRACTOR_CPP_

#include <cstdlib>
#include <fstream>

#include "postfixtree.h"
#include "RuleExtractor.h"


/**
 * Programme de test de l'extraction des règles d'un arbres de motifs fréquents
 */
int main() {
	ofstream ohs;
	prefixtree *tree;

	tree = prefixtree::load("Tree.txt");

	double ct=0.7;

	list<RuleExtractor::Rule> rules=RuleExtractor::buildrules(*tree, ct);

	ohs.open("rules.txt");
	list<RuleExtractor::Rule>::iterator it= rules.begin();
	for(;it!=rules.end();it++) {
		ohs<< (*it) <<endl;
	}
	ohs.close();

	return EXIT_SUCCESS;
}


#endif /* TEST_RULEEXTRACTOR_CPP_ */
