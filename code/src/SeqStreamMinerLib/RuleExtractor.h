/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

#ifndef RULEEXTRACTOR_H_
#define RULEEXTRACTOR_H_

#include <list>
#include <iostream>
#include "stream.h"
#include "postfixtree.h"

using std::list;

/**
 * Classe pour la construction de règles à partir d'un arbre de motifs séquentiels fréquents
 * \author T. Guyet, AGROCAMPUS-OUEST
 * \date 16 juillet 2012
 */
class RuleExtractor {
public:
	class Rule {
	public:
		list<itemset> premise; 		//< premise de la règle
		list<itemset> conclusion;	//< conclusion de la règle
		double confidence;			//< confiance, correspont à la proba conditionnelle : p(conclusion|premise)
		int support;				//< support, # d'instances correspondant à la règle entière
	public:
		Rule();
		Rule(const Rule &);
		virtual ~Rule(){};

		/**
		 * \return Retourne vrai lorsque la premise ou la conclusion de la règle est vide
		 */
		bool isempty();

		/**
		 * Fonction d'affichage
		 */
		friend ostream& operator<< (ostream & os, const Rule & r);
	};

	/**
	 * \brief Fonction de construction de l'ensemble des règles ayant une confiance supérieur au seuil <a> confidence_threshold</a>
	 * \param confidence_threshold seuil de confiance pour retenir une règle
	 * \param tree ensemble des motifs fréquents
	 * \return liste de règles ayant une confiance supérieure au seuil confidence_threshold
	 */
	static list<Rule> buildrules(prefixtree &tree, double confidence_threshold);

protected:
	static Rule next(Rule &r);
};

#endif /* RULEEXTRACTOR_H_ */
