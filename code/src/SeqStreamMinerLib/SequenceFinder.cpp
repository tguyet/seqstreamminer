/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

#include "SequenceFinder.h"

#include <list>
#include <vector>
using std::vector;
using std::list;

/**
 * \param s flux dans lequel recherche un itemset
 * \param sequence sequence à rechercher dans le flux
 * \param maxgap contrainte de distance entre deux itemsets d'une instance de la séquence recherchée
 * \return une liste de liste de position dans la séquence des instance de la séquence <a>sequence</a>
 *
 * Les motifs de taille 1 ne sont pas pris en compte pour cette méthode de recherche
 */
list< list<int> > SequenceFinder::find(stream &s, list<itemset> &sequence, int maxgap) {
	list< list<int> > current, result; //listes de listes de positions

	if( sequence.size()==1) {
		std::cout << "les motifs de taille 1 ne sont pas pris en compte pour cette méthode de recherche\n";
		return result;
	}

	vector<itemset> vseq(sequence.begin(), sequence.end());

	stream::iterator it=s.begin();
	int pos=0;
	while( it!=s.end() ) {
		itemset is = *it; //is correspond à l'itemset en cours de lecture du flux d'itemsets
		bool used_to_complete=false;

		list< list<int> >::iterator itl = current.begin();
		while(itl!=current.end()) {
			//Pour chaque liste de position, on regarde si l'itemset courant
			// du flux permet de compléter la suite d'une instance du motif

			itemset isl = vseq[ (*itl).size() ]; //itemset en attente pour compléter l'instance
			if( is.holds( isl ) ) {
				//ICI l'itemset en cours contient l'itemset attendu, on complète donc l'instance
				(*itl).push_back(pos);

				//Si l'instance est complète, on a trouvé une occurrence du motif
				// 1. on la met les résultats
				// 2. on s'arrête de la prendre en compte
				if( (*itl).size()==vseq.size() ) {
					if( !used_to_complete ) {
						//Si le motif a déjà été utilisé pour compléter un mtof, alors
						// on n'en prend pas d'autre en compte
						result.push_back( (*itl) );
						used_to_complete=true;
					}
					itl=current.erase(itl);

				} else {
					itl++;
				}

			} else if( maxgap && pos>(*itl).back()+maxgap ) {
				// Si la position courante ne respecte plus la contrainte de gap
				// entre itemsets du motifs, alors elle n'est plus valide
				itl=current.erase(itl);
			} else {
				// sinon on continue à explorer le flux
				itl++;
			}
		}

		if( is.holds( vseq[0] ) ) { // Si l'itemset courant contient le premier élément du motif, alors on initie la recherche de cet élément
			list<int> l;
			l.push_back(pos);
			current.push_front( l ); //on met en front pour sélectionner les occurrences minimales grâce à <a>used_to_complete</a>
		}
		pos++;
		it++;
	}

	return result;
}
