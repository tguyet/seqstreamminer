/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

#include "SinglePassTree.h"

#include "debug.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cassert>


int SinglePassTree::position::operator==(const position &p) const {
	return _min_pos==p.min_pos() && _max_pos==p.max_pos();
}

int SinglePassTree::position::operator==(position &p) {
	return _min_pos==p.min_pos() && _max_pos==p.max_pos();
}

int SinglePassTree::position::operator<(const position &p) const {
	if( _min_pos==p.min_pos() ) {
		return _max_pos<p.max_pos();
	} else {
		return _min_pos<p.min_pos();
	}
}

int SinglePassTree::position::operator<(position &p) {
	if( _min_pos==p.min_pos() ) {
		return _max_pos<p.max_pos();
	} else {
		return _min_pos<p.min_pos();
	}
}

void SinglePassTree::position::prefix(const position &p)
{
	_min_pos=p.min_pos();
}

void SinglePassTree::position::postfix(const position &p)
{
	_max_pos=p.max_pos();
}

ostream& operator<< (ostream & os, const SinglePassTree::position & is)
{
	os << "(" << is.min_pos() << ", " << is.max_pos() << ")";
	return os;
}

SinglePassTree::node::node(const node *n): _s(n->_s), _s_parent(n->_s_parent), _tocomplete(n->_tocomplete), _depth(n->_depth), _length(n->_length)
{
	list<node *>::const_iterator it = n->_childs.begin();
	while( it!= n->_childs.end() ) {
		append( new node(*it) );
		it++;
	}

	//recopie de la liste des positions
	_lpos=n->_lpos;
}

SinglePassTree::node::~node()
{
	list<node *>::iterator it = _childs.begin();
	while( it!= _childs.end() ) {
		delete( *it );
		it++;
	}
}

void SinglePassTree::node::print_pattern(std::ostream & of) const {
	if(_s_parent!=NULL) {
		_s_parent->print_pattern(of);
		of << ">" << _s;
	}
}

list<itemset> SinglePassTree::node::pattern() const
{
	list<itemset> parentpattern;
	if(_s_parent!=NULL) {
		parentpattern=_s_parent->pattern();
		parentpattern.push_back( itemset(_s) );
	}
	return parentpattern;
}

void SinglePassTree::node::print(int pos, std::ostream & of) const {
	for(int i=0; i<pos; i++) of << " ";
	list<itemset>::const_iterator itpat;
	of << "[";
	print_pattern(of);
	of <<"] ("<< _childs.size() << ")("<< _depth<< ")("<< _length<< ") : {";
	list<position>::const_iterator itp=_lpos.begin();
	while(itp!=_lpos.end()) {
		of << (*itp);
		of << ", ";
		itp++;
	}
	of << "}";
	if( _tocomplete ) of << " *" << endl;

	pos++;
	list<node *>::const_iterator it = _childs.begin();
	while( it!= _childs.end() ) {
		of << "f";(*it)->print(pos,of);
		it++;
	}
	pos = pos - 1;
}

void SinglePassTree::node::print_instances(std::ostream & of) const {
	list<itemset>::const_iterator itpat;
	of << "[";
	print_pattern(of);
	of << "]" << endl;

	list<position>::const_iterator itp=_lpos.begin();
	while(itp!=_lpos.end()) {
		of << (*itp) << endl;
		itp++;
	}

	list<node *>::const_iterator it = _childs.begin();
	while( it!= _childs.end() ) {
		(*it)->print_instances(of);
		it++;
	}
}

/**
 * <ul>
 * <li>18/10/2012 : si ONEPASS on ne supprime plus les motifs de taille 1, même s'ils ne sont plus fréquents. <br>
 * C'est une contrepartie à avoir pour être en one pass !!! Pas très élégant !
 * </ul>
 */
void SinglePassTree::node::prune(unsigned int fmin) {

	list<SinglePassTree::node *>::iterator it = _childs.begin();
	while( it != _childs.end() ) {
		if( (*it)->_lpos.size() >= fmin || (_s_parent && !_s_parent->isroot()) ) {
			(*it)->prune(fmin);
			it++;
		} else {
			SinglePassTree::node *todelete = *it;
			it = _childs.erase( it );
			delete( todelete );
		}
	}
}

void SinglePassTree::node::nodes(list<node *> &N)
{
	N.push_back(this);
	list<node *>::const_iterator it = _childs.begin();
	while( it!= _childs.end() ) {
		(*it)->nodes(N);
		it++;
	}
}

void SinglePassTree::node::clean()
{
	list<node *>::const_iterator it = _childs.begin();
	while( it!= _childs.end() ) {
		delete(*it);
		it++;
	}
	_childs.clear();
	_lpos.clear();
	_tocomplete=false;
}


int SinglePassTree::node::size() const {
	if(_childs.size()==0) {
		//C'est une feuille
		return 0;
	} else {
		//C'est un noeud normal
		int size=_childs.size();
		list<node *>::const_iterator it = _childs.begin();
		while( it!= _childs.end() ) {
			size+=(*it)->size();
			it++;
		}
		return size;
	}
}

int SinglePassTree::node::leaves() const {
	if(_childs.size()==0) {
		//It's a leaf
		return 1;
	} else {
		//C'est un noeud normal
		int nb=0;
		list<node *>::const_iterator it = _childs.begin();
		while( it!= _childs.end() ) {
			nb+=(*it)->leaves();
			it++;
		}
		return nb;
	}
}


int SinglePassTree::node::maxdepth() const{
	if(_childs.size()==0) {
		//C'est une feuille
		return 0;
	} else {
		//C'est un noeud normal
		int maxdepth=0;
		list<node *>::const_iterator it = _childs.begin();
		while( it!= _childs.end() ) {
			int nodedepth=(*it)->maxdepth();
			maxdepth=(nodedepth>maxdepth?nodedepth:maxdepth);
			it++;
		}
		return maxdepth+1;
	}
}

void SinglePassTree::node::append(node *n) {
	n->set_s_parent(this);
	_childs.push_back(n);
}

/**
 * <ul> modification 27/02 : add the computation of depth and length
 */
void SinglePassTree::node::prefix(const SinglePassTree::position &prefix, int nodedepth, int nodelength)
{
#if DEBUG > 4
	cout << "prefix : "<<prefix << endl;
#endif

	if( !_lpos.empty() ) {
		list<position> newpos;
		list<position>::iterator itp= _lpos.begin();
		while( itp != _lpos.end() ) {
			position p=(*itp);
			p.prefix( prefix );
			newpos.push_back( p );
			itp++;
		}
		_lpos=newpos;
	}

	_length+=nodelength;
	_depth+=nodedepth;

	//Recursion
	list<node *>::iterator it = _childs.begin();
	while( it!= _childs.end() ) {
		(*it)->prefix( prefix, nodedepth, nodelength);
		it++;
	}
}


/**
 *
 * Modification of 21/02/2012 (T. Guyet) :<ul>
 * <li>add maxdepth parameters to cut the tree size by disable merge on deepest nodes
 * <li>add nocompo parameters to disable the merge with composition links
 * </ul>
 *
 * Modification of 12/06/2012 (T. Guyet) :<ul>
 * <li> add the gap_max parameter to take into account the gap max constraint
 * <li> !! parameters was not passed in the recursive call, though that default parameters was used !! this mistacke has been corrected
 * </ul>
 */
void SinglePassTree::node::merge(node *T, unsigned int gap_max, int maxdepth)
{
	assert( T->_s==-1 || _s==T->_s );

#if DEBUG > 4
	cout << "\t MERGE "; print_pattern(); cout << endl;
#endif

	if( T->_s!=-1 ) { // Si ce n'est pas un noeud racine, alors
		//Fusion des positions du noeud courant (sans répétition)
		list<SinglePassTree::position>::iterator itp2;
		list<SinglePassTree::position> toAdd; //Liste des éléments de T._lpos à ajouter à _lpos : uniquement ceux pour lesquelles il n'existe aucune exemple qui a le même préfixe de taille size()-1.
		itp2= T->_lpos.begin(); //itp2 est une position à ajouter dans la liste
		while( itp2 != T->_lpos.end() ) {
			if( _length>=2 ) {
				// Dans le cas des motifs de taille >=2, il ne faut pas ajouter une
				// instance qui briserait la condition 3
				// => la première condition ci-dessous (pas triviale) fonctionne dans le cas
				// des "motifs doublons" !!
				// => la seconde condition correspond à l'ajout d'une contrainte de gap maximum entre
				// les itemsets de l'instance

				if( _lpos.back().max_pos() <= (*itp2).min_pos() && (gap_max==0 || (*itp2).max_pos() <= (*itp2).min_pos()+(int)gap_max)) {
					toAdd.push_back(*itp2);
				}
#if DEBUG > 3
				else {
					print_pattern();
					cout << " : NOT MERGED ";
					cout << "at pos " << _lpos.back() << " try " << (*itp2);
					cout << endl;
				}
#endif
			} else {
				// => contrainte de gap maximum entre
				// les itemsets de l'instance
				if( gap_max==0 || (*itp2).max_pos() <= (*itp2).min_pos()+(int)gap_max ) {
					toAdd.push_back(*itp2);
				}
			}
			itp2++;
		}
		_lpos.merge(toAdd); //! HACK Attention : ceci merge les listes de positions et non un appel récursif
	}

	//ICI on stoppe les merge lorsque la profondeur de l'arbre atteind la limite
	if( maxdepth && maxdepth<=_depth ) {
		return;
	}

	//Récursion sur les relations de séquences
	list<SinglePassTree::node*>::iterator it1, it2;
	it2= T->_childs.begin();
	while( it2 != T->_childs.end() ) {
		bool found = false;
		it1= _childs.begin();
		while( it1 != _childs.end() ) {
			if( *(*it1) == *(*it2) ) {
				found=true;
				break;
			}
			it1++;
		}
		SinglePassTree::node *nn=NULL;
		if( found ) {
			//ICI, le contenu du fils est mergé avec le contenu du fils de r2 (correspondant)
			nn=(*it1);

			//Et on continue ...
			nn->merge( *it2, gap_max, maxdepth);
		} else {
			// ICI, le contenu de r2 n'existe pas : on l'ajoute simplement
			// nn contient des positions (copiées de *it2) qui ont déjà été préfixées
			// lors de l'appel initial de brachement de l'arbre (cf seqstream.cpp,
			// fonction add(itemset s))
			nn=new SinglePassTree::node( *it2 );

			//Indique d'aller rechercher les autres instances de nn dans W
			nn->set_tocomplete();
			append( nn );
		}
		it2++;
	}
}


void SinglePassTree::node::set_tocomplete(bool val) {
	_tocomplete=val;
	list<node *>::const_iterator it = _childs.begin();
	while( it!= _childs.end() ) {
		(*it)->set_tocomplete(val);
		it++;
	}
}

/*
 * <ul>
 * <li> 12/06/2012 (T. Guyet) : add the gap_max parameter to take into account the gap max constraint
 * <li> 18/12/2012 (T. Guyet) : add ONEPASS code parts + function profile modification
 * </ul>
 */
void SinglePassTree::node::complete_succ(const SinglePassTree &tree, unsigned int gap_max)
{
	if( _s_parent->isroot() ) {
		//ICI _s_parent est la racine de l'arbre: on cherche tous les singletons
		//c'est un strict ONEPASS : il n'y a rien à faire par ce qu'on n'a jamais supprimé les motifs de taille 1
#if DEBUG > 3
		cout<< "\t 1-itemset : nothing to do " <<endl;
		cout<< "\t 1-itemset : " << _lpos.size() <<endl;
#endif
		assert( assert_order() );
	} else if( _length==2 ) {
		// ICI le noeud correspond à un motif de taille 2list<itemset> complementar;
		list<itemset> complementar;
		itemset ist( _s_parent->_s );
		complementar.push_back( itemset(_s) );

		#if DEBUG > 3
			cout<< "\t S completed length 2 pattern : complementar pattern : " << _s << endl;
		#endif

		list<position> complementar_positions = tree.get_positions(complementar); //get positions
		if( !complementar_positions.empty() ) {
			_lpos=cat_positions(_s_parent->_lpos, complementar_positions, ist.holds(_s));
			#if DEBUG > 3
			cout<< "\t S completed : #original " << _s_parent->_lpos.size() << ", #complementar " << complementar_positions.size() <<endl;
			#endif
		} else {
			#if DEBUG > 3
			cout<< "\t S completed : no complementar occurrences" <<endl;
			#endif
		}
	} else {
		// !_s_parent->isroot() : ICI _s_parent n'est pas la racine de l'arbre, ni de longueur 1
		list<itemset> complementar;
		itemset ist(_s_parent->_s);
		complementar.push_back( ist );
		complementar.push_back( itemset(_s) );

		#if DEBUG > 3
			cout<< "\t S completed : complementar pattern : ";
			list<itemset>::iterator itcp=complementar.begin();
			while(itcp!=complementar.end()) {
				cout << *itcp <<">";
				itcp++;
			}
			cout << endl;
		#endif

		list<position> complementar_positions = tree.get_positions(complementar);
		if( !complementar_positions.empty() ) {
			_lpos=merge_positions(_s_parent->_lpos, complementar_positions);
			#if DEBUG > 3
			cout<< "\t S completed : #original " << _s_parent->_lpos.size() << ", #complementar " << complementar_positions.size() <<endl;
			#endif
		} else {
			#if DEBUG > 3
			cout<< "\t S completed : no complementar occurrences" <<endl;
			#endif
		}

		#if DEBUG > 3
		cout<< "\t S completed : " << _lpos.size() <<endl;
		#endif
		assert( assert_order() );
	}
}


/**
 * 18/10/21012
 * \see ONEPASS
 */
list<SinglePassTree::position> SinglePassTree::node::merge_positions(const list<SinglePassTree::position> &plpos, const list<SinglePassTree::position> &compl_pos) const
{
	list<SinglePassTree::position> pos;
	//On parcourt toutes les instances du motif du parent
	list<SinglePassTree::position>::const_iterator it=plpos.begin();
	list<SinglePassTree::position>::const_iterator jt=compl_pos.begin();
	while( it!=plpos.end() ) {
		while( (*it).back()> (*jt).front() && jt!=compl_pos.end() ) {
			jt++;
		}

		if( jt==compl_pos.end() ) {
			//HERE their is no more complementar positions to browse
			// it is the end of the search
			break;
		} else if( (*it).back() == (*jt).front() ) {
			//HERE we find a complementar position
			SinglePassTree::position p((*it).front(), (*jt).back());
			pos.push_back(p);
			jt++;
		}
		// ELSE (*it).back() > (*jt).front() we didn't find a corresponding position : their is no
		// possible match between the position it and any complementar position fomr cmpl_pos.
		//  -> nothing to do

		it++;
	}
	return pos;
}


/**
 * 22/10/21012
 * \see ONEPASS
 */
list<SinglePassTree::position> SinglePassTree::node::cat_positions(const list<SinglePassTree::position> &plpos, const list<SinglePassTree::position> &compl_pos, bool included) const
{
	list<SinglePassTree::position> pos;
	//On parcourt toutes les instances du motif du parent
	list<SinglePassTree::position>::const_iterator it=plpos.begin();
	list<SinglePassTree::position>::const_iterator jt=compl_pos.begin();
	list<SinglePassTree::position>::const_iterator kt;
	while( it!=plpos.end() ) {
		cout << "\t\ti:" << (*it).back()<<endl;
		while( (*it).back()>= (*jt).front() && jt!=compl_pos.end() ) {
			cout << "\t\t\tj:" << (*jt).front()<<endl;
			jt++;
		}

		if( it!=plpos.end() ) {
			kt=it;
			kt++;
			while( (*kt).back()<(*jt).front() && kt!=plpos.end() ) {
				it=kt;
				cout << "\t\t\ti:" << (*it).front()<<endl;
				kt++;
			}
		}


		if( jt==compl_pos.end() ) {
			//HERE their is no more complementar positions to browse
			// it is the end of the search
			break;
		}/* else if( included && (*it).back() == (*jt).front() ) {
			//HERE we find the same position while the last itemset is included into the previous one : it is not possible to do a new position
			jt++;
			continue;
		}*/ else {
			cout << "\t\t\tinclude ("<<(*it).front()<<","<< (*jt).back() <<")"<<endl;
			SinglePassTree::position p((*it).front(), (*jt).back());
			pos.push_back(p);
			jt++;
		}

		it++;
	}
	return pos;
}


bool SinglePassTree::node::do_complete(list<SinglePassTree::node *> &tocomplete, const SinglePassTree &tree, unsigned int fmin, unsigned int gap_max)
{
	if( _tocomplete ) { //Lancement de la completion du noeud
		_tocomplete=false;

#if DEBUG > 3
		cout<< "\tis to complete (" << _s << ")" <<endl;
#endif

		assert( _s_parent!=NULL);

		//Suppression de la dernière instance (ajoutée par MERGE)
		if(_length!=1) _lpos.pop_back();

		// this a été obtenu par succession d'un item avec le parent :
		// les éléments de _lpos sont donc des éléments de _c_parent->_lpos
		// auquel on ajoute une position
		complete_succ(tree, gap_max);

	}// to_complete


	// Si _lpos n'est pas assez grand, on retourne false pour faire détruire ce noeud courant!
	// Ceci évite de faire toute la récursion inutile
	if(_length!=1 && !isroot() && _lpos.size() < fmin ) {
		return false;
	}

	// Si _lpos est suffisamment grand, il faut traiter des motifs plus grands
	list<node *>::iterator it = _childs.begin();
	while( it!= _childs.end() ) {
		tocomplete.push_back(*it);
		it++;
	}

	return true;
}


bool SinglePassTree::complete_breadth(const SinglePassTree &tree, unsigned int fmin, unsigned int gap_max)
{
#if DEBUG > 3
	cout << "COMPLETE BREADTH : " <<endl;
#endif
	list<SinglePassTree::node *> tocomplete, tocompletenew;

	//Traitement du niveau 1 :
	list<node *>::iterator it = _root._childs.begin();
	while( it!= _root._childs.end() ) {
		(*it)->do_complete(tocomplete, tree, fmin, gap_max);
		it++;
	}

	while( !tocomplete.empty() ) {
		tocompletenew.clear();
		list<node *>::iterator it=tocomplete.begin();
		while(it!=tocomplete.end()) {
			bool isfreq=(*it)->do_complete(tocompletenew, tree, fmin, gap_max);

			//Elagage éventuel
			if( !isfreq ) {
				node *todelete=*it;
				if( (*it)->_s_parent ) {
					(*it)->_s_parent->_childs.remove( todelete );
				}
				delete( todelete );
				todelete = NULL;
			}
			it++;
		}

		tocomplete = tocompletenew;
	}

	return false;
}

void SinglePassTree::complete(vector<itemset> &sequence, const SinglePassTree &tree, int pos, unsigned int fmin, unsigned int gap_max)
{
	complete_breadth(tree, fmin, gap_max);
}

/**
 * add 12/10/2012
 * \see ONEPASS
 */
list<SinglePassTree::position> SinglePassTree::get_positions(list<itemset> p) const
{
	list<SinglePassTree::position> positions;
	const SinglePassTree::node *patternnode=findnode(p, &_root, true);
	if( !patternnode ) {
		return positions;
	} else {
		return patternnode->_lpos;
	}
	return positions;
}

/**
 * add 12/10/2012
 * \see ONEPASS
 */
const SinglePassTree::node *SinglePassTree::findnode(list<itemset> p, const SinglePassTree::node *currentnode, bool start) const
{
	if( start||p.front().empty() ) {
		//Sequence
		if( !start ) p.pop_front();

		if( p.empty() ) {
			return currentnode;
		}

		int s=p.front().front();
		p.front().pop_front();
		list<node*>::const_iterator it=currentnode->_childs.begin();
		while( it!=currentnode->_childs.end() ) {
			if( (*it)->_s==s ) {
				return findnode(p, *it, false);
			}
			it++;
		}
		//On n'a trouvé aucune correspondance
		return NULL;
	}
	return NULL;
}


#ifdef NDEBUG
int SinglePassTree::assert_monotony(const node *, const node *) const {return 1;}
int SinglePassTree::node::assert_order() const {return 1;}
int SinglePassTree::node::assert_tocomplete(bool val) const {return 1;};
#else
int SinglePassTree::assert_monotony(const node *n, const node *parent) const {
	if( parent!= NULL && n!=NULL && parent->_s!=-1 && parent->_lpos.size() < n->_lpos.size() ) {
		cerr << parent << ":=NULL," <<n <<"!=NULL,"<<parent->_s<<"!=1,"<<parent->_lpos.size()<<"<"<<n->_lpos.size()<<std::endl;
		cout << "TEST"<< std::endl;
		n->print_pattern();
		cout << std::endl;
		return 0;
	}

	list<SinglePassTree::node *>::const_iterator it = n->_childs.begin();
	while( it != n->_childs.end() ) {
		if( !assert_monotony( *it, n ) ) return 0;
		it++;
	}

	return 1;
}

int SinglePassTree::node::assert_order() const {
	list<position>::const_iterator it=_lpos.begin();
	list<position>::const_iterator jt=_lpos.begin();
	jt++;
	while( jt!=_lpos.end() ) {
		if( (*jt)<(*it) ) {
			return 0;
		}
		it++;
		jt++;
	}
	return 1;
}

int SinglePassTree::node::assert_tocomplete(bool val) const {
	if( _tocomplete!=val) {
		return 0;
	}
	list<SinglePassTree::node *>::const_iterator it = _childs.begin();
	while( it != _childs.end() ) {
		if( !(*it)->assert_tocomplete(val) ) return 0;
		it++;
	}
	return 1;
}

#endif

