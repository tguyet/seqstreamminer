/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

#ifndef SEQUENCEMINER_H_
#define SEQUENCEMINER_H_


#include <iostream>
#include "stream.h"
#include "HistoryManager.h"


/**
 * \class SequenceMiner
 * \brief Classe abstraite pour le calcul des motifs localement fréquents.
 * \author T. Guyet
 */
class SequenceMiner {
protected:
	int _ws;            		//!< windows size
	unsigned int _fmin; 		//!< frequency threshold : if _fmin==0 then there is no pruning at all
	const stream * _s;      	//!< data stream
	HistoryManager _history;	//!< pattern history manager
public:
	SequenceMiner(int ws, unsigned int fmin):_ws(ws), _fmin(fmin), _s(NULL){};
	virtual ~SequenceMiner(){};

	/**
	 * \brief Set the stream the process
	 */
	inline void setstream(const stream * s) {_s=s;};

	inline void setws(const int w) {_ws=w;};

	virtual void launch() =0;

	/**
	 * \brief History print out function
	 * \date 26/6/12 modification of the printing function : uses intervals printing
	 */
	inline void print_history(std::ostream & ohs =std::cout) {
		_history.print_historyI(ohs);
		ohs << std::flush;
	};

	inline HistoryManager history() const {return _history;};
};

#endif /* SEQUENCEMINER_H_ */
