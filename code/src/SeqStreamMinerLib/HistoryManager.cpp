/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file HistoryManager.cpp
 * \author Guyet Thomas, Inria
 * \version 0.3
 * \date 25 nov 2021
 */


#include "HistoryManager.h"
#include "debug.h"
#include <algorithm>
#include <fstream>


#ifdef _OPENMP
#include <omp.h>
#include <valarray>
#endif

HistoryManager::HistoryManager() {

}

HistoryManager::~HistoryManager() {

}

void HistoryManager::update(const prefixtree::node *n, int cur_pos) {
	list<itemset> pattern =list<itemset>();
	//HACK disable history
	update(n, pattern, cur_pos);
	assert( pattern.empty() ); //Proof the correctness of recursive call
}

void HistoryManager::updateI(const prefixtree::node *n, int cur_pos) {
	list<itemset> pattern =list<itemset>();
	//HACK disable history
	updateI(n, pattern, cur_pos);
	assert( pattern.empty() ); //Proof the correctness of recursive call
}


/**
 * Comparaison de patterns
 */
bool patterncmp (const list<itemset> & is, const list<itemset> &js) {
	if( is.size()!=js.size() ) return false;
	list<itemset>::const_iterator it=is.begin();
	list<itemset>::const_iterator jt=js.begin();
	while(it!=is.end()) {
		if( (*it)!=(*jt) ) {
			return false;
		}
		it++;
		jt++;
	}
	return true;
}

void HistoryManager::update(const prefixtree::node *n, list<itemset> &cur_it, int cur_pos)
{
	if( !cur_it.empty() ) {
		History::iterator it=_history.begin();
		while( it!=_history.end() ) {
			if( patterncmp( (*it).first, cur_it) ) {
				break;
			}
			it++;
		}
		if( it==_history.end() ) {
			list<itemset> pattern=cur_it;
			list<int> l;
			l.push_back(cur_pos);
			_history.push_back( pair<list<itemset>, list<int> >(pattern, l) );
		} else {
			(*it).second.push_back(cur_pos);
		}
	}

	list<prefixtree::node *>::const_iterator it = n->_seqChilds.begin();
	it= n->_seqChilds.begin();
	while( it != n->_seqChilds.end() ) {
		assert( (*it)!=NULL );

		cur_it.push_back( itemset( (*it)->_s ) );
		update( *it, cur_it, cur_pos);
		cur_it.pop_back();
		it++;
	}

	it = n->_compoChilds.begin();
	while( it != n->_compoChilds.end() ) {
		assert( (*it)!=NULL );

		cur_it.back().push_back( (*it)->_s );
		update( *it, cur_it, cur_pos);
		cur_it.back().pop_back();
		it++;
	}
}

void HistoryManager::updateI(const prefixtree::node *n, list<itemset> &cur_it, int cur_pos)
{
	if( !cur_it.empty() ) {
#ifdef _OPENMP
		#pragma omp critical
#endif
		{ //Un peu brutal : on met tout en section critique pour éviter les écritures/lectures en parallèle sur l'_historyI
			//bool isend=false;
			HistoryI::iterator it=_historyI.begin();
			while( it!=_historyI.end() ) {
				if( patterncmp( (*it).first, cur_it) ) {
					break;
				}
				it++;
			}
			if( it==_historyI.end() ) {
				//On n'a pas trouvé d'entrée pour l'historique de ce motif, on en créé une nouvelle
				list<itemset> pattern=cur_it;
				list<Interval> l;
				Interval interval;
				interval.debut=cur_pos;
				interval.fin=cur_pos;
				l.push_back(interval);
				_historyI.push_back( pair<list<itemset>, list<Interval> >(pattern, l) );
			} else {
				if( (*it).second.back().fin==(cur_pos-1) ) {
					(*it).second.back().fin=cur_pos;
				} else {
					Interval interval;
					interval.debut=cur_pos;
					interval.fin=cur_pos;
					(*it).second.push_back(interval);
				}
			}
		}
	}

#ifndef _OPENMP
	list<prefixtree::node *>::const_iterator its = n->_seqChilds.begin();
	its= n->_seqChilds.begin();
	while( its != n->_seqChilds.end() ) {
		assert( (*its)!=NULL );

		cur_it.push_back( itemset( (*its)->_s ) );
		updateI( *its, cur_it, cur_pos);
		cur_it.pop_back();
		its++;
	}

	its = n->_compoChilds.begin();
	while( its != n->_compoChilds.end() ) {
		assert( (*its)!=NULL );

		cur_it.back().push_back( (*its)->_s );
		updateI( *its, cur_it, cur_pos);
		cur_it.back().pop_back();
		its++;
	}
#else
	int l =0, lSize =0;
	lSize = n->_seqChilds.size();
	valarray<prefixtree::node*> items(lSize);

	list<prefixtree::node *>::const_iterator its = n->_seqChilds.begin();
	its= n->_seqChilds.begin();
	while( its != n->_seqChilds.end() ) {
		items[l]=*its;
		its++;
		l++;
	}

	//L'utilisation de loc_it en variable privée permet de ne pas avoir à gérer le partage
	list<itemset> loc_it = cur_it;
	//Attention !! utiliser firstprivate et pas private !! firstprivate initialise la variable !!
	#pragma omp parallel for default(none) shared(items, l, cur_pos) firstprivate(loc_it) schedule(dynamic)
	for (int i=0; i<l; i++) {
		loc_it.push_back( itemset( items[i]->_s ) );
		updateI( items[i], loc_it, cur_pos);
		loc_it.pop_back();
	}

	l =0; lSize = n->_compoChilds.size();
	items = valarray<prefixtree::node*>(lSize);
	its = n->_compoChilds.begin();
	while( its != n->_compoChilds.end() ) {
		items[l]=*its;
		its++;
		l++;
	}

	loc_it = cur_it;
	#pragma omp parallel for default(none) shared(items, l, cur_pos) firstprivate(loc_it) schedule(dynamic)
	for (int i=0; i<l; i++) {
		loc_it.back().push_back( items[i]->_s );
		updateI( items[i], loc_it, cur_pos);
		loc_it.back().pop_back();
	}

#endif
}

HistoryManager::HistoryI HistoryManager::interval_history() const
{
	HistoryManager::HistoryI _Ihistory;
	History::const_iterator it=_history.begin();
	while( it!= _history.end()) {
		list<HistoryManager::Interval> l;
		HistoryManager::Interval I={(*it).second.front(),(*it).second.front()};

		list<int>::const_iterator i =  (*it).second.begin();
		i++;
		while( i!=(*it).second.end() ) {
			if( I.fin==(*i)-1 ) {
				I.fin=(*i);
			} else {
				l.push_back( I );
				I.debut=(*i);
				I.fin=(*i);
			}
			i++;
		}
		l.push_back( I );

		_Ihistory.push_back( pair<list<itemset>, list<Interval> >((*it).first, l) );
		it++;
	}
	return _Ihistory;
}

void HistoryManager::print(ostream &output) const
{
	HistoryI Ihistory=interval_history();
	HistoryI::const_iterator it=Ihistory.begin();
	while( it!= Ihistory.end()) {
		//print the pattern
		list<itemset>::const_iterator is = (*it).first.begin();
		output << "{";
		while( is!=(*it).first.end() ) {
			output << *is;
			is++;
		}
		output << "}  :";
		//print the history
		list<Interval>::const_iterator i =  (*it).second.begin();
		while( i!=(*it).second.end() ) {
			output << "[" << (*i).debut << ", " << (*i).fin<<"], ";
			i++;
		}
		output << endl;
		output << flush;
		it++;
	}
}

void HistoryManager::print_history(ostream &output) const
{
	History::const_iterator it=_history.begin();
	while( it!= _history.end()) {
		//print the pattern
		list<itemset>::const_iterator is = (*it).first.begin();
		output << "{";
		while( is!=(*it).first.end() ) {
			output << *is;
			is++;
		}
		output << "} : ";
		//print the history
		list<int>::const_iterator i =  (*it).second.begin();
		while( i!=(*it).second.end() ) {
			output << (*i) << ", ";
			i++;
		}
		output << endl;
		it++;
	}
}



void HistoryManager::print_historyI(ostream &output) const
{
	HistoryI::const_iterator it=_historyI.begin();
	while( it!= _historyI.end()) {
		//print the pattern
		list<itemset>::const_iterator is = (*it).first.begin();
		output << "{";
		while( is!=(*it).first.end() ) {
			output << ">";
			output << *is;
			is++;
		}
		output << "} : ";
		//print the history
		list<Interval>::const_iterator i =  (*it).second.begin();
		while( i!=(*it).second.end() ) {
			output << "["<< (*i).debut << ","<< (*i).fin << "], ";
			i++;
		}
		output << endl;
		it++;
	}
}

bool HistoryManager::cmp(const HistoryManager &hm) const
{
	bool different=false;
	History::const_iterator jt=_history.begin();
	while( jt!= _history.end()) {
		list<itemset> is=(*jt).first;
		// find is in hm
		History::const_iterator it=hm._history.begin();
		while( it!=hm._history.end() ) {
			if( patterncmp( (*it).first, is) ) {
				break;
			}
			it++;
		}
		if( it!=hm._history.end() ) {
			//On a trouvé is : on regarde à l'intérieur maintenant !
			list<int> lpos=(*jt).second;
			list<int> hmlpos=(*it).second;

			//On les ordonne pour les comparer plus facilement
			lpos.sort();
			hmlpos.sort();

			list<int>::iterator pos=lpos.begin();
			list<int>::iterator hmpos=hmlpos.begin();
			while( pos!=lpos.begin() ) {
				while( *pos == *hmpos && pos!=lpos.end() && hmpos!=hmlpos.end()) {
					pos++;
					hmpos++;
				}
				if( pos==lpos.end() && hmpos==hmlpos.end() ) {
					//égalité parfaite
					break;
				}

				if( hmpos!=hmlpos.end() ) {
					cout << "> " << *pos <<endl;
					pos++;
					continue;
				}

				while(*pos != *hmpos  && hmpos!=hmlpos.end() ) {
					cout << "< " << *hmpos <<endl;
					hmpos++;
				}
				pos++;
			}

		} else {
			list<itemset>::const_iterator isit = is.begin();
			cout << "{";
			while( isit!=is.end() ) {
				cout << *isit;
				isit++;
			}
			cout << "} has not been found" <<endl;
			different=true;
		}

		jt++;
	}


	return !different;
}


HistoryManager::HistoryITNode *HistoryManager::HistoryTree::findnode(list<itemset>& pattern)
{
	return findnode(pattern, &root);
}

/**
 * Fonction récursive permettant de trouver/construire le noeud d'un arbre représentant l'historique.
 * La structure est un arbre de préfixage avec deux types de liens Succession ou Combinaison.
 * Dans le cas où l'itemset n'a pas été trouvé, tous les itemsets nécessaire à l'arbre sont créés.
 * Le résultat de la fonction est non-NULL.
 */
HistoryManager::HistoryITNode *HistoryManager::HistoryTree::findnode(list<itemset>& pattern, HistoryITNode *curnode)
{
	itemset is = pattern.front();//On supprime un itemset
	pattern.pop_front();
	int tofind = is.front();
	is.pop_front();
	HistoryITNode *node=NULL;
	bool found = false;

	if( is.size() == 0 ) {
		////////// Cas d'une succession //////////
		list<HistoryITNode*>::const_iterator itf = curnode->_S.begin();

		while(itf != curnode->_S.end()) {
			if( (*itf)->item==tofind ) {
				found=true;
				break;
			}
			itf++;
		}
		if( found ) {
			node = (*itf);
		} else {
			node = new HistoryITNode(tofind);
			curnode->_S.push_back(node);
		}
		if( pattern.size()==0 ) {
			return node;
		} else {
			return findnode( pattern, node );
		}

	} else {
		////////// Cas d'une composition //////////
		//On remet l'itemset auquel on a supprimé le premier élément
		pattern.push_front(is);

		list<HistoryITNode*>::const_iterator itf = curnode->_C.begin();
		while(itf != curnode->_C.end()) {
			if( (*itf)->item==tofind ) {
				found=true;
				break;
			}
			itf++;
		}
		if( found ) {
			node = (*itf);
		} else {
			node = new HistoryITNode(tofind);
			curnode->_C.push_back(node);
		}
		return findnode( pattern, node );
	}
}

HistoryManager::HistoryTree HistoryManager::history_tree() const
{
	HistoryManager::HistoryTree tree;

	History::const_iterator it=_history.begin();
	while( it!=_history.end() ) {
		list<itemset> pattern=(*it).first;
		HistoryITNode *node = tree.findnode( pattern );
		node->_pattern=(*it).first;
		node->_positions=(*it).second;
		node->_newPositions=(*it).second; //Ici, on recopie la même chose !

		//Calcul des intervalles
		list<HistoryManager::Interval> l;
		HistoryManager::Interval I={(*it).second.front(),(*it).second.front()};

		list<int>::const_iterator i =  (*it).second.begin();
		i++;
		while( i!=(*it).second.end() ) {
			if( I.fin==(*i)-1 ) {
				I.fin=(*i);
			} else {
				l.push_back( I );
				I.debut=(*i);
				I.fin=(*i);
			}
			i++;
		}
		l.push_back( I );

		node->_intervals = l;
		it++;
	}

	tree.computeNewPositions();

	return tree;
}

HistoryManager::HistoryTree *HistoryManager::load(std::string &filename)
{
	HistoryManager::HistoryTree *tree = new HistoryManager::HistoryTree();
	typedef enum {START, SEQUENCE_START, SEQUENCE_END, SEQUENCE_COMP, SEQUENCE_SUCC, READ_INTERVALS, INTERVAL_FIRST, INTERVAL_SECOND, INTERVAL_END, COMMENTARY} loadstate;

	ifstream cin;
	char c;
	cin.open(filename.c_str(), ifstream::in);
	if( !cin.good() ) {
		cerr << "While opening a file "<< filename <<" an error is encountered" << endl;
		return NULL;
	}

	loadstate state = START;
	HistoryITNode *previous;
	HistoryManager::Interval interval;
	int item;
	int lno=1;

	while( cin.good() ) {
		c='\0';
		state=START;
		while( c!='\n' && cin.good() ) {
			cin >> c;
			if( state==COMMENTARY ) {
				//On saute la fin de la ligne
				continue;
			}
			switch(c) {
			case '{':
				//On commence une nouvelle séquence
				state=SEQUENCE_START;
				previous=tree->getroot();
				break;
			case '}':
				state=SEQUENCE_END;
				break;
			case '[':
				if(state==READ_INTERVALS) {
					state=INTERVAL_FIRST;
				} else {
					cerr << "Error while reading file " << filename << " : invalid '[' line "<< lno << "." << endl;
					return NULL;
				}
				break;
			case ',':
				if( state!=INTERVAL_SECOND && state!=READ_INTERVALS ) {
					cerr << "Error while reading file " << filename << " : invalid ',' line "<< lno << "." << endl;
					return NULL;
				}
				break;
			case '>':
				state=SEQUENCE_SUCC;
				break;
			case '-':
				state=SEQUENCE_COMP;
				break;
			case ':':
				if( state!=SEQUENCE_END ) {
					cerr << "Error while reading file " << filename << " : invalid ':' line "<< lno << "." << endl;
					return NULL;
				}
				state=READ_INTERVALS;
				break;
			case ']':
				if( state!=INTERVAL_END ) {
					cerr << "Error while reading file " << filename << " : invalid ']' line "<< lno << "." << endl;
					return NULL;
				}
				state=READ_INTERVALS;
				break;
			case '#':
				state=COMMENTARY;
				break;
			}

			if(c==' ' || state==READ_INTERVALS || state==SEQUENCE_START || state==COMMENTARY  || state==SEQUENCE_END || state==INTERVAL_END ) {
				continue;
			}

			//On lit la valeur de l'entier
			cin >> item;
			if( !cin.good() ) {
				cerr << "Error while reading file " << filename << " : unexpected number, line "<< lno << "." << endl;
				delete(tree);
				return NULL;
			}

			//On recherche si le noeud de l'item existe déjà
			list<HistoryITNode*>::iterator it;
			int found=0;
			if( state==SEQUENCE_SUCC ) {
				it=previous->_S.begin();
				while( it!=previous->_S.end() ) {
					if( (*it)->item == item ) {
						found=1;
						break;
					}
					it++;
				}
				if( !found ) {
					HistoryITNode *n=new HistoryITNode(item);
					list<itemset> pattern=previous->_pattern;
					pattern.push_back(itemset(item));
					n->_pattern=pattern;
					previous->_S.push_back(n);
					previous=n;
				} else {
					previous=*it;
				}

			} else if( state==SEQUENCE_COMP ) {
				it=previous->_C.begin();
				while( it!=previous->_C.end() ) {
					if( (*it)->item == item ) {
						found=1;
						break;
					}
					it++;
				}
				if( !found ) {
					HistoryITNode * n=new HistoryITNode(item);
					list<itemset> pattern=previous->_pattern;
					pattern.back().push_back(item);
					n->_pattern=pattern;
					previous->_C.push_back(n);
					previous=n;
				} else {
					previous=*it;
				}
			} else if ( state==INTERVAL_FIRST ) {
				interval.debut=item;
				state=INTERVAL_SECOND;
			} else if ( state==INTERVAL_SECOND ) {
				interval.fin=item;
				state=INTERVAL_END;
				previous->_intervals.push_back(interval);
			}

		}
		//Ici, on est à la fin d'une ligne ou qqc c'est mal passé
		lno++;
	}

	cin.close();
	return tree;
}


void HistoryManager::HistoryTree::computeNewPositions()
{
	list<HistoryITNode*>::iterator itf = root._S.begin();
	while(itf != root._S.end()) {
		computeNewPositions(NULL, *itf);
		itf++;
	}
	itf = root._C.begin();
	while(itf != root._C.end()) {
		computeNewPositions(NULL, *itf);
		itf++;
	}
}


void HistoryManager::HistoryTree::computeNewPositions(HistoryITNode *parentnode, HistoryITNode *curnode)
{
	if(parentnode!=NULL) {
		//Suppression dans la liste parentnode._newPositions tous les éléments qui se trouvent dans curnode._positions
		list<int> diff;
		diff.assign(parentnode->_newPositions.size(),0);
		list<int>::iterator it;
		//Pas très élégent : normalement utiliser un back_inserter ... mais ca marche pas !
		it=set_difference(parentnode->_newPositions.begin(), parentnode->_newPositions.end(), curnode->_positions.begin(), curnode->_positions.end(), diff.begin());
		diff.remove(0);
		parentnode->_newPositions = diff;
	}
	//Recursion
	list<HistoryITNode*>::iterator itf = curnode->_S.begin();
	while(itf != curnode->_S.end()) {
		computeNewPositions(curnode, *itf);
		itf++;
	}
	itf = curnode->_C.begin();
	while(itf != curnode->_C.end()) {
		computeNewPositions(curnode, *itf);
		itf++;
	}
}


