/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

#ifndef STREAMGENERATOR_H_
#define STREAMGENERATOR_H_

#include "stream.h"

/**
 * \brief Functions to generate stream of itemsets
 * \date 25 nov 2021
 * \author T. Guyet
 */
class StreamGenerator {
public:
	static stream generate(int s, int ql);
	static stream bernoulli(int s, int ql, double p);
	static stream poisson(int s, int ql, double lambda);
};

#endif /* STREAMGENERATOR_H_ */
