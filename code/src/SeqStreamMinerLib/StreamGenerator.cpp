/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */


#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "StreamGenerator.h"
#include "alea.h"


/**
 * Génération d'une séquence d'items (itemsets de taille 1) avec évènements équiprobables
 * @param ql nombre d'items
 * @param s longueur de la séquence générée
 */
stream StreamGenerator::generate(int s, int ql)
{
	stream S;
	srand( time(NULL) );

	for(int i=0; i<s; i++) {
		itemset is;
		is.push_back( 1+rand()%ql );
		S.push_back(is);
	}

	return S;
}


/**
 * Génération d'une séquence d'itemsets avec un modèle de Bernoulli (même paramètre pour tous les items)
 * @param ql number of items
 * @param s sequence length
 * @param p Bernoulli parameter
 */
stream StreamGenerator::bernoulli(int s, int ql, double p)
{
	stream S;
	init_genrand(time(NULL));

	for(int i=0; i<s; i++) {
		itemset is;
		for( int j=1;j<=ql; j++) {
			if(genrand_real2()<p) {
				is.push_back( j );
			}
		}
		S.push_back(is);
	}

	return S;
}



/**
 * Génération d'une séquence d'itemsets avec un modèle de Poisson
 * @param ql nombre d'items
 * @param s longueur de la séquence générée
 */
stream StreamGenerator::poisson(int s, int ql, double lambda)
{
	stream S;
	init_genrand( (unsigned long) time(NULL) );

	for(int i=0; i<s; i++) {
		itemset is;
		for( int j=1;j<=ql; j++) {
			if(poissonrand(lambda)<1) {
				is.push_back( j );
			}
		}
		S.push_back(is);
	}

	return S;
}

