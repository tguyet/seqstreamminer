/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file PSStream.h
 * \author Guyet Thomas, Inria
 * \version 0.3
 * \date 25 nov 2021
 */

#ifndef PSSTREAM_H_
#define PSSTREAM_H_

#include <vector>
#include "postfixtree.h"
#include "SequenceMiner.h"

using namespace std;

/**
 * \brief Classe pour l'extraction de motifs fréquents dans des fenêtres d'une longue séquence en effectuant la fouille successivement
 * sur chaque fenêtre (méthode dite par batch)
 */
class PSStream : public SequenceMiner {
protected:
	int _cur_pos;       	//!< current position in the data stream
	vector<itemset> _sequence;		//!< The current sequence in the stream
	bool _output;					//!< false to disable all outputs in non-debug mode !
public:
	PSStream(int ws, unsigned int fmin):SequenceMiner(ws,fmin), _cur_pos(0), _output(true){};
	virtual ~PSStream(){};

	void launch();

	/**
	 * \brief Setter
	 */
	void set_output(bool val=true) {_output=val;};

protected:

	/**
	 * \brief mine all the frequent patterns  in the windows sequence
	 * \param the sequence to mine
	 *
	 * A frequent pattern is a subsequence that appears with a frequence >_fmin.
	 */
	prefixtree *mine(vector<itemset> &sequence);

	void project(prefixtree::node *n, vector<itemset> &sequence, list<int> items);
};

#endif /* PSSTREAM_H_ */
