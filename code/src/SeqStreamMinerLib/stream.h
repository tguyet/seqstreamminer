/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ agrocampus-ouest fr (remove space and add dots)
 */

/**
 * \file stream.h
 * \author Guyet Thomas, Inria
 * \version 0.3
 * \date 25 nov 2021
 */

#define FILE_FORMAT_PERSO	0
#define FILE_FORMAT_IBM 	1

#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED

#include <list>
#include <ostream>
#include <istream>
#include <map>
#include <iostream>

using std::list;

/**
 * \class itemset
 * \brief a set of items
 * No repetition in one itemset. Items are always sorted !
 * \author Guyet Thomas, AGROCAMPUS-OUEST
 * \todo generalized with an other type of item (currently int)
 */
class itemset : public list<int>
{
public:
	typedef list<int>::const_iterator const_iterator;
	typedef list<int>::iterator iterator;
protected:
	int _timestamp;
public:
	/**
	 * \brief Default constructor
	 */
	itemset():_timestamp(0){};

	/**
	 * \brief Copy constructor
	 */
	itemset(const itemset &is):list<int>(is), _timestamp(is._timestamp){};

	/**
	 * \brief Constructor with a first item
	 */
	itemset(int i):_timestamp(0){push_back(i);};

	/**
	 * \brief Desctructor
	 */
	~itemset(){};

	inline void set_timestamp(int t) {_timestamp=t;}
	inline int timestamp() const {return _timestamp;};

	/**
	 * \brief opérateur de comparaison basé si le timestamp
	 *
	 * Cette fonction est utile pour ordonné les séquences d'itemset.
	 * \see Data::load
	 */
	inline bool operator<(const itemset& i2) const { return _timestamp < i2._timestamp;};

	/**
	 * \brief Comparison operator
	 */
	int operator==(const itemset &) const;

	/**
	 * \brief Comparison operator
	 */
	int operator!=(const itemset &) const;

	/**
	 * \brief stream output (printing)
	 */
	friend std::ostream& operator<< (std::ostream & os, const itemset & is);

	/**
	 * \brief Push back an item and ensure their order.
	 * \post ensures that the list of items is sorted !
	 */
	void push_back(int i) {list<int>::push_back(i); sort();};

	/**
	 * \brief Check if an item i is in the itemset
	 * \param i
	 */
	bool holds(const int i) const;

	/**
	 * \brief Check if an itemset is is in the itemset
	 * \param is
	 */
	bool holds(const itemset is) const;

	/*
	unsigned int size() const {return list<int>::size();};

	const_iterator begin() const {return list<int>::begin();};
	iterator begin() {return list<int>::begin();};

	const_iterator end() const {return list<int>::end();};
	iterator end() {return list<int>::end();};

	void clear() {list<int>::clear();};

	void pop_back() {list<int>::pop_back();};
	void pop_front() {list<int>::pop_front();};
	void push_front(int val) {list<int>::push_front(val); sort();};
	int front() {return list<int>::front();};

	void sort() {list<int>::sort();};
	void unique() {list<int>::unique();};
	//*/
};

/**
 * \class stream
 * \brief a stream is an ordered set of itemsets
 */
class stream : public list<itemset>
{
protected:
	mutable const_iterator _pos;        //!< current position in the stream (mutable for constant functions next() and init())
	mutable int _posid;                 //!< id of the current position (mutable for constant functions next() and init())
	mutable bool _isend;                //!< stream end flag (mutable for constant functions next() and init())

	std::map<char, int> _symbols;    //!< input map that maps the characters of the input file with identifier

public:
	/**
	 * \brief Default constructor
	 */
	stream():_posid(0), _isend(false){};

	/**
	 * \brief Destructor
	 */
	~stream(){};

	/**
	 * \brief getter
	 */
	inline int posid() const {return _posid;};

	/**
	 * \brief simple function overload
	 */
	inline unsigned int size() const {return list<itemset>::size();};

	/**
	 * \brief getter
	 */
	inline bool isend() const {return _isend;};

	/**
	 * \brief initialize the stream
	 */
	inline void init() const {_pos=begin(); _posid=0; _isend=false;};

	/**
	 * \brief provide the next itemset to process
	 * \return the next itemset if some, and an empty itemset of no more.
	 */
	const itemset next() const {
		if( _pos == end() ) {
			_isend=true;
			return itemset();
		}
		itemset val=*_pos;
		_pos++;
		_posid++;
		return val;
	};

	/**
	 * \brief push an itemset at the back of the stream
	 */
	inline void push_back(const itemset i) { list<itemset>::push_back(i); };

	/**
	 * \brief clear the stream and the symbols
	 */
	inline void clear() { list<itemset>::clear(); _symbols.clear(); };

	/**
	 * \brief Construct a sequence of itemset from input stream
	 * \param input the input stream
	 * \return 0 if reading encountered an error (then the stream is empty), 1 otherwise
	 *
	 * The input stream must be in character format : "a(ab)(abc) ...". Each character
	 * is automatically mapped to an integer via the _symbol structure.
	 * The file may be multiline.
	 * Space, tabulation are omitted
	 * The character '#' may be used to insert comment in the file
	 */
	int read(std::istream &input);

	/**
	 * \brief Construct a sequence of itemset from input stream in an IBM format
	 *
	 * Only the 1 sequence of the IBM generated sequence will be taken into account.
	 *
	 */
	int read_ibm(std::istream &input);

	/**
	 * \brief print the stream on cout
	 */
	void print() const;


	friend std::ostream& operator<< (std::ostream & os, const stream & is);

};

#endif // STREAM_H_INCLUDED
