/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

#ifndef SINGLEPASSTREE_H_
#define SINGLEPASSTREE_H_

#include <list>
#include <vector>
#include <iostream>
#include <ostream>
#include <string>

#include "stream.h"
#include "debug.h"

using namespace std;

class SinglePassTree {
public:

	/**
	 * \class position
	 * \brief This class describes one position of an itemset in the sequence
	 */
	class position {
	protected:
		int _min_pos, _max_pos;
	public:

		/**
		 * \brief Default constructor
		 */
		position():_min_pos(0),_max_pos(0){};

		/**
		 * \brief Constructor
		 * \param i
		 *
		 * Initialize the position with the value i
		 */
		position(int i):_min_pos(i),_max_pos(i){};

		/**
		 * \brief Constructor
		 * \param b : minimal position (begin)
		 * \param e : maximal position (end)
		 *
		 * Initialize the position with the value i
		 */
		position(int b, int e):_min_pos(b),_max_pos(e){};


		/**
		 * \brief Destructor
		 */
		~position(){};

		/**
		 * \brief egality operator
		 * \param p
		 * \return true if p is equal to the position, false otherwise
		 *
		 * The method compares positions until dismatch.
		 */
		int operator==(const position &p) const;

		/**
		 * \brief egality operator
		 * \param p
		 * \return true if p is equal to the position, false otherwise
		 *
		 * The method compares positions until dismatch.
		 */
		int operator==(position &p);


		/**
		 * \brief comparison operator
		 * \param p
		 *
		 * p1 < p2 iff p1.front < p2.front or (p1.front==p2.front et p1'.back<p2'.back) with p1=front.p1' and p2=front.p2'
		 */
		int operator<(const position &p) const;

		/**
		 * \brief comparison operator
		 * \param p
		 *
		 * p1 < p2 iff p1.front < p2.front or (p1.front==p2.front et p1'.back<p2'.back) with p1=front.p1' and p2=front.p2'
		 */
		int operator<(position &p);

		/**
		 * \brief increment operator
		 *
		 * The function increments the min position and the max position
		 * May be usefull to decay the value of the whole position.
		 */
		void operator++(int ){
			_min_pos++;
			_max_pos++;
		}

		/**
		 * \brief decrement operator
		 *
		 * The function decrements the min position and the max position.
		 * May be usefull to decay the value of the whole position.
		 * \see seqstream::decayallpos(SinglePassTree::node *)
		 */
		void operator--(int ){
			_min_pos--;
			_max_pos--;
		}

		/**
		 * \param p a position to prefix with
		 * \brief The function prefixes the current position with p
		 * The order of concatenated positions are safe using this function
		 */
		void prefix(const position &p);

		/**
		 * \param p a position to prefix with
		 * \brief The function postfixes the current position with p
		 * The order of concatenated positions are safe using this function
		 */
		void postfix(const position &p);

		/**
		 * \brief stream output (printing)
		 */
		friend ostream& operator<< (ostream & os, const SinglePassTree::position & is);


		/** GETTERS and SETTERS **/

		int front() const {
			return _min_pos;
		}
		int back() const {
			return _max_pos;
		}

		int min_pos() const {
			return _min_pos;
		}

		int max_pos() const {
			return _max_pos;
		}

		void set_min_pos(int val) {
			_min_pos=val;
		}

		void set_max_pos(int val) {
			_max_pos=val;
		}
	};

	/**
	 * \class node
	 * \brief One node of the tree
	 *
	 * One node may have a composition parent of a sequential parent (not both !) or none (only the root)
	 */
	class node {
	public:
		list<node *> _childs;    //!< childs of the current node (sequence relationship), this list must keep ordered if ORDERNODES is true (use only append function to add nodes)
		list<position> _lpos;       //!< positions of the pattern (represented by the node in the tree) in the sequence
		int _s;                     //!< item value
		node *_s_parent;			//!< the parent node (sequential)
		bool _tocomplete;			//!< flag to process to the completion of the position list of the node !
		int _depth;					//!< depth in the tree (correspond to the size of the pattern)
		int _length;				//!< the length is the number of itemsets in the pattern

	public:
		/**
		 * \brief Default constructor
		 */
		node(int s): _s(s), _s_parent(NULL), _tocomplete(false), _depth(0), _length(0)
		{};

		/**
		 * \brief Copy constructor
		 */
		node(const node *n);

		/**
		 * \brief Destructor : recursively destruct the node (and descendants)
		 */
		~node();

		/**
		 * \brief the function construct recursively the list of all nodes in the subtree of the current node and fill the list
		 * \param l the list of nodes to fill in.
		 */
		void nodes(list<node *> &l);

		/**
		 * \brief cleaning function
		 *
		 * Recursively destroys subnodes without destroying the current node.
		 */
		void clean();

		/**
		 * \brief comparison operator
		 */
		inline int operator==(const node &n) const {return _s==n._s;};


		/**
		 * \brief Add a node n corresponding to a sequencial item
		 * \param n pointer on the node to add
		 *
		 * The object pointed by n is not copied.
		 */
		void append(node *n);


		/**
		 * \brief Add a position to the list of position
		 * \param p the position to add
		 */
		inline void append(position &p) {_lpos.push_back(p);};

		/**
		 * \brief It prefixes recursively all the positions of the subtree with the prefix prefixe
		 * \param prefixe
		 * \param nodedepth : add this value to the current _depth attribute vlaue
		 * \param nodelength : add this value to the current _length attribute value
		 */
		void prefix(const SinglePassTree::position &prefixe, int nodedepth, int nodelength);

		/**
		 * \brief Set the sequential parent
		 */
		void set_s_parent(node *parent) {_depth=parent->_depth+1;_s_parent=parent;_length=parent->_length+1;};

		/**
		 * \brief define recursively the value of the flag tocomplete to the node and subnodes
		 * \param val (default true)
		 *
		 * Be careful of the recursion behaviour ; if val is false, then it sets the value false to the flag
		 * of all subnodes. But, if the val is true then the recursion is applied only to the composition childs.
		 */
		void set_tocomplete(bool val=true);


		/**
		 * \brief Test if the node is the root
		 */
		inline bool isroot() const {return _s_parent==NULL;};

		/**
		 * \brief merge recursively the content of the node n (items and positions) into the current node
		 * \param prefixes list of prefixes
		 * \param maxdepth maximum depth of the merged node (infinite if null) (default, null)
		 * \param nocompo disable merge on composition (default, false)
		 * \param gap_max maximum gap between two successive instances of one pattern, no constraint if nul (default 0)
		 *
		 * The function adds the positions to the matching nodes, add children nodes and recurs
		 * If maxdepth is not nul, the recursion is stopped while the node \a n has a depth greater to maxdepth
		 * The  gap_max constraint is used in both adding instance to existing nodes and in the completion step
		 *
		 * \pre each instance of node \a n may be different from instance of the current node otherwise doublon will be inserted !
		 */
		void merge(node *n, unsigned int gap_max=0, int maxdepth=0);

		/**
		 * \pre plpos and compl_pos are ordered lists of positions
		 * \pre lists of positions are associated to two patterns that have one itemset in common
		 * \post returns an ordered list of positions
		 */
		list<position> merge_positions(const list<position> &plpos, const list<position> &compl_pos) const;

		list<position> cat_positions(const list<position> &plpos, const list<position> &compl_pos, bool included) const;

		/**
		 * \brief Complete the list of instances of the current node
		 * \param tocomplete list of nodes that is filled by the function. The list holds the nodes that must be recursivelly completed
		 * \param sequence sequence of the current window
		 * \param tree current tree of frequent patterns
		 * \param fmin frequency threshold
		 * \param gap_max
		 * \return true is the current node, while completed, is not frequent (to delete) or not.
		 */
		bool do_complete(list<SinglePassTree::node *> &tocomplete, const SinglePassTree &tree, unsigned int fmin, unsigned int gap_max);

		/**
		 * \brief print recursively the node and its descendants on stdout
		 * \param pos : recursion level required to decay the print
		 * \param of : output stream (cout by default)
		 * \see SinglePassTree::node::print_pattern
		 */
		void print(int pos, std::ostream & of =std::cout) const;

		/**
		 * \brief print recursively the node and its descendants on stdout
		 * \param of : output stream (cout by default)
		 * \see SinglePassTree::node::print_pattern
		 */
		void print_instances(std::ostream & of =std::cout) const;


		/**
		 * \brief print recursively the node and its descendants on stdout whithout decay
		 * \see SinglePassTree::node::print(int pos)
		 */
		void print_pattern(std::ostream & of =std::cout) const;


		/**
		 * \brief recursively build the pattern of the node
		 *
		 * This function is not efficient. Uses it with parcimony !
		 *
		 * \see RuleExtractor
		 */
		list<itemset> pattern() const;


		/**
		 * \brief prune all the node whose _lpos has a size lower to fmin
		 * \param fmin occurrence number threshold
		 *
		 * This function can be used to force pruning a subtree from its unfrequent nodes.
		 */
		void prune(unsigned int );

		/**
		 * \brief Recursively computes the size of the node (# of whole child nodes)
		 */
		int size() const;

		/**
		 * \brief Recursively computes the maximal depth of the descendant of the node
		 */
		int maxdepth() const;


		int length() const {
			return _length;
		}
		int depth() const {
			return _depth;
		}

		/**
		 * \brief Return the number of leaves
		 */
		int leaves() const;

		/**
		 * \brief Check the value of the node attribute _tocomplete is equal to \a val
		 * \param val
		 *
		 * \return 1 if so
		 */
		int assert_tocomplete(bool val) const;


		/**
		 * \brief Completion of the position list in case of succession (the parent is a succession parent)
		 * \see SinglePassTree::node::complete(vector<itemset> &sequence, int pos, unsigned int fmin)
		 * \param sequence correspond à la fenêtre dans le flux
		 * \param pos position de la fenetre dans le flux : le (X-pos+1) itemset du flux est le Xieme itemset de la fenêtre entre 1 et sequence.size().
		 * \param gap_max contrainte de gap maximal entre deux itemsets successifs d'une instance d'un motif (si 0, pas de contrainte)
		 */
		void complete_succ(const SinglePassTree &tree, unsigned int gap_max);

	protected:


		/**
		 * \brief Check the position list order (for debug)
		 *
		 * \return 1 if the list is ordered, 0 otherwise
		 */
		int assert_order() const;
	};

protected:
	node _root; //!< Root node of the tree (item -1)

public:
	/**
	 * \brief Default constructor
	 */
	SinglePassTree():_root(-1){};

	/**
	 * \brief Copy constructor
	 */
	SinglePassTree(const SinglePassTree &tree):_root(tree._root){};

	/**
	 * \brief Destructor
	 * Destruct recursively all the nodes
	 */
	~SinglePassTree(){};

	/**
	 * \brief getter
	 */
	inline node *root() {return &_root;};

	/**
	 * \brief print the tree on output stream (cout by default)
	 */
	void print(std::ostream & of =std::cout) const {
		of << "~~~~Tree~~~~\n";
		_root.print(0, of);
		of << "\n~~~~~~~~~~~~\n";
	};

	void print_instances(std::ostream & of =std::cout) const {
		_root.print_instances(of);
	}

	/**
	 * \brief Construct the list of nodes in the tree
	 *
	 * NB: Can not be const because the returned list includes itself and the list is used to prefix its elements
	 * \return the list of nodes
	 */
	list<node *> nodes() {
		list<node *> nodeliste;
		_root.nodes(nodeliste);
		return nodeliste;
	};

	/**
	 * \brief Check the monotony of the current structure (for debug)
	 *
	 * The function can be used to assert the monotony of the _tree (formally proved).
	 * \see SinglePassTree::assert_monotony(const node *, const node *)
	 */
	int assert_monotony() const {if(!assert_monotony(&_root)) { _root.print(0);return 0;} return 1;};


	/**
	 * \brief Check the value of the node attribute _tocomplete (equal to \a val
	 * \param val
	 *
	 * \return 1 if the attribute _tocomplete for all nodes of the tree is equal to val
	 */
	int assert_tocomplete(bool val) const { return _root.assert_tocomplete(val); };

	/**
	 * \brief prune all the nodes whose _lpos has a size lower to fmin
	 * \param fmin occurrence number threshold
	 */
	void prune(unsigned int fmin) { _root.prune(fmin); };


	/**
	 * \brief Return the size of the tree (# of nodes)
	 */
	int size() const {return _root.size();};

	/**
	 * \brief Return the maximum depth of the tree
	 */
	int depth() const {return _root.maxdepth();};

	/**
	 * \brief Return the number of leaves
	 */
	int leaves() const {return _root.leaves();};


	/**
	 * \brief Implement the completion of the list of instance of whole the node of the tree
	 */
	void complete(vector<itemset> &sequence, const SinglePassTree &tree, int pos, unsigned int fmin, unsigned int gap_max);


	/**
	 * \brief get the positions list of a pattern. If the pattern is not in
	 * the tree the function returns an empty list.
	 */
	list<position> get_positions(list<itemset> p) const;


protected:
	/**
	 * \brief Check the monotony of the current structure (for debug)
	 *
	 * The function can be used to assert the monotony of the _tree (formally proved).
	 * \see SinglePassTree::assert_monotony()
	 */
	int assert_monotony(const node *n, const node * =NULL) const;

	/**
	 * \brief completion of the lists of instances of the current tree of frequent patterns
	 *
	 * This algorithm traverses the tree using a breadth-first strategy. Thus, the do_complete function can
	 * be call ensuring that all shorter patterns have been completed before.
	 */
	bool complete_breadth(const SinglePassTree &tree, unsigned int fmin, unsigned int gap_max);

	/**
	 * \brief Search the node corresponding to the pattern p in the SinglePassTree.
	 * \param p postfix pattern
	 * \param currentnode prefix node
	 * \param start must be true if the currentnode is the root of the tree and p is the looked for pattern (false otherwise)
	 */
	const node *findnode(list<itemset> p, const node *currentnode, bool start) const;
};

#endif /* SINGLEPASSTREE_H_ */
