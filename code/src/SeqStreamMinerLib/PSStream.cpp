/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file PSStream.cpp
 * \author Guyet Thomas, Inria
 * \version 0.3
 * \date 25 nov 2021
 */

#include "PSStream.h"

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <map>
#include <unistd.h>
#include <stdlib.h>

#include "debug.h"


void PSStream::launch() {
	prefixtree *_tree=NULL;
	if( _s==NULL ) {
		cerr << "No stream\n";
		return;
	}

	#ifdef TIME_PROBE
	clock_t start,end;
	fstream timefile( "times_sm.txt", ios::out );
	if( timefile.fail() ) {
		cerr << "The TIME_PROBE file ('times_sm.txt') can not be open !" << endl;
		exit(EXIT_FAILURE);
	}
	timefile << "pos time tree_size tree_depth"<<endl;
	#endif

	_s->init();
	_cur_pos=0;
	//bootstrap sur la première fenêtre : on ne fait que des ajouts
	while( !_s->isend() && _s->posid()!=_ws ) {
		#ifdef TIME_PROBE
		start = clock();
		#endif

		_sequence.push_back( _s->next() );
		_cur_pos++;

		#if DEBUG > 1
		cout << "======================\n";
		cout << "   received " << _sequence.back() << "\n";
		cout << "======================\n";
		#endif

		_tree=mine(_sequence);
		#if DEBUG >2
		_tree->print();
		#endif

    	_history.update(_tree->root(), _cur_pos );

		#if DEBUG < 1
		if( _output ) {
			printf("%.2lf%%", 100*(float)_cur_pos/(float)_s->size());
			fflush(stdout);
			printf("\r");
		}
		#endif

		#ifdef TIME_PROBE
		end = clock();
		timefile << _cur_pos << " "<< (double)(end-start)/CLOCKS_PER_SEC << " " << _tree->size() << " " << _tree->depth() << std::endl;
		#endif

    	delete(_tree);
    	_tree=NULL;
	}

	while( !_s->isend() ) {
		#ifdef TIME_PROBE
		start = clock();
		#endif

		#if DEBUG > 1
		cout << "======================\n";
		cout << "   deleted ("<< _cur_pos-_ws+1 <<") " << _sequence[0] << "\n";
		cout << "======================\n";
		#endif

		//decalage de la sequence :
		for(int i=0; i<_ws-1; i++) {
			_sequence[i]=_sequence[i+1];
		}
		_sequence[_ws-1] = _s->next();
		_cur_pos++;

		#if DEBUG > 1
		cout << "======================\n";
		cout << "   received " << _sequence[_ws-1] << "\n";
		cout << "======================\n";
		#endif

		_tree=mine(_sequence);
		#if DEBUG >2
		_tree->print();
		#endif

    	_history.update(_tree->root(), _cur_pos );

		#if DEBUG < 1
		if( _output ) {
			printf("%.2lf%%", 100*(float)_cur_pos/(float)_s->size());
			fflush(stdout);
			printf("\r");
		}
		#endif

		#ifdef TIME_PROBE
		end = clock();
		timefile << _cur_pos << " "<< (double)(end-start)/CLOCKS_PER_SEC << " " << _tree->size() << " " << _tree->depth() << std::endl;
		#endif

    	delete(_tree);
    	_tree=NULL;
	}

	#if DEBUG < 1
	if( _output ) {
		printf("[COMPLETE]\n");
		fflush(stdout);
	}
	#endif
}

prefixtree *PSStream::mine(vector<itemset> &sequence) {
	prefixtree *tree = new prefixtree();

	prefixtree::node *n=tree->root();
	list<int> items;
	//Niveau 1
	vector<itemset>::iterator it = sequence.begin();
	int pos=1;
	while( it!=sequence.end() ) {
		itemset is = *it;
		itemset::iterator itis = is.begin();
		while( itis!=is.end() ) {
			bool found=false;
			list<prefixtree::node *>::iterator itn = n->_seqChilds.begin();
			while( itn!= n->_seqChilds.end() ) {
				if( (*itn)->_s == (*itis) ) {
					(*itn)->_lpos.push_back( prefixtree::position(pos) );
					found=true;
					break;
				}
#if ORDERNODES
				else if ( (*itn)->_s>(*itis) ) { //HACK uses the order of _compoChild
					break;
				}
#endif
				itn++;
			}
			if( !found ) { //Si on n'a trouvé aucun noeud pour itis, alors on ajoute un noeud dans l'arbre
				prefixtree::node *nn = new prefixtree::node( *itis );
				nn->_lpos.push_back( prefixtree::position(pos) );
				n->append( nn );
			}
			itis++;
		}
		pos++;
		it++;
	}

	//Construction de la liste des items de l'arbre (non-nécessairement fréquents)
	list<prefixtree::node *> nodes=tree->nodes();
	list<prefixtree::node *>::iterator itn=nodes.begin();
	while( itn!=nodes.end() ) {
		if( (*itn)->_s==-1 ) {
			itn++;
			continue;
		}
		items.push_back( (*itn)->_s );
		itn++;
	}
	items.sort();
	items.unique();

	 // On supprime les non-fréquents
	tree->prune( _fmin );

	//Projections des suivants
	nodes=tree->nodes();
	itn=nodes.begin();
	while( itn!=nodes.end() ) {
		project(*itn, sequence, items);
		itn++;
	}
	return tree;
}

void PSStream::project(prefixtree::node *n, vector<itemset> &sequence, list<int> items) {
	list<int>::iterator it=items.begin();
	list<prefixtree::position>::iterator itp, jtp;

#if DEBUG > 3
	cout << "==============" << endl;
	cout << "PROJECTION : "; n->print_pattern(); cout << endl;
	cout << "itemsize ("<< items.size()<< " )"<<endl;
#endif


	while( it!= items.end() ) {
		list<prefixtree::position> curpositions;

#if DEBUG > 3
		itemset current=n->currentitemset();
		cout << "\tItemset to extend : "<< current << endl;
#endif


#if DEBUG > 3
		cout << "\tSuccession : "<< *it << endl;
#endif
		//Recherche des instances du motif postfixé avec (*it) avec une relation sequentielle
		itp = n->_lpos.begin();
		while( itp != n->_lpos.end()) {
			prefixtree::position p=*itp;

			jtp=itp;
			jtp++;
			unsigned int max=sequence.size();
			if( jtp!=n->_lpos.end() ) {
				prefixtree::position next_p=*jtp;
				max=next_p.back();
			}

#if DEBUG > 4
			cout << "\t\textends : " << p  << endl;
#endif
			unsigned int pos=p.back();
			pos++;
			while( pos <= max ) {
#if DEBUG > 4
				cout << "\t\tpos : "<< pos << " : " << sequence[pos-1] << endl;
#endif

				if( sequence[pos-1].holds(*it) ) {
#if DEBUG > 4
					cout << "\t\t\tfound" << endl;
#endif
					p.set_max_pos(pos);
					curpositions.push_back(p);
					break; //On s'arrête dès qu'on en a trouvé un !
				}
				pos++;
			}

			itp++;
		}

		//On ajoute à l'arbre et on "récurre", que si on est fréquent !
		if( curpositions.size() >= _fmin ) {
			prefixtree::node *nn = new prefixtree::node( *it );
			nn->_lpos = curpositions;
			n->append( nn );

			//TODO sort!!
			nn->_lpos.sort();

			//Recursion :
			project(nn,sequence,items);
		}

#if DEBUG > 3
		cout << "\tComposition : " << *it << endl;
#endif

		//Recherche des instance du motif postfixé avec (*it) avec une relation de composition
		curpositions.clear();

		if( n->_s < *it ) {
			itp = n->_lpos.begin();

			while( itp!= n->_lpos.end()) {
				prefixtree::position p=*itp;
				unsigned int pos=p.back();
				itemset is = sequence[pos-1];
#if DEBUG > 3
				cout << "\t\tTest " << p << cout<<": " << is << " holds " << *it << endl;
#endif
				if( is.holds(*it) ) {
#if DEBUG > 3
					cout << "\t\t\tadded" <<endl;
#endif
					curpositions.push_back(p);

				} else if ( n->length()>1 ) {

#if DEBUG > 3
					cout << "\t\t\t not found locally : FULL complete search" << endl;
#endif

					//Reconstruction de l'itemset entier (parent + *it)
					itemset current= n->currentitemset();
					current.push_back( *it );

					pos++;
					jtp=itp;
					jtp++;
					unsigned int max=sequence.size();
					if( jtp!=n->_lpos.end() ) {
						unsigned int nextpos = (*jtp).back();
						nextpos--;
						max=(max<nextpos?max:nextpos); //Position max à ne pas dépasser : le min de la sequence et de la position suivante
					}
#if DEBUG > 4
					cout << "\t\t\t\t max : " << max << " (" << pos <<")"<< endl;
#endif
					while( pos<=max ) {
						is = sequence[ pos-1 ];
#if DEBUG > 4
						cout << "\t\t\t\t Test " << is << endl;
#endif
						if( is.holds( current ) ) {
#if DEBUG > 4
							cout << "\t\t\t\t FOUND" << endl;
#endif
							prefixtree::position lp=*itp;
							lp.set_max_pos( pos );
							curpositions.push_back( lp );
							break; //On s'arrète au premier
						}
						pos++;
					}
				}


				itp++;
			}//fin while( itp!= n->_lpos.end())


			//On ajoute à l'arbre et on "récurre", que si on est fréquent !
			if( curpositions.size() >= _fmin ) {

				prefixtree::node *nn = new prefixtree::node( *it );
				nn->_lpos = curpositions;
				n->compose( nn );

				//TODO sort!!
				nn->_lpos.sort();

				//Recursion :
				project(nn, sequence, items);
			}
		}
		it++;
	}

#if DEBUG > 3
	cout << "FIN PROJECTION : "; n->print_pattern(); cout << endl;
	cout << "^^^^^^^^^^^^^^^^" << endl;
#endif
}


