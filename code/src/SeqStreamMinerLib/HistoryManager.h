/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ agrocampus-ouest fr (remove space and add dots)
 */

/**
 * \file HistoryManager.h
 * \author Guyet Thomas, Inria
 * \brief Class to manage the history of frequent patterns into a long sequence
 * \version 0.3
 * \date 25 nov 2021
 */

#ifndef HISTORYMANAGER_H_
#define HISTORYMANAGER_H_

#include <list>
#include "postfixtree.h"
#include "debug.h"
#include "stream.h"


using std::list;

/**
 * \class HistoryManager
 * \brief Class to manage the pattern history in the stream
 * \author Guyet Thomas
 * \date 27/06/12 ajout des fonctions de mise à jour de l'historique par interval, les anciennes fonctions sont passées en deprecated
 */
class HistoryManager {
public:
	/**
	 * \brief Internal structure of timestamp intervals used to remove trivial matches in the patterns history
	 */
	typedef struct {
		int debut, fin;
	} Interval;

	/**
	 * \class HistoryManager::HistoryITNode
	 * \brief Node for an history tree
	 *
	 * \see HistoryManager::HistoryTree
	 */
	class HistoryITNode {
	public:
		HistoryITNode(int _val) : item(_val){};
		HistoryITNode() : item(-1){};
	public:
		int item;					//< item associated to the node (added to the parent)
		list<itemset> _pattern;		//< overall pattern represented by the node
		list<Interval> _intervals;	//< list of intervals of positions in which the pattern has been found
		list<int> _positions;		//< list of positions in which the pattern has been found
		list<int> _newPositions;	//< list of positions in which the pattern has been found but none of its children has been found
		list<HistoryITNode*> _S, _C;//< two lists of childrens : sequences and compositions
	};

	/**
	 * \class HistoryManager::HistoryTree
	 * \brief Hierarchical representation of the history of the positions found in a stream
	 *
	 * Nodes are HistoryManager::HistoryITNode. The tree structure is a sequence tree with two kinds of
	 * relationships : sequence between itemsets and composition of itemset.
	 *
	 * \see HistoryManager::HistoryITNode
	 */
	class HistoryTree {
	public:
		/**
		 * \brief Return the node corresponding to the itemset into the tree (designed by its root)
		 * \param itemset the looked for pattern
		 * \return return the node of the pattern
		 */
		HistoryITNode *findnode(list<itemset>& itemset);

		/**
		 * \brief getter
		 */
		const HistoryITNode* getroot() const {return &root;};
		HistoryITNode* getroot() {return &root;};

		/**
		 * \brief Compute the positions that are really new for a pattern
		 * \pre The tree must have been constructed. All node must have a new positions list equals to the positions list.
		 */
		void computeNewPositions();
	protected:
		HistoryITNode root; //< root of the history tree
	protected:
		/**
		 * \brief Recursive function to return the node of a pattern
		 * \param curnode
		 * \param pattern
		 */
		HistoryITNode *findnode(list<itemset>& itemset, HistoryITNode *curnode);

		/**
		 * \brief Recursive function to compute the _newPositions of the current node
		 *
		 * The function remove all positions of curnode from the list of the newpositions of parentnode
		 *
		 * \param curnode one of the children of the parent node
		 * \param parentnode node that is processed
		 */
		void computeNewPositions(HistoryITNode *parentnode, HistoryITNode *curnode);
	};

protected:

	/**
	 * \brief Internal structure of History based on instant
	 */
	typedef list< std::pair< list<itemset>, list<int> > > History;

	/**
	 * \brief Internal structure of History based on intervals
	 */
	typedef list< std::pair< list<itemset>, list<Interval> > > HistoryI ;

	/* The structure to save the history of frequent patterns
	 * Note that a map structure can not be used due to the some impossibility
	 */
	History _history;

	/* The structure to save the history of frequent patterns represented by intervals
	 * Note that a map structure can not be used due to the some impossibility
	 */
	HistoryI _historyI;

public:
	HistoryManager();
	virtual ~HistoryManager();

	/**
	 * \brief print an interval based history (without trivial matches)
	 * \param output output stream (cout by default)
	 *
	 * The function computes at each call the interval history and prints it.
	 * The <a>_history</a> has to be constructed using <a>update</a> function
	 * (and not the updateI</a> function).
	 *
	 * \see print_historyI(ostream &) const
	 * \deprecated
	 */
	void print(std::ostream & =cout) const;

	/**
	 * \brief print the history on stdout by default
	 * \param output output stream (cout by default)
	 *
	 * This function uses the <a>_history</a> data.
	 * \deprecated
	 */
	void print_history(std::ostream & output=std::cout) const;

	/**
	 * \brief print an interval based history (without trivial matches)
	 * \param output output stream (cout by default)
	 *
	 *
	 * This function uses the <a>_historyI</a> data. The <a>_historyI</a> has to be constructed using <a>updateI</a> function.
	 */
	void print_historyI(std::ostream & output=std::cout) const;


	/**
	 * \brief Update the _historyI data structure by adding a new record with the _cur_pos value to all itemset represented by the prefixtree n
	 * \param n prefixtree of the itemset to add to the history (must be frequents)
	 * \param cur_pos position (timestamp) to associate to the pattern
	 * \deprecated
	 */
	void update(const prefixtree::node *n, int cur_pos);
	/**
	 * \brief Update the _history data structure by adding a new record with the _cur_pos value to all itemset represented by the prefixtree n
	 * \param n prefixtree of the itemset to add to the history (must be frequents)
	 * \param cur_pos position (timestamp) to associate to the pattern
	 */
	void updateI(const prefixtree::node *n, int cur_pos);

	/**
	 * \brief Compare _history results
	 */
	bool cmp(const HistoryManager &hm) const;


	/**
	 * \brief Construct a tree of patterns associated with intervals
	 * \see HistoryManager::HistoryTree, HistoryManager::HistoryITNode
	 */
	HistoryTree history_tree() const;


	static HistoryTree *load(std::string &filename);

protected:
	/**
	 * \brief Construct an interval based history to suppress trivial matches
	 * \return History (with temporal intervals)
	 *
	 * For each itemset in the history, the function browses its history and put contigus
	 * timestamps into a unique interval.
	 *
	 * \see print(), Interval
	 * \deprecated
	 */
	HistoryI interval_history() const;


	/**
	 * \brief Recursive <a>_history</a> update function
	 * \param n prefixtree of the itemset to add to the history (must be frequents)
	 * \param cur_it current itemset to add constructed recursively
	 * \param cur_pos current position index
	 * \deprecated
	 */
	void update(const prefixtree::node *n, list<itemset> &cur_it, int cur_pos);
	/**
	 * \brief Recursive <a>_historyI</a> update function
	 * \param n prefixtree of the itemset to add to the history (must be frequents)
	 * \param cur_it current itemset to add constructed recursively
	 * \param cur_pos current position index
	 */
	void updateI(const prefixtree::node *n, list<itemset> &cur_it, int cur_pos);
};

#endif /* HISTORYMANAGER_H_ */
