/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file SinglePassSeqStream.h
 * \author Guyet Thomas, Inria
 * \version
 * \date 25 nov 2021
 */

#ifndef SINGLEPASSSEQSTREAM_H_
#define SINGLEPASSSEQSTREAM_H_

#include <vector>
#include <map>
#include <list>
#include <ostream>
#include "SinglePassTree.h"
#include "SequenceMiner.h"

using namespace std;

/**
 * \brief dedicated implementation of the single pass algorithm on sequence of items (not itemsets !)
 */
class SinglePassSeqStream : public SequenceMiner {
public:

protected:
	SinglePassTree _tree;   		//!< incremental prefix tree structure
	int _cur_pos;       			//!< current position in the data stream
	vector<itemset> _sequence;		//!< The mined sequence of itemset
	bool _output;					//!< false to disable all outputs in non-debug mode !
	unsigned int _maxdepth;			//!< maximum depth (infinite if null)
	unsigned int _gap_max;			//!< maximum gap constraint (none if 0)

public:
	/**
	 * \brief Default constructor
	 * \param ws the windows size
	 * \param fmin the frequency threshold (which is a number of item threshold here)
	 *
	 */
	SinglePassSeqStream(int ws, unsigned int fmin): SequenceMiner(ws,fmin), _cur_pos(0), _output(true), _maxdepth(0), _gap_max(0){};

	/**
	 * \brief Destructor
	 */
	~SinglePassSeqStream(){};

	/**
	 * \brief Launch the algorithm to extract frequent patterns in the stream
	 *
	 * \pre the stream must have been setup before launching.
	 * \see setstream
	 */
	void launch();

	/**
	 * \brief Setter
	 */
	void set_output(bool val=true) {_output=val;};
	void set_maxdepth(unsigned int val) {_maxdepth=val;};
	void set_maxgap(int val) {_gap_max=val;};
	int get_treesize() const {return _tree.size();};
	const SinglePassTree *get_tree() const {return &_tree;};

protected:
	/**
	 * \brief Removes positions from the children of n, and remove a child if its positions are empty
	 * \param n node to modify
	 * \param val the value to remove
	 * \param is itemset that will be added (required to detect quasi-frequents patterns)
	 * \param pqf (presque quasi-fréquent) means that the last itemset of the sequence n is a sub-itemset of is
	 *
	 * For each child of n, the function removes all the positions that starts with the value val.
	 * If the position list of a child is empty, then the node is destructed.
	 * Note that the position list of the node n is not modified, but only the position list of the children of n.
	 *
	 * Based on a monotony property, the function is recursive, the recursion is cutted while a children is unfrequent (node and all subsnodes removed) or if any position of a node holds val as first at the head,
	 *
	 * The remove function test if nodes holds enough positions, otherwise nodes are cutted.
	 * if _fmin==0 there is no pruning, nontheless, empty leaves with _fmin==0 are removed.
	 * \pre The size of _lpos must formally decrease with the deepness of the node (monotony property)
	 * \pre val must appeared only at the head of the positions in n
	 * \pre antimonotony of _lpos.size() wrt deep
	 * \post Completness : the algorithm prune all unfrequent patterns and ensure to keep all the others
	 * \post Correction : any frequent patterns are deleted
	 * \post antimonotony of _lpos.size() wrt deep
	 */
	void remove(SinglePassTree::node *n, int val =1);

	/**
	 * \brief Removes positions from root
	 * \param val the timestamp value to remove
	 *
	 * \see remove(SinglePassTree::node *, int)
	 */
	void remove(int val =1);

	/**
	 * \brief add the itemset s to the tree structure
	 * \param s itemset to add
	 *
	 * In the Phase 1, a tree structure is constructed from s,
	 * then (Phase 2), the tree structure is merge into all the
	 * node (from leaves to the root). Position of the tree are
	 * prefixed by the position of the merged nodes. And finally
	 * (Phase 3), the tree structure is merged to the root
	 * (whithout prefix).
	 *
	 *
	 * The function does not check if the size of position lists _lpos is lower than the frequency threshold. As a consequence, any nodes are pruned here.
	 *
	 * \pre each node of _tree has a _lpos whose size is greater than _fmin
	 * \pre antimonotony of _lpos.size() wrt deep
	 * \post each node of _tree has a _lpos whose size is greater than _fmin
	 * \post antimonotony of _lpos.size() wrt deep
	 * \post Correctness : each maximal pattern instances are taken into account in the tree
	 * \see _tree
	 * \see add(SinglePassTree::node *, SinglePassTree::node *)
	 */
	void add(itemset &s, vector<itemset> &sequence, int pos);


	/**
	 * \brief Phase 2 implementation
	 * \param ps tree to merge into T
	 * \param T tree in which ps is merged
	 * \pre descendants of T have been merged
	 *
	 * The function merges the node ps iterativelly on each node of T except quasi-frequent nodes
	 */
	void add(SinglePassTree::node *ps, SinglePassTree::node *T);

	/**
	 * \brief Construct a tree based on a simple itemset (no sequential relationship)
	 * \param s itemset
	 * \return the constructed tree
	 * \pre itemset must be ordered
	 * \see create_tree_rec
	 */
	SinglePassTree::node *create_tree(const itemset &s);

	/**
	 * assert that the whole tree has nodes with attribute _tocomplete at the value val !
	 */
	int assert_tocomplete(SinglePassTree::node *n, bool val);
};

#endif /* SINGLEPASSSEQSTREAM_H_ */
