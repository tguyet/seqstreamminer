/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file seqstream.cpp
 * \author Guyet Thomas, Inria
 * \version 0.4
 * \date 25 nov 2021
 */

/**
 * Penser à utiliser la commande "setenv OMP_NUM_THREADS 4" ou "export OMP_NUM_THREADS=4" pour définir le nombre de thread avant de lancer l'application en distribué
 */
#ifdef _OPENMP
#include <omp.h>
#include <valarray>
#endif

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <cassert>
#include <string>

#include "seqstream.h"
#include "debug.h"

using namespace std;

void seqstream::launch()
{
#ifdef TIME_PROBE
	clock_t start,end;

	fstream timefile( "times.txt", ios::out );
	if( timefile.fail() ) {
		cerr << "The TIME_PROBE file ('times.txt') can not be open !" << endl;
		exit(EXIT_FAILURE);
	}
	timefile << "pos time tree_size tree_leaves tree_depth"<<endl;
#endif

	if( _s==NULL ) {
		cerr << "No stream\n";
		return;
	}

	if(_fmin==0) {
		_fmin=1;
		cerr << "Warning : fmin was null ! it has been set to 1 to avoid errors"<<endl;
	}

#if DEBUG > 1
	cout << "   _fmin : "<<_fmin << "\n";
	cout << "   _ws : "<< _ws << "\n";
	cout << "   _mg : "<<_gap_max << "\n";
#endif

	//initialisation before starting the stream processing
	_cur_pos=0;
	_s->init();
	_tree.root()->clean();
	_sequence.clear();

	//bootstrap sur la première fenêtre : on ne fait que des ajouts
	while( !_s->isend() && _s->posid()!=_ws ) {
#ifdef TIME_PROBE
		start= clock();
#endif

		_sequence.push_back( _s->next() );

#if DEBUG > 1
		cout << "======================\n";
		cout << "   received " << _sequence.back() << "\n";
		cout << "======================\n";
#endif

		_cur_pos++;
		add( _sequence.back(), _sequence, 1 );

#if DEBUG > 2
		cout << " *** Do completion ***"<<endl;
#endif
		_tree.complete(_sequence, _tree, 1, _fmin, _gap_max);

		_history.updateI(_tree.root(), _cur_pos );

#if DEBUG > 2
		_tree.print();
#endif
		assert( _tree.assert_monotony() );

#if DEBUG < 1
		if( _output ) {
			printf("%.2lf%%", 100*(float)_cur_pos/(float)_s->size());
			fflush(stdout);
			printf("\r");
		}
#endif

#ifdef TIME_PROBE
		end = clock();
		timefile << _cur_pos << " " << (double)(end-start)/CLOCKS_PER_SEC << " " << _tree.size() << " " << _tree.leaves() << " " << _tree.depth() << std::endl;
#endif
	}

	while( !_s->isend() ) {
#ifdef TIME_PROBE
		start=clock();
#endif

#if DEBUG > 1
		cout << "======================\n";
		cout << "   deleted ("<< _cur_pos-_ws+1 <<") " << _sequence[0] << "\n";
		cout << "======================\n";
#endif

		//decalage de la sequence :
		for(int i=0; i<_ws-1; i++) {
			_sequence[i]=_sequence[i+1];
		}
		_sequence[_ws-1] = _s->next();
		//Mise à jour de la structure de données par suppression

		remove( _sequence[_ws-1], false, _cur_pos-_ws+1 );

#if DEBUG > 2
		_tree.print();
#endif
		assert( _tree.assert_monotony() );

#if DEBUG > 1
		cout << "======================\n";
		cout << "   received " << _sequence[_ws-1] << "\n";
		cout << "======================\n";
#endif
		//Mise à jour de la structure de données par ajout
		_cur_pos++;
		add( _sequence[_ws-1], _sequence, _cur_pos-_ws+1 );


#if DEBUG > 2
		cout << " *** Do completion ***"<<endl;
#endif
		_tree.complete(_sequence, _tree, _cur_pos-_ws+1, _fmin, _gap_max);

		_history.updateI(_tree.root(), _cur_pos );

#if DEBUG > 2
		_tree.print();
#endif
		assert( _tree.assert_monotony() );

#if DEBUG < 1
		if( _output ) {
			printf("%.2lf%%", 100*(float)_cur_pos/(float)_s->size());
			fflush(stdout);
			printf("\r");
		}
#endif

#ifdef TIME_PROBE
		end = clock();
		timefile << _cur_pos << " "<< (double)(end-start)/CLOCKS_PER_SEC << " " << _tree.size() << " " << _tree.leaves() << " " << _tree.depth() << std::endl;
#endif
	}


#if DEBUG < 1
	if( _output ) {
		printf("[COMPLETE]\n");
		fflush(stdout);
	}
#endif
#ifdef TIME_PROBE
	timefile.close();
#endif
}

/**
 * Quasifrequent node at level one are not take into account
 */
void seqstream::remove(const itemset &is, bool pqf, int val)
{
#ifdef _OPENMP
	int l =0, lSize =0;
	bool present;

	lSize = _tree.root()->_seqChilds.size();
	valarray<prefixtree::node*> items(lSize);

	list<prefixtree::node*>::iterator it = _tree.root()->_seqChilds.begin();
	while( it != _tree.root()->_seqChilds.end() ) {
		present = false;
		list<prefixtree::position>::iterator itp = (*it)->_lpos.begin();
		while( itp != (*it)->_lpos.end() ) {
			if( (*itp).front() == val ) {
				present = true;
				itp = (*it)->_lpos.erase( itp );
			} else {
				itp++;
			}
		}

		if( !present ) {
			it++;
			continue;
		}
		if( (*it)->_lpos.size() <_fmin ) {
			prefixtree::node *todelete = *it;
			it = _tree.root()->_seqChilds.erase( it );
			delete( todelete );
		} else {
			items[l] = (*it);
			l++;
			it++;
		}
	}

	assert( _tree.root()->_compoChilds.size()==0 );

	//On est obligé de recopier la référence pour OMP
	itemset isl=is;

	//! Parallelism schedule is dynamic to take into account unbalanced trees
	// The items variable is shared : direct access to elements using []
#pragma omp parallel for default(none) firstprivate(val, isl)  shared(items,l) schedule(dynamic)
	for (int i=0; i<l; i++)
		remove( items[i], isl, false, val );

#pragma omp barrier
#else //_OPENMP
	remove( _tree.root(), is, pqf, val);
#endif
}

/**
 * \pre val est la plus petite valeur utilisée dans les listes de position
 * \pre toutes les listes de position sont ordonnées et vérifient les contraintes MinOcc
 * \post toutes les listes de position sont ordonnées et vérifient les contraintes MinOcc
 */
void seqstream::remove(prefixtree::node *n, const itemset &is, bool pqf, int val)
{
#ifdef _OPENMP
#if DEBUG>3
#pragma omp critical(print)
	{
		printf("remove : thread id : %d\n", omp_get_thread_num());
	}
#endif
#endif

	list<prefixtree::node *>::iterator it = n->_seqChilds.begin();
	while( it != n->_seqChilds.end() ) {

#if DEBUG >4
		cout << "remove instances of ";(*it)->print_pattern();cout <<endl;
#endif

		//HACK : par les contraintes MinOcc, il ne peut exister qu'un seul élément avec comme début val
		//		 par contrainte d'ordre des listes de position, une position utilisant val est nécessairement positionné en début de liste
		list<prefixtree::position>::iterator itp = (*it)->_lpos.begin();
		if( (*itp).min_pos()==val ) {
			(*it)->_lpos.erase( itp );

#if DEBUG >4
			cout << "\t remove "<< (*itp) <<endl;
#endif
		} else {

			/** HACK OPTIM: si un élément de la séquence n'est
			 * présent dans aucune des positions de n, alors
			 * il ne le sera pas dans ses fils : on arrête !
			 */
			it++;
			continue;
		}
#if DEBUG >4
		if((*it)->_lpos.size()==_fmin-1) {
			(*it)->print_pattern();
			printf("quasi-frequent ?? %d ??\n", is.holds( (*it)->_s ));
		}
#endif


		//TODO vérifier avec les quasi-fréquents depuis l'ajouts de (*it)->_length>1

#if DEBUG >4
		cout << "\t remaining instances : " << (*it)->_lpos.size() << "/"<<_fmin <<endl;
#endif
#if ONEPASS
		if((*it)->_lpos.size()==0) {
			//On traite le cas des noeuds vides : il doivent être supprimés
#if DEBUG >4
			cout << "\t delete node" <<endl;
#endif
			prefixtree::node *todelete = *it;
			it = n->_seqChilds.erase( it );
			delete( todelete );
		}
		if( !((*it)->_s_parent!=NULL && (*it)->_s_parent->isroot()) && (*it)->_lpos.size() <_fmin-1 ) {
#else
		if( (*it)->_lpos.size() <_fmin-1 ) {
#endif

#if DEBUG >4
			cout << "\t delete node" <<endl;
#endif
			prefixtree::node *todelete = *it;
			it = n->_seqChilds.erase( it );
			delete( todelete );
#if ONEPASS
		} else if( !((*it)->_s_parent!=NULL && (*it)->_s_parent->isroot()) && (*it)->_lpos.size()==_fmin-1  ) {
#else
		} else if( (*it)->_lpos.size()==_fmin-1  ) {
#endif
#if QFREQ
			if( is.holds( (*it)->_s )) {
				//HERE, the node is quasi-frequent, we keep it
				(*it)->_quasi_frequent=true;
				remove(*it, is, true, val);
				it++;
			} else {
#endif
#if DEBUG >4
				cout << "\t delete pqf node" <<endl;
#endif
				prefixtree::node *todelete = *it;
				it = n->_seqChilds.erase( it );
				delete( todelete );
#if QFREQ
			}
#endif
		} else {
#if QFREQ
			remove(*it, is, is.holds( (*it)->_s ), val);
#else
			remove(*it, is, false, val);
#endif
			it++;
		}
	}

	it = n->_compoChilds.begin();
	while( it != n->_compoChilds.end() ) {
		//HACK : par les contraintes MinOcc, il ne peut exister qu'un seul élément avec comme début val
		//		 par contrainte d'ordre des listes de position, une position utilisant val est nécessairement positionné en début de liste
		list<prefixtree::position>::iterator itp = (*it)->_lpos.begin();
		if( (*itp).min_pos()==val ) {
			(*it)->_lpos.erase( itp );

#if DEBUG >4
				cout << "\t remove "<< (*itp) <<endl;
#endif
		} else {
			/** HACK OPTIM: si un élément de la séquence n'est
			 * présent dans aucune des positions de n, alors
			 * il ne le sera pas dans ses fils : on arrête !
			 */
			it++;
			continue;
		}

		if( (*it)->_lpos.size() < _fmin ) {
			prefixtree::node *todelete = *it;
			it = n->_compoChilds.erase( it );
			delete( todelete );
		} else if( pqf && (*it)->_lpos.size()==_fmin-1 ) {
			//HERE, the node is quasi-frequent, we keep it
#if QFREQ
			if(is.holds( (*it)->_s )) {
				(*it)->_quasi_frequent=true;
				remove(*it, is, true, val);
				it++;
			} else {
#endif
#if DEBUG >4
				cout << "\t delete pqf node" <<endl;
#endif
				prefixtree::node *todelete = *it;
				it = n->_compoChilds.erase( it );
				delete( todelete );
#if QFREQ
			}
#endif
		} else {
#if QFREQ
			remove(*it, is, pqf && is.holds( (*it)->_s ), val);
#else
			remove(*it, is, false, val);
#endif
			it++;
		}
	}
}

void seqstream::prune(prefixtree::node *n)
{
	list<prefixtree::node *>::iterator it = n->_seqChilds.begin();
	while( it != n->_seqChilds.end() ) {
		if( (*it)->_lpos.size() < _fmin ) {
			prefixtree::node *todelete = *it;
			it = n->_seqChilds.erase( it );
			delete( todelete );
		} else {
			prune(*it);
			it++;
		}
	}

	it = n->_compoChilds.begin();
	while( it != n->_compoChilds.end() ) {
		if( (*it)->_lpos.size() < _fmin ) {
			prefixtree::node *todelete = *it;
			it = n->_compoChilds.erase( it );
			delete( todelete );
		} else {
			prune(*it);
			it++;
		}
	}
}

void seqstream::create_tree_rec(const itemset &s, prefixtree::node *pred, itemset::const_iterator &it, const prefixtree::position &p)
{
	itemset::const_iterator jt=it;
	jt++;
	while( jt != s.end() ) {
		prefixtree::node *nn=new prefixtree::node(*jt);
		//Ajout de la même position !
		nn->_lpos.push_back( p );
		pred->compose( nn );

		create_tree_rec(s, nn, jt, p);

		jt++;
	}
}

/**
 * \pre s doit être ordonné
 */
prefixtree::node *seqstream::create_tree(const itemset &s) {

	prefixtree::node *ps=new prefixtree::node(-1);

	//Attention : les itemset doivent être ordonnés (ordre lexicographique)
	itemset::const_iterator it=s.begin();
	while( it!= s.end() ) {
		//Ajout d'un noeud pred, successeur de ps
		prefixtree::node *pred=new prefixtree::node(*it);

		//Ajout d'une position à pred
		prefixtree::position p(_cur_pos);
		pred->_lpos.push_back( p );
		ps->append( pred );

		create_tree_rec(s, pred, it, p);
		it++;
	}
	return ps;
}

/**
 * TODO : TREE_REDUCTION does no work with ONE_PASS
 * \pre _fmin>1 to be complete if ONE_PASS !
 *
 * In case of ONE_PASS : simple items are proceeced before recuring on composition edges
 */
void seqstream::simplify_tree(prefixtree::node *ps, prefixtree::node *T, vector<itemset> &sequence, int pos)
{
	list<prefixtree::node *>::iterator it;
	list<prefixtree::node *>::iterator itT;


	if( T==NULL ) {
		//ICI le parent n'a pas de correspondance fréquente dans l'arbre T, mais sa completion le rend fréquent
		// On teste maintenant récursivement sans info existante
		// On regarde nécessairement des compositions, mais pas de successions
		it=ps->_compoChilds.begin();
		while( it != ps->_compoChilds.end() ) {
			(*it)->set_tocomplete(true);
			(*it)->_lpos.pop_back();
			(*it)->complete_comp(sequence, _tree, pos);
			if( (*it)->_lpos.size() < _fmin ) {
				//On ne conserve pas le noeud
				prefixtree::node *todelete = *it;
				it = ps->_compoChilds.erase( it );
				delete( todelete );
				return;
			} else {
				//On conserve le noeud
				simplify_tree(*it, NULL, sequence, pos);
			}

			//On remet la position finale :
			(*it)->_lpos.clear();
			prefixtree::position p(_cur_pos);
			(*it)->_lpos.push_back( p );
			it++;
		}
		return;
	}


	bool found;
	if( ps->_s==-1 ) {

#if ONEPASS
		//Here, it's a breadth-first parcourt
		list< pair<prefixtree::node *, prefixtree::node *> > toproceed;
#endif

		//Dans le cas de la racine, on ne regarde que les successions
		it=ps->_seqChilds.begin();
		while( it != ps->_seqChilds.end() ) {
			itT= T->_seqChilds.begin();
			found=false;
			while( itT != T->_seqChilds.end() ) {
				if( (*itT)->_s == (*it)->_s ) {
					found=true;
					break;
				}
#if ORDERNODES
				else if ( (*itT)->_s > (*it)->_s ) { //HACK uses the order of _seqChild
					break;
				}
#endif
				itT++;
			}
			if( found ) {
				//ici *it correspond à *itT
#if ONEPASS

				if( (*itT)->_lpos.size()>=_fmin-1 ) {
					//On ajoute ces noeuds aux noeuds à traiter !
					toproceed.push_back( make_pair(*it, *itT) );
				} else {
					//On ajoute la position courante à la liste pour compléter le noeud
					prefixtree::position p(_cur_pos);
					(*itT)->_lpos.push_back(p);
					//le noeud doit être supprimé, car non fréquent
					prefixtree::node *todelete = *it;
					it = ps->_seqChilds.erase( it );
					delete( todelete );
					continue;
				}
#else
				//ici *it correspond à *itT
				simplify_tree(*it, *itT, sequence, pos);
#endif //ONEPASS
			} else {
				//Here, it's a depth-first parcourt
#if ONEPASS
				//Here : s'il n'existe pas, c'est qu'il n'y avait aucune occurrence de celui-ci avant !
				//	-> on créé la liste pour le maintient à jour des itemsets de taille 1, mais on ne
				//	poursuit pas la récursion (hypothèse _fmin>1)
				//

#if DEBUG > 3
				cout  << "create node : " << (*it)->_s <<endl ;
#endif
				prefixtree::node *n=new prefixtree::node( (*it)->_s );
				T->append(n);

				//On ajoute la position courante à la liste pour compléter le noeud
				prefixtree::position p(_cur_pos);
				n->_lpos.push_back(p);

				//on supprime le noeud de ps
				prefixtree::node *todelete = *it;
				it = ps->_seqChilds.erase( it );
				delete( todelete );
				continue;

#else //ONEPASS
				(*it)->set_tocomplete(true);
				(*it)->_lpos.pop_back();
				(*it)->complete_succ(sequence, _tree, pos, 1/*gapmax*/);
				if( (*it)->_lpos.size() < _fmin ) {
					//On ne conserve pas le noeud
					prefixtree::node *todelete = *it;
					it = ps->_seqChilds.erase( it );
					delete( todelete );
					continue;
				} else {
					//On conserve le noeud, et on regarde plus loin
					simplify_tree(*it, NULL, sequence, pos);
				}
#endif //ONEPASS
			}

			//On remet la position finale :
			(*it)->_lpos.clear(); //TODO mineur : intérêt de cette ligne ??
			prefixtree::position p(_cur_pos);
			(*it)->_lpos.push_back( p );
			it++;
		}

#if ONEPASS
		//Recursion !
		list< pair<prefixtree::node *, prefixtree::node *> >::iterator itp=toproceed.begin();
		while( itp!=toproceed.end() ) {
			simplify_tree( (*itp).first, (*itp).second, sequence, pos);
			itp++;
		}
#endif

	} else {
		//Dans le cas général, il n'y a que des nodes par composition pour les noeuds normaux ! Aucun par succession
		it=ps->_compoChilds.begin();
		while( it != ps->_compoChilds.end() ) {
			itT= T->_compoChilds.begin();
			found=false;
			while( itT != T->_compoChilds.end() ) {
				if( (*itT)->_s == (*it)->_s ) {
					found=true;
					break;
				}
#if ORDERNODES
				else if ( (*itT)->_s > (*it)->_s ) { //HACK uses the order of _seqChild
					break;
				}
#endif
				itT++;
			}
			if(found) {
				//si on l'a trouvé, alors on va voir plus loin, alors *it est fréquent et doit être conservé
				ps->_lpos=T->_lpos; //copies des listes d'instances qui pourront être utilisées pour les sous-itemsets
				simplify_tree(*it, *itT, sequence, pos);
			} else {
#if ONEPASS

#else
				//ICI it n'a pas été trouvé, il faut donc le compléter pour en savoir plus ...
				(*it)->set_tocomplete(true);
				//We complete the positions list (à partir de celles de T)
				(*it)->_lpos.pop_back();
				(*it)->complete_comp(sequence, _tree, pos);
				if( (*it)->_lpos.size() < _fmin ) {
					//We doesn't we the node
					prefixtree::node *todelete = *it;
					it = ps->_compoChilds.erase( it );
					delete( todelete );
					continue;
				} else {
					//We keep the node
					simplify_tree(*it, NULL, sequence, pos);
				}
#endif //ONEPASS
			}

			//On ne conserve que la position finale
			while( (*it)->_lpos.size()>1 ) {
				(*it)->_lpos.pop_front();
			}
			it++;
		}
	}
}

/**
 * Modification 21/02/2012, T. Guyet : add the merging depth limit
 * Modification 28/06/2012, T. Guyet : ps are not merged to the quasi_frequent nodes. quasi_frequent node may be used exactly as regular node to merge instances, then they will not be to be completed.
 */
void seqstream::add(prefixtree::node *ps, prefixtree::node *T)
{
#ifdef _OPENMP
#if DEBUG > 3
#pragma omp critical(print)
	{
		printf("Add from thread = %d\n", omp_get_thread_num());
		T->print(0);
	}
#endif
#endif

	prefixtree::node *nc;
	list<prefixtree::node *> nodes;
	T->nodes(nodes);

	/*! HACK : Il faut commencer par remplir par la fin de la liste (qui correspond
 aux noeud les plus profonds)
 Dans le cas inverse, on ajoute des positions intermédiaires dans le sous-arbre
 qui sont réutilisés à tort dans la suite des copies sur les noeuds inférieurs
	 */

	list<prefixtree::node *>::reverse_iterator itn = nodes.rbegin();
	while( itn != nodes.rend() ) {

#if QFREQ
		//Les noeuds quasi-fréquents ne sont pas mergés
		if( (*itn)->_quasi_frequent ) {
#if DEBUG > 4
#pragma omp critical(print)
			{
				cout << "quasi-frequent node : not merged\n";
				(*itn)->print(0);
			}
#endif
			(*itn)->_quasi_frequent=false;
			itn++;
			continue;
		}
#endif

		//Copie du noeud racine sur le noeud (*itn) (qui ne peut pas être la racine)
		//#pragma omp single //protection de la copie lors de la parallelisation (? même si que en lecture !!)
		// si on utilise ce single, il y a un interblocage !
		nc = new prefixtree::node( ps );

		//We add the last prefix to the positions of nc
		if( !(*itn)->_lpos.empty() ) {
			nc->prefix( (*itn)->_lpos.back(), (*itn)->depth(), (*itn)->length() );
		}

#if DEBUG > 4
#pragma omp critical(print)
		{
			cout << "prefixed tree : \n";
			nc->print(0);
			cout << "finprefixed tree : \n";
		}
#endif

		//On fusionne cette copie
		(*itn)->merge( nc, _gap_max, _maxdepth, _nocompo );

#if DEBUG > 4
#pragma omp critical(print)
		{
			cout << "resultat prefixed tree : \n";
			_tree.print();
			cout << "resultat finprefixed tree : \n";
		}
#endif
		delete( nc );
		itn++;
	}
}

void seqstream::add(itemset &s, vector<itemset> &sequence, int pos)
{
	assert( _tree.assert_tocomplete(false) ); //on vérifie que les attributs _tocomplete sont bien =false

	//Phase 1 : on créé un prefixtree ps à partir de l'itemset

	unsigned int taille = s.size();
	s.sort(); //< pour être sûr de la pré-condition !
	s.unique();
	if( taille != s.size() ) {
		cerr << "Warning : redundant items in an itemset have been removed"<<endl;
	}
	prefixtree::node *ps = create_tree(s);

#if DEBUG > 3
	cout <<"------- Add itemset tree -------\n";
	ps->print(0);
	cout << endl;
#endif

#if TREEREDUCE
#if DEBUG > 3
	cout <<"=== TREE REDUCTION ===\n";
#endif
	simplify_tree(ps, _tree.root(), sequence, pos);
#if DEBUG > 3
	cout <<"------- Simplified itemset tree -------\n";
	ps->print(0);
	cout << endl;
#endif
#endif



#ifdef _OPENMP
	//Phase 2 : parallelize only the processing of the childs of the root
	// to ensure that the merging starts from leaves.
	int l =0, lSize =0;

	lSize = _tree.root()->_seqChilds.size();
	valarray<prefixtree::node*> items(lSize);

	list<prefixtree::node*>::iterator it = _tree.root()->_seqChilds.begin();
	while( it != _tree.root()->_seqChilds.end() ) {
		items[l] = (*it);
		l++;
		it++;
	}
	assert( _tree.root()->_compoChilds.size()==0 );

#if DEBUG > 3
	cout << "======== PARALLEL ===========\n";
	_tree.root()->print(0);
#endif

	// Schedule=dynamic to take into account unbalanced tree
#pragma omp parallel for default(none) shared(ps, items, l) schedule(dynamic)
	for (int i=0; i<l; i++)
		add(ps, items[i]);

#pragma omp barrier

	//Phase 3 : on fait une fusion à la racine (pas de prefixage à faire, car on est à la racine)
	_tree.root()->merge( ps, _gap_max, _maxdepth, _nocompo );
#if DEBUG > 3
	cout <<"Merging result:\n";
	_tree.print();
#endif

#else //pas _OPENMP

	//Phase 2 and 3 : on ajoute systématiquement une copie de ps sous chaque noeud de l'arbre principal
	add(ps, _tree.root());

#if DEBUG > 3
	cout <<"Copying result:\n";
	_tree.print();
#endif

#endif//pragma omp parallel

	//ps n'est plus utile : on le delete
	delete(ps);
#if DEBUG > 3
	cout << "-----\n\n";
#endif
}




