/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */


#include "SequenceFinder.h"
#include "stream.h"
#include <iostream>
#include <string>
#include <list>

using namespace std;

int main(int argc, char* argv[])
{
	stream s;
	s.push_back( itemset(1) );
	s.push_back( itemset(3) );
	s.push_back( itemset(2) );
	s.push_back( itemset(3) );
	s.push_back( itemset(4) );
	s.push_back( itemset(2) );


	list<itemset> is;
	is.push_back( itemset(3) );
	is.push_back( itemset(2) );


	list<list <int> > res=SequenceFinder::find(s, is);

	for(list<list <int> >::iterator i=res.begin(); i!=res.end(); i++) {
		for(list<int>::iterator j=(*i).begin(); j!=(*i).end(); j++)
			cout << *j <<",";
		cout << "\n";
	}

	return 0;
}
