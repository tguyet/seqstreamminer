/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file postfixtree.cpp
 * \author Guyet Thomas, Inria
 * \version 0.5
 * \date 25 nov 2021
 */

#include "postfixtree.h"
#include "debug.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cassert>

#ifdef _OPENMP
#include <omp.h>
#include <valarray>
#endif


int prefixtree::position::operator==(const position &p) const {
	return _min_pos==p.min_pos() && _max_pos==p.max_pos();
}

int prefixtree::position::operator==(position &p) {
	return _min_pos==p.min_pos() && _max_pos==p.max_pos();
}

int prefixtree::position::operator<(const position &p) const {
	if( _min_pos==p.min_pos() ) {
		return _max_pos<p.max_pos();
	} else {
		return _min_pos<p.min_pos();
	}
}

int prefixtree::position::operator<(position &p) {
	if( _min_pos==p.min_pos() ) {
		return _max_pos<p.max_pos();
	} else {
		return _min_pos<p.min_pos();
	}
}

void prefixtree::position::prefix(const position &p)
{
	_min_pos=p.min_pos();
}

void prefixtree::position::postfix(const position &p)
{
	_max_pos=p.max_pos();
}

ostream& operator<< (ostream & os, const prefixtree::position & is)
{
	os << "(" << is.min_pos() << ", " << is.max_pos() << ")";
	return os;
}

prefixtree::node::node(const node *n): _s(n->_s), _c_parent(n->_c_parent), _s_parent(n->_s_parent), _tocomplete(n->_tocomplete), _depth(n->_depth), _length(n->_length)
#if QFREQ
, _quasi_frequent(n->_quasi_frequent)
#endif
{
	list<node *>::const_iterator it = n->_seqChilds.begin();
	while( it!= n->_seqChilds.end() ) {
		append( new node(*it) );
		it++;
	}
	it = n->_compoChilds.begin();
	while( it!= n->_compoChilds.end() ) {
		compose( new node(*it) );
		it++;
	}

	//recopie de la liste des positions
	_lpos=n->_lpos;
}

prefixtree::node::~node()
{
	list<node *>::iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		delete( *it );
		it++;
	}
	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		delete( *it );
		it++;
	}
}

void prefixtree::node::print_pattern(std::ostream & of) const {
	if(_s_parent!=NULL) {
		_s_parent->print_pattern(of);
		of << ">" << _s;
	} else if (_c_parent!=NULL) {
		_c_parent->print_pattern(of);
		of << "-" << _s;
	}
}

list<itemset> prefixtree::node::pattern() const
{
	list<itemset> parentpattern;
	if(_s_parent!=NULL) {
		parentpattern=_s_parent->pattern();
		parentpattern.push_back( itemset(_s) );
	} else if (_c_parent!=NULL) {
		parentpattern=_c_parent->pattern();
		parentpattern.back().push_back(_s);
	}
	return parentpattern;
}

void prefixtree::node::print(int pos, std::ostream & of) const {
	for(int i=0; i<pos; i++) of << " ";
	list<itemset>::const_iterator itpat;
	of << "[";
	print_pattern(of);
	of <<"] ("<< _seqChilds.size() + _compoChilds.size() << ")("<< _depth<< ")("<< _length<< ") : {";
	list<position>::const_iterator itp=_lpos.begin();
	while(itp!=_lpos.end()) {
		of << (*itp);
		of << ", ";
		itp++;
	}
	of << "}";
	if( _tocomplete ) of << " *";
#if QFREQ
	if( _quasi_frequent ) of << " #";
#endif
	of << endl;

	pos++;
	list<node *>::const_iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		of << "f";(*it)->print(pos,of);
		it++;
	}
	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		of<< "c";(*it)->print(pos, of);
		it++;
	}
	pos = pos - 1;
}

void prefixtree::node::print_instances(std::ostream & of) const {
	list<itemset>::const_iterator itpat;
	of << "[";
	print_pattern(of);
	of << "]" << endl;

	list<position>::const_iterator itp=_lpos.begin();
	while(itp!=_lpos.end()) {
		of << (*itp) << endl;
		itp++;
	}
	//of << "---" << endl;

	list<node *>::const_iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		(*it)->print_instances(of);
		it++;
	}
	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		(*it)->print_instances(of);
		it++;
	}
}

/**
 * \see PSStream
 */
void prefixtree::node::prune(unsigned int fmin) {

	list<prefixtree::node *>::iterator it = _seqChilds.begin();
	while( it != _seqChilds.end() ) {
		if( (*it)->_lpos.size() >= fmin || (_s_parent && !_s_parent->isroot()) ) {
			(*it)->prune(fmin);
			it++;
		} else {
			prefixtree::node *todelete = *it;
			it = _seqChilds.erase( it );
			delete( todelete );
		}
	}

	it = _compoChilds.begin();
	while( it != _compoChilds.end() ) {
		if( (*it)->_lpos.size() < fmin ) {
			prefixtree::node *todelete = *it;
			it = _compoChilds.erase( it );
			delete( todelete );
		} else {
			(*it)->prune(fmin);
			it++;
		}
	}
}

void prefixtree::node::nodes(list<node *> &N)
{
	N.push_back(this);
	list<node *>::const_iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		(*it)->nodes(N);
		it++;
	}
	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		(*it)->nodes(N);
		it++;
	}
}

void prefixtree::node::clean()
{
	list<node *>::const_iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		delete(*it);
		it++;
	}
	_seqChilds.clear();
	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		delete(*it);
		it++;
	}
	_compoChilds.clear();
	_lpos.clear();
	_tocomplete=false;
}


int prefixtree::node::size() const {
	if(_seqChilds.size()==0 && _compoChilds.size()==0) {
		//C'est une feuille
		return 0;
	} else {
		//C'est un noeud normal
		int size=_seqChilds.size()+_compoChilds.size();
		list<node *>::const_iterator it = _seqChilds.begin();
		while( it!= _seqChilds.end() ) {
			size+=(*it)->size();
			it++;
		}
		it = _compoChilds.begin();
		while( it!= _compoChilds.end() ) {
			size+=(*it)->size();
			it++;
		}
		return size;
	}
}

int prefixtree::node::leaves() const {
	if(_seqChilds.size()==0 && _compoChilds.size()==0) {
		//It's a leaf
		return 1;
	} else {
		//C'est un noeud normal
		int nb=0;
		list<node *>::const_iterator it = _seqChilds.begin();
		while( it!= _seqChilds.end() ) {
			nb+=(*it)->leaves();
			it++;
		}
		it = _compoChilds.begin();
		while( it!= _compoChilds.end() ) {
			nb+=(*it)->leaves();
			it++;
		}
		return nb;
	}
}


int prefixtree::node::maxdepth() const{
	if(_seqChilds.size()==0 && _compoChilds.size()==0) {
		//C'est une feuille
		return 0;
	} else {
		//C'est un noeud normal
		int maxdepth=0;
		list<node *>::const_iterator it = _seqChilds.begin();
		while( it!= _seqChilds.end() ) {
			int nodedepth=(*it)->maxdepth();
			maxdepth=(nodedepth>maxdepth?nodedepth:maxdepth);
			it++;
		}
		it = _compoChilds.begin();
		while( it!= _compoChilds.end() ) {
			int nodedepth=(*it)->maxdepth();
			maxdepth=(nodedepth>maxdepth?nodedepth:maxdepth);
			it++;
		}
		return maxdepth+1;
	}
}



void prefixtree::node::compose(itemset *its) {
	itemset::iterator it = its->begin();
	while( it!= its->end() ) {
		node *nn=new prefixtree::node(*it);
		compose( nn );
		it++;
	}
}

#if ORDERNODES
static bool node_cmp(prefixtree::node * lhs, prefixtree::node * rhs)
{
	return *lhs < *rhs;
}
#endif

void prefixtree::node::append(node *n) {
	n->set_s_parent(this);
	_seqChilds.push_back(n);

#if ORDERNODES
	_seqChilds.sort(node_cmp);
#endif
}

void prefixtree::node::compose(node *n) {
	n->set_c_parent(this);
	_compoChilds.push_back(n);
#if ORDERNODES
	_compoChilds.sort(node_cmp);
#endif
}


void prefixtree::node::currentitemset(itemset &is, node **n) {
	if( _c_parent!=NULL) {
		_c_parent->currentitemset(is, n);
		is.push_back(_s);
	} else if( _s_parent!=NULL ) {
		is.push_back(_s);
		if(n!=NULL) *n=_s_parent;
	}
}

itemset prefixtree::node::currentitemset() {
	itemset is;
	node *currentnode=this;
	is.push_front( currentnode->_s );
	while( currentnode->_s_parent==NULL ) {
		if( currentnode->_c_parent==NULL ) return is; //root
		currentnode=currentnode->_c_parent;
		is.push_front( currentnode->_s );
	}
	return is;
}

bool prefixtree::node::isfirstitemset() {
	if( _c_parent!=NULL) {
		return _c_parent->isfirstitemset();
	} else if( _s_parent!=NULL ) {
		if( _s_parent->isroot() ) {
			return true;
		} else {
			return false;
		}
	}
	return false;
}

itemset prefixtree::node::previousitemset() {
	itemset is;
	node *currentnode=this;
	while( currentnode->_s_parent==NULL ) {
		if( currentnode->_c_parent==NULL ) return is;
		currentnode=currentnode->_c_parent;
	}
	currentnode=currentnode->_s_parent;
	//currentnode est ici le dernier item de l'itemset precedent !
	is.push_front( currentnode->_s );
	while( currentnode->_s_parent==NULL ) {
		if( currentnode->_c_parent==NULL ) return is;
		currentnode=currentnode->_c_parent;
		is.push_front( currentnode->_s );
	}
	return is;
}

/**
 * <ul> modification 27/02 : add the computation of depth and length
 */
void prefixtree::node::prefix(const prefixtree::position &prefix, int nodedepth, int nodelength)
{
#if DEBUG > 4
	cout << "prefix : "<<prefix << endl;
#endif

	if( !_lpos.empty() ) {
		list<position> newpos;
		list<position>::iterator itp= _lpos.begin();
		while( itp != _lpos.end() ) {
			position p=(*itp);
			p.prefix( prefix );
			newpos.push_back( p );
			itp++;
		}
		_lpos=newpos;
	}

	_length+=nodelength;
	_depth+=nodedepth;

	//Recursion
	list<node *>::iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		(*it)->prefix( prefix, nodedepth, nodelength);
		it++;
	}
	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		(*it)->prefix( prefix, nodedepth, nodelength);
		it++;
	}
}


/**
 *
 * Modification of 21/02/2012 (T. Guyet) :<ul>
 * <li>add maxdepth parameters to cut the tree size by disable merge on deepest nodes
 * <li>add nocompo parameters to disable the merge with composition links
 * </ul>
 *
 * Modification of 12/06/2012 (T. Guyet) :<ul>
 * <li> add the gap_max parameter to take into account the gap max constraint
 * <li> !! parameters was not passed in the recursive call, though that default parameters was used !! this mistacke has been corrected
 * </ul>
 */
void prefixtree::node::merge(node *T, unsigned int gap_max, int maxdepth, bool nocompo)
{
	assert( T->_s==-1 || _s==T->_s );

#if DEBUG > 4
	cout << "\t MERGE "; print_pattern(); cout << endl;
#endif

	if( T->_s!=-1 ) { // Si ce n'est pas un noeud racine, alors
		//Fusion des positions du noeud courant (sans répétition)
		list<prefixtree::position>::iterator itp2;
		list<prefixtree::position> toAdd; //Liste des éléments de T._lpos à ajouter à _lpos : uniquement ceux pour lesquelles il n'existe aucune exemple qui a le même préfixe de taille size()-1.
		itp2= T->_lpos.begin(); //itp2 est une position à ajouter dans la liste
		while( itp2 != T->_lpos.end() ) {
			if( _length>=2 ) {
				// Dans le cas des motifs de taille >=2, il ne faut pas ajouter une
				// instance qui briserait la condition 3
				// => la première condition ci-dessous (pas triviale) fonctionne dans le cas
				// des "motifs doublons" !!
				// => la seconde condition correspond à l'ajout d'une contrainte de gap maximum entre
				// les itemsets de l'instance

				if( _lpos.back().max_pos() <= (*itp2).min_pos() && (gap_max==0 || (*itp2).max_pos() <= (*itp2).min_pos()+(int)gap_max)) {
					toAdd.push_back(*itp2);
				}
#if DEBUG > 3
				else {
					print_pattern();
					cout << " : NOT MERGED ";
					cout << "at pos " << _lpos.back() << " try " << (*itp2);
					cout << endl;
				}
#endif
			} else {
				// => contrainte de gap maximum entre
				// les itemsets de l'instance
				if( gap_max==0 || (*itp2).max_pos() <= (*itp2).min_pos()+(int)gap_max ) {
					toAdd.push_back(*itp2);
				}
			}
			itp2++;
		}
		_lpos.merge(toAdd); //! HACK Attention : ceci merge les listes de positions et non un appel récursif
	}

	//ICI on stoppe les merge lorsque la profondeur de l'arbre atteind la limite
	if( maxdepth && maxdepth<=_depth ) {
		return;
	}

	//Récursion sur les relations de séquences
	list<prefixtree::node*>::iterator it1, it2;
	it2= T->_seqChilds.begin();
	while( it2 != T->_seqChilds.end() ) {
		bool found = false;
		it1= _seqChilds.begin();
		while( it1 != _seqChilds.end() ) {
			if( *(*it1) == *(*it2) ) {
				found=true;
				break;
			}
#if ORDERNODES
			else if ( (*it1)->_s > (*it2)->_s ) { //HACK uses the order of _seqChild
				break;
			}
#endif
			it1++;
		}
		prefixtree::node *nn=NULL;
		if( found ) {
			//ICI, le contenu du fils est mergé avec le contenu du fils de r2 (correspondant)
			nn=(*it1);

			//Et on continue ...
			nn->merge( *it2, gap_max, maxdepth, nocompo);
		} else {
			// ICI, le contenu de r2 n'existe pas : on l'ajoute simplement
			// nn contient des positions (copiées de *it2) qui ont déjà été préfixées
			// lors de l'appel initial de brachement de l'arbre (cf seqstream.cpp,
			// fonction add(itemset s))
			nn=new prefixtree::node( *it2 );

			//Indique d'aller rechercher les autres instances de nn dans W
			nn->set_tocomplete();
			append( nn );
		}
		it2++;
	}

	if( nocompo ) {
		return;
	}

	//Récursion sur les relations de composition d'itemset
	it2= T->_compoChilds.begin();
	while( it2 != T->_compoChilds.end() ) {

		assert( (*it2)->_s != T->_s ); //< On ne doit pas composer un motif avec lui même (assertion peut être rendu fausse à cause d'un jeu de donnée d'entrée pathétique (e.g. "(bb)").

		bool found = false;
		it1= _compoChilds.begin();
		while( it1 != _compoChilds.end() ) {
			if( (*it1)->_s == (*it2)->_s ) {
				found=true;
				break;
			}
#if ORDERNODES
			else if ( (*it1)->_s>(*it2)->_s ) { //HACK uses the order of _compoChild
				break;
			}
#endif
			it1++;
		}
		prefixtree::node *nn=NULL;
		if( found ) {
			//ICI, le contenu du fils de r1 est remplacé par le contenu du fils de de r1 fusionné avec celui de r2 (correspondant)
			nn=(*it1);
#if DEBUG > 4
			cout << "\t\tC merge : "; (*it2)->print_pattern(); cout << endl;
#endif

			//Et on continue ...
			nn->merge( *it2, gap_max, maxdepth, nocompo );
		} else {

#if DEBUG > 4
			cout << "\t\tC create : "; (*it2)->print_pattern(); cout << endl;
#endif
			//ICI, le contenu de r2 n'existe pas dans r1 : on l'ajoute simplement
			// nn contient des positions (copiées de *it2) qui ont déjà été préfixées
			// lors de l'appel initial de brachement de l'arbre (cf seqstream.cpp,
			// fonction add(itemset s))
			nn=new prefixtree::node(*it2);

			//Indique d'aller rechercher les autres instances de nn dans W :
			nn->set_tocomplete();
			compose( nn );
		}
		it2++;
	}
}


void prefixtree::node::set_tocomplete(bool val) {
	_tocomplete=val;
	list<node *>::const_iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		(*it)->set_tocomplete(val);
		it++;
	}
	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		(*it)->set_tocomplete(val);
		it++;
	}
}

/*
 * <ul>
 * <li> 12/06/2012 (T. Guyet) : add the gap_max parameter to take into account the gap max constraint
 * <li> 18/12/2012 (T. Guyet) : add ONEPASS code parts + function profile modification
 * </ul>
 */
void prefixtree::node::complete_succ(vector<itemset> &sequence, const prefixtree &tree, int pos, unsigned int gap_max)
{
	if( _s_parent->isroot() ) {
		//ICI _s_parent est la racine de l'arbre: on cherche tous les singletons
#if ONEPASS
		//ICI, c'est un strict ONEPASS : il n'y a rien à faire par ce qu'on n'a jamais supprimé les motifs de taille 1
#if DEBUG > 3
		cout<< "\t 1-itemset : nothing to do " <<endl;
#endif
#else//else !ONEPASS

		// On traite la séquence à l'envers pour avoir _lpos ordonné !
		vector<itemset>::reverse_iterator is=sequence.rbegin();

		int i=sequence.size();
		for( ; is!=sequence.rend(); is++) {
			for(itemset::iterator itis=(*is).begin(); itis!= (*is).end(); itis++) {
				if( (*itis)==_s ) {
					position p( i+pos-1 );

					//HACK : on ajoute au début car on parcours la sequence à l'envers :
					_lpos.push_front( p );
					break; //Ca ne sert à rien de parcourir le reste de l'itemset
				} else if( (*itis)>_s ) break; //Ca ne sert à rien de parcourir le reste de l'itemset (ordonné)
			}
			i--;
		}
#endif//end ONEPASS
#if DEBUG > 3
		cout<< "\t 1-itemset : " << _lpos.size() <<endl;
#endif
		assert( assert_order() );
#if ONEPASS
	} else if( _length==2 ) {
		// ICI le noeud correspond à un motif de taille 2
		list<itemset> complementar;
		itemset ist = _s_parent->currentitemset();
		complementar.push_back( itemset(_s) );

		#if DEBUG > 4
			cout<< "\t S completed length 2 pattern : complementar pattern : " << _s << endl;
		#endif

		list<position> complementar_positions = tree.get_positions(complementar); //get positions
		if( !complementar_positions.empty() ) {
			_lpos=cat_positions(_s_parent->_lpos, complementar_positions, ist.holds(_s));
			#if DEBUG > 4
			cout<< "\t S completed : #original " << _s_parent->_lpos.size() << ", #complementar " << complementar_positions.size() <<endl;
			#endif
		} else {
			#if DEBUG > 4
			cout<< "\t S completed : no complementar occurrences" <<endl;
			#endif
		}

#endif
	} else {
		// !_s_parent->isroot() : ICI _s_parent n'est pas la racine de l'arbre
		// (il existe un itemset précédent l'itemset de l'item courant dans le motif)

#if ONEPASS
		list<itemset> complementar;
		itemset ist = _s_parent->currentitemset();
		complementar.push_back( ist );
		complementar.push_back( itemset(_s) );

		#if DEBUG > 4
			cout<< "\t S completed : complementar pattern : ";
			list<itemset>::iterator itcp=complementar.begin();
			while(itcp!=complementar.end()) {
				cout << *itcp <<">";
				itcp++;
			}
			cout << endl;
		#endif

		list<position> complementar_positions = tree.get_positions(complementar);
		if( !complementar_positions.empty() ) {
			_lpos=merge_positions(_s_parent->_lpos, complementar_positions);
			#if DEBUG > 4
			cout<< "\t S completed : #original " << _s_parent->_lpos.size() << ", #complementar " << complementar_positions.size() <<endl;
			#endif
		} else {
			#if DEBUG > 4
			cout<< "\t S completed : no complementar occurrences" <<endl;
			#endif
		}
#else //else !ONEPASS
		#if DEBUG > 3
		cout << "\t positions to extend : " <<_s_parent->_lpos.size() << endl;
		#endif
		//On parcourt toutes les instances du motif du parent
		list<position>::iterator it=_s_parent->_lpos.begin();
		while( it!=_s_parent->_lpos.end() ){

			list<position>::iterator jtp=it;
			jtp++;
			unsigned int max=sequence.size();
			if( jtp!=_s_parent->_lpos.end() ) {
				prefixtree::position next_p=*jtp;
				max=next_p.back()-pos+1;
#if DEBUG > 3
				cout << "\t\t max " << max << endl;
#endif
			}
#if DEBUG > 3
			else {
				cout << "\t\t max fin " << max << endl;
			}
#endif

			unsigned int currentpos=(*it).back()-pos; //ICI currentpos est exprimé dans les indices
													  // de la séquence (entre 0 et sequence.size()-1)
			currentpos++; //< on cherche à partir du suivant !

#if DEBUG > 3
			cout << "\t\t complete "<< (*it) << endl;
			cout << "\t\t start pos " << currentpos << "(seq size " << sequence.size() << ")" <<endl;
#endif


			while( (currentpos < max) && (gap_max==0 || currentpos<= ((*it).back()-pos)+gap_max ) ) {
				itemset is = sequence[currentpos];
#if DEBUG > 4
				cout<< "\t\t\t research "<< _s << " in current itemset "<< is<< endl;
#endif
				if( is.holds(_s) ) {
					position p((*it).min_pos(), pos+currentpos );
					_lpos.push_back( p );
					break;
				}
				currentpos++;
			}



			//On passe à la completion de la position suivante ...
			it++;
		}

#endif //end ONEPASS

#if DEBUG > 3
		cout<< "\t S completed : " << _lpos.size() <<endl;
#endif
		assert( assert_order() );
	}
}

#if ONEPASS

/**
 * 18/10/21012
 * \see ONEPASS
 */
list<prefixtree::position> prefixtree::node::merge_positions(const list<prefixtree::position> &plpos, const list<prefixtree::position> &compl_pos) const
{
	list<prefixtree::position> pos;
	//On parcourt toutes les instances du motif du parent
	list<prefixtree::position>::const_iterator it=plpos.begin();
	list<prefixtree::position>::const_iterator jt=compl_pos.begin();
	while( it!=plpos.end() ) {
		while( (*it).back()> (*jt).front() && jt!=compl_pos.end() ) {
			jt++;
		}

		if( jt==compl_pos.end() ) {
			//HERE their is no more complementar positions to browse
			// it is the end of the search
			break;
		} else if( (*it).back() == (*jt).front() ) {
			//HERE we find a complentar position
			prefixtree::position p((*it).front(), (*jt).back());
			pos.push_back(p);
			jt++;
		}
		// ELSE (*it).back() > (*jt).front() we didn't find a corresponding position : their is no
		// possible match between the position it and any complementar position fomr cmpl_pos.
		//  -> nothing to do

		it++;
	}
	return pos;
}


/**
 * 22/10/21012
 * \see ONEPASS
 */
list<prefixtree::position> prefixtree::node::cat_positions(const list<prefixtree::position> &plpos, const list<prefixtree::position> &compl_pos, bool included) const
{
	list<prefixtree::position> pos;
	//On parcourt toutes les instances du motif du parent
	list<prefixtree::position>::const_iterator it=plpos.begin();
	list<prefixtree::position>::const_iterator jt=compl_pos.begin();
	list<prefixtree::position>::const_iterator kt;
	while( it!=plpos.end() ) {
#if DEBUG>4
		cout << "\t\ti:" << (*it).back()<<endl;
#endif
		while( (*it).back()>= (*jt).front() && jt!=compl_pos.end() ) {
#if DEBUG>4
			cout << "\t\t\tj:" << (*jt).front()<<endl;
#endif
			jt++;
		}

		if( it!=plpos.end() ) {
			kt=it;
			kt++;
			while( kt!=plpos.end() && (*kt).back()<(*jt).front() ) {
				it=kt;
#if DEBUG>4
				cout << "\t\t\ti:" << (*it).front()<<endl;
#endif
				kt++;
			}
		}


		if( jt==compl_pos.end() ) {
			//HERE their is no more complementar positions to browse
			// it is the end of the search
			break;
		}/* else if( included && (*it).back() == (*jt).front() ) {
			//HERE we find the same position while the last itemset is included into the previous one : it is not possible to do a new position
			jt++;
			continue;
		}*/ else {
#if DEBUG>4
			cout << "\t\t\tinclude ("<<(*it).front()<<","<< (*jt).back() <<")"<<endl;
#endif
			prefixtree::position p((*it).front(), (*jt).back());
			pos.push_back(p);
			jt++;
		}

		it++;
	}
	return pos;
}


bool prefixtree::node::do_complete(list<prefixtree::node *> &tocomplete, vector<itemset> &sequence, const prefixtree &tree, int pos, unsigned int fmin, unsigned int gap_max)
{
	if( _tocomplete ) { //Lancement de la completion du noeud
		_tocomplete=false;

#if DEBUG > 3
		cout<< "\tis to complete (" << _s << ")" <<endl;
#endif

		assert( _c_parent!=NULL || _s_parent!=NULL);

		//Suppression de la dernière instance (ajoutée par MERGE)
		if( !(_s_parent!=NULL && _s_parent->isroot()) ) _lpos.pop_back();

		if(_c_parent!=NULL) {
			// this a été obtenu par composition avec le parent : les éléments de _lpos sont
			// donc des éléments de _c_parent->_lpos
			complete_comp(sequence, tree, pos);

		} else { //_c_parent==NULL : nécessairement _s_parent!=NULL
			// this a été obtenu par succession d'un item avec le parent :
			// les éléments de _lpos sont donc des éléments de _c_parent->_lpos
			// auquel on ajoute une position

			complete_succ(sequence, tree, pos, gap_max);
		} // _s_parent!=NULL
	}// to_complete


	// Si _lpos n'est pas assez grand, on retourne false pour faire détruire ce noeud courant!
	// Ceci évite de faire toute la récursion inutile
	if( !(_s_parent!=NULL && _s_parent->isroot()) && !isroot() && _lpos.size() < fmin ) {
		return false;
	}

	// Si _lpos est suffisamment grand, il faut traiter des motifs plus grands
	list<node *>::iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		tocomplete.push_back(*it);
		it++;
	}

	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		tocomplete.push_back(*it);
		it++;
	}

	return true;
}


bool prefixtree::complete_breadth(vector<itemset> &sequence, const prefixtree &tree, int pos, unsigned int fmin, unsigned int gap_max)
{
#if DEBUG > 3
	cout << "COMPLETE BREADTH : " <<endl;
#endif
	list<prefixtree::node *> tocomplete, tocompletenew;

	//Traitement du niveau 1 :
	list<node *>::iterator it = _root._seqChilds.begin();
	while( it!= _root._seqChilds.end() ) {
		(*it)->do_complete(tocomplete, sequence, tree, pos, fmin, gap_max);
		it++;
	}

	it = _root._compoChilds.begin();
	while( it!= _root._compoChilds.end() ) {
		(*it)->do_complete(tocomplete, sequence, tree, pos, fmin, gap_max);
		it++;
	}

	while( !tocomplete.empty() ) {
		tocompletenew.clear();
		list<node *>::iterator it=tocomplete.begin();
		while(it!=tocomplete.end()) {
			bool isfreq=(*it)->do_complete(tocompletenew, sequence, tree, pos, fmin, gap_max);

			//Elagage éventuel
			if( !isfreq ) {
#if DEBUG>3
				cout<< "remove ";
				(*it)->print_pattern();
				cout << endl;
#endif
				node *todelete=*it;
				if( (*it)->_c_parent ) {
					(*it)->_c_parent->_compoChilds.remove( todelete );
				} else if( (*it)->_s_parent ) {
					(*it)->_s_parent->_seqChilds.remove( todelete );
				}
				delete( todelete );
				todelete = NULL;
			}
			it++;
		}

		tocomplete = tocompletenew;
	}

	return false;
}

#endif //ONEPASS


void prefixtree::node::complete_comp(vector<itemset> &sequence, const prefixtree &tree, int pos)
{
	// Un _c_parent ne peut pas être root !!
	assert(_c_parent->_c_parent!=NULL || _c_parent->_s_parent!=NULL);

#if ONEPASS
	list<itemset> complementar;
	itemset ist = currentitemset();
	ist.pop_front();
	complementar.push_back(ist);

	 //complementar_positions is the list of the last itemset
	list<position> complementar_positions = tree.get_positions(complementar);

	_lpos.clear();
	//implémenter l'espece de fonction de merge ici !
	list<position>::iterator it=_c_parent->_lpos.begin();
	list<position>::iterator jt=complementar_positions.begin(); // for all position, (*jt).front() == (*jt).back() : it's an itemset !
#if DEBUG>3
	cout << "\t\tparent_positions: " << _c_parent->_lpos.size() << endl;
	cout << "\t\tcomplementar : " << ist <<endl;
	cout << "\t\tcomplementar_positions: " << complementar_positions.size() << endl;
#endif
	while( it!=_c_parent->_lpos.end() ) {
		while( (*it).back() > (*jt).front() && jt!=complementar_positions.end() ) {
			jt++;
		}
		if( jt==complementar_positions.end() ) {
			break;
		}

		if( (*it).back()==(*jt).front() ) {
			//HERE: the position (*it) that holds the itemset _c_parent, although holds the itemset _c_parent+_s
			_lpos.push_back( (*it) );
			jt++;
#if DEBUG>4
			cout << "\t\t\tpush: " << (*it) <<endl;
#endif
		} else {
			//HERE: position (*jt) is strictly after (*it).back() :
			// ** Full completion case ! **
			// [(*it).front(), (*jt).back()] holds a complete instance of the current pattern
			// -> Assuming that kt is the position of the next incomplete pattern,
			//	  it's a minimal occurrence iff (*kt).back()>(*jt).back() !
			//
			//	"--it--  (jt) ---kt--" is ok
			//  '--it--  --kt--  (jt)", thus [(*it).front(), (*jt).back()] will not be a minimal occurrence

#if DEBUG>3
			cout << "\t\tFull completion" << endl;
#endif
			list<position>::iterator kt=it;
			kt++;

			if( kt!=_c_parent->_lpos.end() && (*kt).back()>(*jt).back() ) {
				position p=*it;
				p.set_max_pos((*jt).back());
				_lpos.push_back(p);
#if DEBUG>4
			cout << "\t\t\tpush: " << p <<endl;
#endif
				jt++;
			} else {

			}
		}

		it++;
	}
#else //NOT ONEPASS

	list<position>::iterator it=_c_parent->_lpos.begin();
	list<position>::iterator jt;
	while( it!=_c_parent->_lpos.end() ) {
		//Ici, on regarde si pour toutes les positions du parent, le dernier itemset comporte _s
		// si c'est le cas, alors on ajoute la position dans _lpos
		position p; //Position à ajouter dans la liste (utile par la suite)

		unsigned int currentpos=(*it).back()-pos+1;

#if DEBUG > 3
		cout << "\t\t complete "<< (*it) << endl;
		cout << "\t\t pos " << currentpos << "(seq size "<<sequence.size()<<") : " << sequence[ currentpos-1 ] <<endl;
#endif

		itemset is = sequence[ currentpos-1 ];
		if( is.holds(_s) ) {
			//l'itemset contient bien l'item recherché, il suffit d'ajouter la position à la liste
			p=*it;
			_lpos.push_back( p );
		} else if( (*it).min_pos()!=(*it).max_pos() ) { // test si la longueur de la séquence est >1 (au moins deux itemsets)
			//Recherche plus loin d'un éventuel match complet

#if DEBUG > 3
			cout << "\t\t\t not found locally : FULL complete search" << endl;
#endif
			itemset current=currentitemset();
			currentpos++;
			jt=it;
			jt++;
			unsigned int max=sequence.size();
			if( jt!=_c_parent->_lpos.end() ) {
				unsigned int nextpos = (*jt).back()-pos+1;
				nextpos--;
				max=(max<nextpos?max:nextpos); //Position max à ne pas dépasser : le min de la sequence et de la position suivante
			}
#if DEBUG > 4
			cout << "\t\t\t\t max : " << max << endl;
#endif
			while( currentpos<=max ) {
				is = sequence[ currentpos-1 ];
#if DEBUG > 4
				cout << "\t\t\t\t Test " << is << endl;
#endif
				if( is.holds( current ) ) {
#if DEBUG > 4
					cout << "\t\t\t\t FOUND" << endl;
#endif
					p=*it;
					p.set_max_pos(currentpos+pos-1);
					_lpos.push_back( p );
					break; //On s'arrète au premier
				}
				currentpos++;
			}
		}
		it++;
	}
#endif //ONEPASS

#if DEBUG > 3
	cout<< "\t C completed : " << _lpos.size() <<endl;
#endif

	//HACK : l'ordre semble respecté en sortie de cette fonction !
	assert( assert_order() );
}

void prefixtree::complete(vector<itemset> &sequence, const prefixtree &tree, int pos, unsigned int fmin, unsigned int gap_max)
{
#if ONEPASS
	complete_breadth(sequence, tree, pos, fmin, gap_max);
#else
	_root.complete_depth(sequence, tree, pos, fmin, gap_max);
	#ifdef _OPENMP
	#pragma omp barrier //Barrière servant à attendre la fin de la completion avant de continuer
	#endif
#endif
}

/*
 * Modification of 12/06/2012 (T. Guyet) :<ul>
 * <li> add the gap_max parameter to take into account the gap max constraint
 * </ul>
 */
bool prefixtree::node::complete_depth(vector<itemset> &sequence, const prefixtree &tree, int pos, unsigned int fmin, unsigned int gap_max)
{
#if DEBUG > 3
	cout << "COMPLETE DEPTH : ";
	print_pattern();cout <<"|"<<endl;
#endif

	if( _tocomplete ) { //Lancement de la completion du noeud
		_tocomplete=false;

#if DEBUG > 3
		cout<< "\tis to complete (" << _s << ")" <<endl;
#endif

		assert( _c_parent!=NULL || _s_parent!=NULL);

		//Suppression de la dernière instance (ajoutée par MERGE)
		_lpos.pop_back();

		if(_c_parent!=NULL) { 	// this a été obtenu par composition avec le parent : les éléments de _lpos sont
			// donc des éléments de _c_parent->_lpos
			complete_comp(sequence, tree, pos);

		} else { //_c_parent==NULL : nécessairement _s_parent!=NULL
			// this a été obtenu par succession d'un item avec le parent :
			// les éléments de _lpos sont donc des éléments de _c_parent->_lpos
			// auquel on ajoute une position

			complete_succ(sequence, tree, pos, gap_max);
		} // _s_parent!=NULL
	}// to_complete


	// Si _lpos n'est pas assez grand, on retourne false pour faire détruire ce noeud courant!
	// Ceci évite de faire toute la récursion inutile
	if( !isroot() && _lpos.size() < fmin ) {
		return false;
	}

	// Si _lpos est suffisamment grand, il faut faire la récursion pour aller plus loin
	// Récursion après la complétion (vers les feuilles de l'arbre) pour accroitre les tailles des motifs au fur et à mesure (et n'oublier aucune position !)

#ifndef _OPENMP
	list<node *>::iterator it = _seqChilds.begin();
	while( it!= _seqChilds.end() ) {
		bool isfreq=(*it)->complete_depth(sequence, tree, pos, fmin, gap_max);

		//Elagage éventuel
		if( !isfreq ) {
			node *todelete=*it;
			it = _seqChilds.erase( it );
			delete( todelete );
		} else {
			it++;
		}
	}
	it = _compoChilds.begin();
	while( it!= _compoChilds.end() ) {
		bool isfreq=(*it)->complete_depth(sequence, tree, pos, fmin, gap_max);

		//Elagage éventuel
		if( !isfreq ) {
			node *todelete=*it;
			it = _compoChilds.erase( it );
			delete( todelete );
		} else {
			it++;
		}
	}
#else
	//Parallelisation de la completion : à chaque de niveau de récursion, on effectue une distribution de tâches
	// Ceci ne pose pas de problème car 1) il s'agit d'une récursion sur une structure d'arbre, 2) il n'y pas de modification des ancêtres
	int l =0, lSize =0;
	lSize = _seqChilds.size();
	valarray<prefixtree::node*> items(lSize);
	list<prefixtree::node*>::iterator it = _seqChilds.begin();
	while( it != _seqChilds.end() ) {
		items[l] = (*it);
		l++;
		it++;
	}
	list<prefixtree::node*> todelete;

    //! Parallelism schedule is dynamic to take into account unbalanced trees
    // The items variable is shared : direct access to elements using []
    #pragma omp parallel for default(shared) schedule(dynamic)
	for (int i=0; i<l; i++) {
		// La fonction complete fera appel à tous les noeuds du sous arbre de this et
		// à la liste des positions de this (en lecture uniquement, et ne sera plus touché ci-dessous) !
		// Je ne vois pas de cas d'exclusion mutuelle à mettre !
		bool isfreq=items[i]->complete_depth(sequence, tree, pos, fmin, gap_max);

		//Elagage éventuel
		if( !isfreq ) {
			#pragma omp critical
			todelete.push_back(items[i]);
		}
	}

	//Suppression effective des noeuds
	list<prefixtree::node*>::iterator ittd=todelete.begin();
	while( ittd!=todelete.end() ) {
		_seqChilds.remove(*ittd);
		delete( *ittd );
		ittd++;
	}

	//Pareil pour la composition
	todelete.clear();
	lSize = _compoChilds.size();
	items=valarray<prefixtree::node*>(lSize);
	it = _compoChilds.begin();
	l=0;
	while( it != _compoChilds.end() ) {
		items[l] = (*it);
		l++;
		it++;
	}
	//! Parallelism schedule is dynamic to take into account unbalanced trees
	// The items variable is shared : direct access to elements using []
	#pragma omp parallel for default(shared) schedule(dynamic)
	for (int i=0; i<l; i++) {
		// La fonction complete fera appel à tous les noeuds du sous arbre de this et
		// à la liste des positions de this (en lecture uniquement, et ne sera plus touché ci-dessous) !
		// Je ne vois pas de cas d'exclusion mutuelle à mettre !
		bool isfreq=items[i]->complete_depth(sequence, tree, pos, fmin, gap_max);

		//Elagage éventuel
		if( !isfreq ) {
			#pragma omp critical
			todelete.push_back(items[i]);
		}
	}

	//Suppression effective des noeuds
	ittd=todelete.begin();
	while( ittd!=todelete.end() ) {
		_compoChilds.remove(*ittd);
		delete( *ittd );
		ittd++;
	}
#endif
	//On conserve le noeud
	return true;
}



prefixtree *prefixtree::load(const string filename)
{
	typedef enum {START, SEQUENCE_START, SEQUENCE_END, SEQUENCE_COMP, SEQUENCE_SUCC, INSTANCE_FIRST, INSTANCE_SECOND, INSTANCE_END, COMMENTARY} loadstate;

	ifstream cin;
	char c;
	cin.open(filename.c_str(), ifstream::in);
	if( !cin.good() ) {
		cerr << "While opening a file "<< filename <<" an error is encountered" << endl;
		return NULL;
	}

	prefixtree *tree=new prefixtree();
	loadstate state = START;
	position pos;
	node *previous;
	int item;
	int lno=1;

	while( cin.good() ) {
		c='\0';
		state=START;
		while( c!='\n' && cin.good() ) {
			cin >> c;
			if( state==COMMENTARY ) {
				//On saute la fin de la ligne
				continue;
			}
			switch(c) {
			case '[':
				//On commence une nouvelle séquence
				state=SEQUENCE_START;
				previous=&(tree->_root);
				break;
			case ',':
				if( state!=INSTANCE_SECOND ) {
					cerr << "Error while reading file " << filename << " : invalid ',' line "<< lno << " ." << endl;
					continue;
				}
				break;
			case '>':
				state=SEQUENCE_SUCC;
				break;
			case '-':
				state=SEQUENCE_COMP;
				break;
			case ']':
				state=SEQUENCE_END;
				break;
			case '(':
				state=INSTANCE_FIRST;
				break;
			case ')':
				state=INSTANCE_END;
				break;
			case '#':
				state=COMMENTARY;
				break;
			}

			if(state==SEQUENCE_START || state==COMMENTARY  || state==SEQUENCE_END || state==INSTANCE_END ) {
				continue;
			}

			//On lit la valeur de l'entier
			cin >> item;
			if( !cin.good() ) {
				cerr << "Error while reading file " << filename << " : unespected number, line "<< lno << "." << endl;
				delete(tree);
				return NULL;
			}

			//On recherche si le noeud de l'item existe déjà
			list<node*>::iterator it;
			int found=0;
			if( state==SEQUENCE_SUCC ) {
				it=previous->_seqChilds.begin();
				while( it!=previous->_seqChilds.end() ) {
					if( (*it)->_s == item ) {
						found=1;
						break;
					}
					it++;
				}
				if( !found ) {
					node *n=new node(item);
					previous->append(n);
					previous=n;
				} else {
					previous=*it;
				}

			} else if( state==SEQUENCE_COMP ) {
				it=previous->_compoChilds.begin();
				while( it!=previous->_compoChilds.end() ) {
					if( (*it)->_s == item ) {
						found=1;
						break;
					}
					it++;
				}
				if( !found ) {
					node * n=new node(item);
					previous->compose(n);
					previous=n;
				} else {
					previous=*it;
				}
			} else if ( state==INSTANCE_FIRST ) {
				pos.set_min_pos(item);
				state=INSTANCE_SECOND;
			} else if ( state==INSTANCE_SECOND ) {
				pos.set_max_pos(item);
				state=INSTANCE_END;
				previous->_lpos.push_back(pos);
			}

		}
		//Ici, on est à la fin d'une ligne ou qqc c'est mal passé
		lno++;
	}

	cin.close();
	return tree;
}

/**
 * add 12/10/2012
 * \see ONEPASS
 */
list<prefixtree::position> prefixtree::get_positions(list<itemset> p) const
{
	list<prefixtree::position> positions;
	const prefixtree::node *patternnode=findnode(p, &_root, true);
	if( !patternnode ) {
		return positions;
	} else {
		return patternnode->_lpos;
	}
	return positions;
}

/**
 * add 12/10/2012
 * \see ONEPASS
 */
const prefixtree::node *prefixtree::findnode(list<itemset> p, const prefixtree::node *currentnode, bool start) const
{
	if( start||p.front().empty() ) {
		//Sequence
		if( !start ) p.pop_front();

		if( p.empty() ) {
			return currentnode;
		}

		int s=p.front().front();
		p.front().pop_front();
		list<node*>::const_iterator it=currentnode->_seqChilds.begin();
		while( it!=currentnode->_seqChilds.end() ) {
			if( (*it)->_s==s ) {
				return findnode(p, *it, false);
			}
			it++;
		}
		//On n'a trouvé aucune correspondance
		return NULL;
	} else {
		//Composition
		int s=p.front().front();
		p.front().pop_front();
		list<node*>::const_iterator it=currentnode->_compoChilds.begin();
		while( it!=currentnode->_compoChilds.end() ) {
			if( (*it)->_s==s ) {
				return findnode(p, *it, false);
			}
			it++;
		}
		//On n'a trouvé aucune correspondance
		return NULL;
	}
	return NULL;
}




#ifdef NDEBUG
int prefixtree::assert_monotony(const node *, const node *) const {return 1;}
int prefixtree::node::assert_order() const {return 1;}
int prefixtree::node::assert_tocomplete(bool val) const {return 1;};
#else
int prefixtree::assert_monotony(const node *n, const node *parent) const {
	if( parent!= NULL && n!=NULL && parent->_s!=-1 && parent->_lpos.size() < n->_lpos.size() ) {
		cerr << parent << ":=NULL," <<n <<"!=NULL,"<<parent->_s<<"!=1,"<<parent->_lpos.size()<<"<"<<n->_lpos.size()<<std::endl;
		cout << "TEST"<< std::endl;
		n->print_pattern();
		cout << std::endl;
		return 0;
	}

	list<prefixtree::node *>::const_iterator it = n->_seqChilds.begin();
	while( it != n->_seqChilds.end() ) {
		if( !assert_monotony( *it, n ) ) return 0;
		it++;
	}
	it = n->_compoChilds.begin();
	while( it != n->_compoChilds.end() ) {
		if( !assert_monotony( *it, n ) ) return 0;
		it++;
	}

	return 1;
}

int prefixtree::node::assert_order() const {
	list<position>::const_iterator it=_lpos.begin();
	list<position>::const_iterator jt=_lpos.begin();
	jt++;
	while( jt!=_lpos.end() ) {
		if( (*jt)<(*it) ) {
			return 0;
		}
		it++;
		jt++;
	}
	return 1;
}

int prefixtree::node::assert_tocomplete(bool val) const {
	if( _tocomplete!=val) {
		return 0;
	}
	list<prefixtree::node *>::const_iterator it = _seqChilds.begin();
	while( it != _seqChilds.end() ) {
		if( !(*it)->assert_tocomplete(val) ) return 0;
		it++;
	}
	it = _compoChilds.begin();
	while( it != _compoChilds.end() ) {
		if( !(*it)->assert_tocomplete(val) ) return 0;
		it++;
	}
	return 1;
}

#endif

