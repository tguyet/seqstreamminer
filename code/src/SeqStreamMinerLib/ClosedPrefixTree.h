/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove spaces and add dots)
 */


/**
 * \file ClosedPrefixTree.h
 * \author Guyet Thomas, Inria
 * \version 0.1
 * \date 25 nov 2021
 */

#ifndef CLOSEDPREFIXTREE_H_
#define CLOSEDPREFIXTREE_H_


#include <iostream>
#include <cstdio>
#include <list>
#include <vector>

#include "stream.h"
#include "debug.h"


using namespace std;

/**
 * A class to represent a prefix tree of closed frequent patterns
 *
 * The closeness of a pattern is a backward closeness.
 *
 * Currently, we only implemented sequential pattern.
 */
class ClosedPrefixTree {
public:
	/**
	 * \class position
	 * \brief This class describes one position of an itemset in the sequence
	 */
	class position {
	protected:
		int _min_pos, _max_pos;
		const position *_prefix; // null for singletons positions
		list<int> _pos;
	public:
		/**
		 * \brief Constructor for a singleton position
		 * \param i
		 *
		 * Initialize the position for a singleton with the value i
		 */
		position(int i):_min_pos(i),_max_pos(i),_prefix(NULL)
		{
			_pos.push_back(i);
		};

		/**
		 * \brief Constructor
		 * \param b : minimal position (begin)
		 * \param e : maximal position (end)
		 *
		 * Initialize the position with the value i
		 */
		position(int b, int e):_min_pos(b),_max_pos(e),_prefix(NULL){};

		/**
		 * \brief Constructor
		 * \param p position of a sub-pattern
		 * \param i position of the last item in the occurrence of the pattern
		 *
		 * Construct a new position for an occurrence of a pattern P=Mu{a} such that p is a position of M and i is the position of a.
		 */
		position(position *p, int i):_min_pos(p->front()),_max_pos(i),_prefix(p)
		{
			_pos = p->_pos;
			_pos.push_back(i);
		};

		/**
		 * \brief Destructor
		 */
		~position(){_prefix=NULL;};

		/**
		 * \brief egality operator
		 * \param p
		 * \return true if p is equal to the position, false otherwise
		 *
		 * The method compares positions until dismatch.
		 */
		int operator==(const position &p) const;

		/**
		 * \brief egality operator
		 * \param p
		 * \return true if p is equal to the position, false otherwise
		 *
		 * The method compares positions until dismatch.
		 */
		int operator==(position &p);


		/**
		 * \brief comparison operator
		 * \param p
		 *
		 * p1 < p2 iff p1.front < p2.front or (p1.front==p2.front et p1'.back<p2'.back) with p1=front.p1' and p2=front.p2'
		 */
		int operator<(const position &p) const;

		/**
		 * \brief comparison operator
		 * \param p
		 *
		 * p1 < p2 iff p1.front < p2.front or (p1.front==p2.front et p1'.back<p2'.back) with p1=front.p1' and p2=front.p2'
		 */
		int operator<(position &p);

		/**
		 * \brief stream output (printing)
		 */
		friend ostream& operator<< (ostream & os, const ClosedPrefixTree::position & is);

		void full_print(ostream & os) const;

		void pop_back() {
			_pos.pop_back();
			_max_pos=_pos.back();
		}

		/** GETTERS and SETTERS **/

		int front() const {
			return _min_pos;
		}
		int back() const {
			return _max_pos;
		}

		int min_pos() const {
			return _min_pos;
		}

		int max_pos() const {
			return _max_pos;
		}

		const position *prefix() const {
			return _prefix;
		}

		void set_min_pos(int val) {
			_min_pos=val;
		}

		void set_max_pos(int val) {
			_max_pos=val;
		}

		void set_prefix(const position *p) {
			_prefix=p;
		}
	};

	/**
	 * \class node
	 * \brief One node of the tree
	 *
	 * One node may have a composition parent of a sequential parent (not both !) or none (only the root)
	 *
	 * TODO Members are publics for convenient, may be protected
	 */
	class node {
	public:
		list<ClosedPrefixTree::node *> _Childs;  		//!< childs of the current node
		list<ClosedPrefixTree::position> _lpos;       //!< positions of the pattern (represented by the node in the tree) in the sequence
		int _s;                     //!< item value
		node *_parent;				//!< the parent node
		bool _doclose;				//!< true if the frequency (_lpos size) of this node is egal to the frequency of the node _parent
		bool _tocomplete;			//!< flag to process to the completion of the position list of the node !
		list<itemset> _pattern;		//!< the pattern

		/**
		 * \brief Default constructor
		 */
		node(int s): _s(s), _parent(NULL), _doclose(false), _tocomplete(false){
			if( s!=-1 ) {
				_pattern.push_back(itemset(s));
			}
		};

		/**
		 * \brief Copy constructor
		 */
		node(const node *n);

		/**
		 * \brief Destructor : recursively destruct the node (and descendants)
		 */
		~node();

		/**
		 * \brief the function construct recursively the list of all nodes in the subtree of the current node and fill the list
		 * \param l the list of nodes to fill in.
		 */
		void nodes(list<ClosedPrefixTree::node *> &l);


		/**
		 * \brief Indicates if the node can be close.
		 *
		 * The function evaluates if each child has the flag to close, if so the function returns true otherwise it returns false.
		 * It the node is a leaf, the function return false.
		 */
		bool isToClose();

		/**
		 * \brief cleaning function
		 *
		 * Recursively destroys subnodes without destroying the current node.
		 */
		void clean();

		/**
		 * \brief comparison operator
		 */
		inline int operator==(const ClosedPrefixTree::node &n) const {return _s==n._s;};

		/**
		 * \brief Add a node n corresponding to a sequencial item
		 * \param n pointer on the node to add
		 *
		 * The object pointed by n is not copied.
		 */
		void append(ClosedPrefixTree::node *n);

		/**
		 * \brief Add a position to the list of position
		 * \param p the position to add
		 */
		inline void append(position &p) {_lpos.push_back(p);};

		/**
		 * \brief It prefixes recursively all the positions of the subtree with the prefix prefixe
		 * \param prefixe
		 */
		void prefix(const ClosedPrefixTree::position *prefixe);

		/**
		 * \brief Set the parent
		 */
		void set_parent(ClosedPrefixTree::node *parent) {_parent=parent;};

		/**
		 * \brief define recursively the value of the flag tocomplete to the node and subnodes
		 * \param val (default true)
		 *
		 * Be careful of the recursion behaviour ; if val is false, then it sets the value false to the flag
		 * of all subnodes. But, if the val is true then the recursion is applied only to the composition childs.
		 */
		void set_tocomplete(bool val=true);


		/**
		 * \brief Test if the node is the root
		 */
		inline bool isroot() const {return _parent==NULL;};

		/**
		 * \brief merge recursively the content of the node n (items and positions) into the current node
		 * \param prefixes list of prefixes
		 * \param nocompo disable merge on composition (default, false)
		 * \param gap_max maximum gap between two successive instances of one pattern, no constraint if nul (default 0)
		 *
		 * The function adds the positions to the matching nodes, add children nodes and recurs
		 * If maxdepth is not nul, the recursion is stopped while the node \a n has a depth greater to maxdepth
		 * The  gap_max constraint is used in both adding instance to existing nodes and in the completion step
		 *
		 * \pre each instance of node \a n may be different from instance of the current node otherwise doublon will be inserted !
		 */
		void merge(ClosedPrefixTree::node *n, unsigned int gap_max=0);

		/**
		 * \brief print recursively the node and its descendants on stdout
		 * \param pos : recursion level required to decay the print
		 * \param of : output stream (cout by default)
		 * \see prefixtree::node::print_pattern
		 */
		void print(int pos, std::ostream & of =std::cout) const;

		/**
		 * \brief print recursively the node and its descendants on stdout
		 * \param of : output stream (cout by default)
		 * \see prefixtree::node::print_pattern
		 */
		void print_instances(std::ostream & of =std::cout) const;


		/**
		 * \brief print recursively the node and its descendants on stdout whithout decay
		 * \see prefixtree::node::print(int pos)
		 */
		void print_pattern(std::ostream & of =std::cout) const;

		/**
		 * \brief prune all the node whose _lpos has a size lower to fmin
		 * \param fmin occurrence number threshold
		 *
		 * This function can be used to force pruning a subtree from its unfrequent nodes.
		 */
		void prune(unsigned int );

		/**
		 * \brief Complete the positions list (_lpos) with possible missed instances and prune unfrequent nodes.
		 * \param sequence correspond à la fenêtre dans le flux
		 * \param pos position de la fenetre dans le flux : le (X-pos+1) itemset du flux est le Xieme itemset de la fenêtre entre 1 et sequence.size().
		 * \param fmin seuil de fréquence
		 * \param gap_max contrainte de gap maximal entre deux itemsets successifs d'une instance d'un motif (si 0, pas de contrainte)
		 * \return false if is not frequent (todelete)
		 *
		 * If the node is to complete (according to the corresponding flag), then the sequence is browsed to find out all occurrences of the
		 * current pattern. If the number of occurences is lower than fmin, then the node is destruced (with all its subnodes) aotherwise, the
		 * completion is recursivelly performed.
		 *
		 * \see prefixtree::node::_tocomplete, prefixtree::node::complete(vector<itemset> &sequence, int pos)
		 */
		bool complete(vector<itemset> &sequence, int pos, unsigned int fmin, unsigned int gap_max);


		/**
		 * \brief Completion of the position list in case of composition (the parent is a composition parent)
		 * \see prefixtree::node::complete(vector<itemset> &sequence, int pos, unsigned int fmin)
		 * \param sequence correspond à la fenêtre dans le flux
		 * \param pos position de la fenetre dans le flux : le (X-pos+1) itemset du flux est le Xieme itemset de la fenêtre entre 1 et sequence.size().
		 */
		void complete(vector<itemset> &sequence, int pos, unsigned int gap_max);

		/**
		 * \brief Recursively computes the size of the node (# of child nodes in the whole subtree)
		 */
		int size() const;


		/**
		 * \brief Recursively computes the maximal depth of the descendant of the node
		 */
		int maxdepth() const;

		/**
		 * \brief Return the number of leaves
		 */
		int leaves() const;

		/**
		 * \brief Check the value of the node attribute _tocomplete is equal to \a val
		 * \param val
		 *
		 * \return 1 if so
		 */
		int assert_tocomplete(bool val) const;


	};

protected:
	ClosedPrefixTree::node _root; //!< Root node of the tree (item -1)

public:
	/**
	 * \brief Default constructor
	 */
	ClosedPrefixTree():_root(-1){};
	/**
	 * \brief Copy constructor
	 */
	ClosedPrefixTree(const ClosedPrefixTree &tree):_root(tree._root){};

	/**
	 * \brief Destructor
	 * Destruct recursively all the nodes
	 */
	virtual ~ClosedPrefixTree(){};


	/**
	 * \brief Update the tree by computing the closed nodes
	 */
	void close();


	/**
	 * \brief prune all the nodes whose _lpos has a size lower to fmin
	 * \param fmin occurrence number threshold
	 */
	inline void prune(unsigned int fmin) { _root.prune(fmin); };

	/**
	 * \brief print the tree on output stream (cout by default)
	 */
	void print(std::ostream & of =std::cout) const {
		of << "~~~~Tree~~~~\n";
		_root.print(0, of);
		of << "\n~~~~~~~~~~~~\n";
	};

	inline void print_instances(std::ostream & of =std::cout) const {
		_root.print_instances(of);
	}

	/**
	 * \brief Construct the list of nodes in the tree
	 *
	 * NB: Can not be const because the returned list includes itself and the list is used to prefix its elements
	 * \return the list of nodes
	 */
	list<node *> nodes() {
		list<node *> nodeliste;
		_root.nodes(nodeliste);
		return nodeliste;
	};

	/**
	 * \brief Return the size of the tree (# of nodes)
	 */
	inline int size() const {return _root.size();};

	/**
	 * \brief Return the maximum depth of the tree
	 */
	inline int depth() const {return _root.maxdepth();};

	/**
	 * \brief Return the number of leaves
	 */
	inline int leaves() const {return _root.leaves();};

	/**
	 * \brief getter
	 */
	inline ClosedPrefixTree::node *root() {return &_root;};

protected:
	/**
	 * Test the non-closure of n and transform the tree in order to remove non-closed ancestors from n.
	 * - n may be transform into a close ancestor, or
	 * - n may be remove if their is no closed ancestor until the parent in the tree
	 *
	 * In case of transformation, the function is call recursively.
	 *
	 * The function return true if n is closed and has been removed from the tree.
	 *
	 * If the function return true then the node must be removed
	 */
	bool close(ClosedPrefixTree::node *n);
};

#endif /* CLOSEDPREFIXTREE_H_ */
