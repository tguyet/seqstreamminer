/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

#include "RuleExtractor.h"


static prefixtree::node * previousnode(prefixtree::node *n) {
	while(n->_s_parent==NULL) {
		if(n->_c_parent==NULL) {
			return NULL;
		}
		n=n->_c_parent;
	}
	return n->_s_parent;
}


list<RuleExtractor::Rule> RuleExtractor::buildrules(prefixtree &tree, double ct) {
	list<Rule> rules;
	list<prefixtree::node *> nodes = tree.nodes();

	list<prefixtree::node *>::iterator it=nodes.begin();
	for(;it!=nodes.end(); it++) {
		Rule rule;
		prefixtree::node *pn=(*it);
		rule.premise = pn->pattern();
		rule.support = pn->_lpos.size();
		rule = next(rule);
		pn=previousnode(pn); //pn pointe maintenant sur
		while( !rule.isempty() ) {
			//Calcul de la confiance de la règle
			rule.confidence = (double)rule.support/(double)pn->_lpos.size();
			//Insertion éventuelle
			if(rule.confidence > ct) {
				rules.push_back( rule );
			} else {
				break;
			}
			rule = next(rule);
			pn=previousnode(pn);
		}
	}

	return rules;
}

bool RuleExtractor::Rule::isempty()
{
	return premise.size()==0 || conclusion.size()==0;
}

RuleExtractor::Rule::Rule():confidence(-1), support(-1)
{

}

RuleExtractor::Rule::Rule(const Rule &rin): premise(rin.premise), conclusion(rin.conclusion), confidence(rin.confidence), support(rin.support) {

}


RuleExtractor::Rule RuleExtractor::next(RuleExtractor::Rule &rin)
{
	Rule rout;
	if( rin.premise.size()>1 ) {
		rout = rin;
		rout.conclusion.push_front( rout.premise.back() );
		rout.premise.pop_back();
		rout.confidence=-1;
	}
	return rout;
}

ostream& operator<<(ostream & os, const RuleExtractor::Rule & r)
{
	list<itemset>::const_iterator it=r.premise.begin();
	for(;it!=r.premise.end();it++) {
		os << ">" << (*it);
	}
	os << " => ";
	it=r.conclusion.begin();
	for(;it!=r.conclusion.end();it++) {
		os << ">" << (*it);
	}
	os << " (" << r.confidence << ")";
	return os;
}
