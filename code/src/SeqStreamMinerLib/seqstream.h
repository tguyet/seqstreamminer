/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/*! \mainpage SeqStreamMiner
 * \author Guyet Thomas, Inria
 * \version 0.5
 * \date 25 nov 2021
 *
 * \section intro_sec Introduction
 *
 * SeqStreamMiner is a tool to extract frequent patterns incrementally.
 *
 * \section version_sec Version
 * <ul>
 * <li>0.1 : first stable version</li>
 * <li>0.2 : introduction to the HistoryManager class</li>
 * <li>0.3 : stable version with PSStream and SeqStreamMiner comparison</li>
 * <li>0.4 : add tree depth pruning and maximal gap constraint</li>
 * <li>0.5 : improvement of positions representation </li>
 * <li>0.6 : </li>
 * <li>1.0 : second stable version with tools (CMake project)
 * <ul>
 * 	<li>GtkMM graphical visualisation</li>
 * 	<li>Random itemset sequence generators</li>
 * </ul></li>
 * <li>1.1 : Algorithm modification including tree reduction to improve performances</li>
 * <li>1.2 : OPENMP parallelization</li>
 * </ul>
 *
 * \section expe_sec Experimentations
 */

/**
 * \file seqstream.h
 * \author Guyet Thomas, Inria
 * \version 0.4
 * \date 25 nov 2021
 */

#ifndef SEQSTREAM_H_INCLUDED
#define SEQSTREAM_H_INCLUDED

#include <vector>
#include <map>
#include <list>
#include <ostream>
#include "postfixtree.h"
#include "SequenceMiner.h"

using namespace std;


/**
 * \class seqstream
 * \brief Class implementing the seqstream algorithm
 * \author Guyet Thomas
 */
class seqstream : public SequenceMiner {

protected:
	prefixtree _tree;   			//!< incremental prefix tree structure
	int _cur_pos;       			//!< current position in the data stream
	vector<itemset> _sequence;		//!< The mined sequence of itemset
	bool _output;					//!< false to disable all outputs in non-debug mode !
	unsigned int _maxdepth;			//!< maximum depth (infinite if null)
	bool _nocompo;					//!< Composition merging option (disable composition merging)
	unsigned int _gap_max;			//!< maximum gap constraint (none if 0)

public:
	/**
	 * \brief Default constructor
	 * \param ws the windows size
	 * \param fmin the frequency threshold (which is a number of item threshold here)
	 *
	 */
	seqstream(int ws, unsigned int fmin): SequenceMiner(ws,fmin), _cur_pos(0), _output(true), _maxdepth(0), _nocompo(false), _gap_max(0){};

	/**
	 * \brief Destructor
	 */
	~seqstream(){};

	/**
	 * \brief Launch the algorithm to extract frequent patterns in the stream
	 *
	 * \pre the stream must have been setup before launching.
	 * \see setstream
	 */
	void launch();

	/**
	 * \brief Setter
	 */
	void set_output(bool val=true) {_output=val;};
	void set_maxdepth(unsigned int val) {_maxdepth=val;};
	void set_nocompo(bool val=true) {_nocompo=val;};
	void set_maxgap(int val) {_gap_max=val;};
	int get_treesize() const {return _tree.size();};
	const prefixtree *get_tree() const {return &_tree;};

protected:
	/**
	 * \brief Removes positions from the children of n, and remove a child if its positions are empty
	 * \param n node to modify
	 * \param val the value to remove
	 * \param is itemset that will be added (required to detect quasi-frequents patterns)
	 * \param pqf (presque quasi-fréquent) means that the last itemset of the sequence n is a sub-itemset of is
	 *
	 * For each child of n, the function removes all the positions that starts with the value val.
	 * If the position list of a child is empty, then the node is destructed.
	 * Note that the position list of the node n is not modified, but only the position list of the children of n.
	 *
	 * Based on a monotony property, the function is recursive, the recursion is cutted while a children
	 * is unfrequent (node and all subsnodes removed) or if any position of a node holds val as first at the head.
	 *
	 * The remove function test if nodes holds enough positions, otherwise nodes are cutted off.
	 * if _fmin==0 there is no pruning, nontheless, empty leaves with _fmin==0 are removed.
	 * \pre The size of _lpos must formally decrease with the deepness of the node (monotony property)
	 * \pre val must appeared only at the head of the positions in n
	 * \pre antimonotony of _lpos.size() wrt deep
	 * \post Completness : the algorithm prune all unfrequent patterns and ensure to keep all the others
	 * \post Correction : any frequent patterns are deleted
	 * \post antimonotony of _lpos.size() wrt deep
	 * \see SANS_DECALAGE
	 *
	 * \date 28/6/12 add quasi frequent computation
	 */
	void remove(prefixtree::node *n, const itemset &is, bool pqf, int val =1);

	/**
	 * \brief Removes positions from root
	 * \param val the timestamp value to remove
	 * \param is itemset that will be added (required to detect quasi-frequents patterns)
	 * \param pqf (presque quasi-fréquent) means that the last itemset of the sequence n is a sub-itemset of is
	 *
	 * The function may be parallelized (activate OPENMP) according to the
	 * childs of the root (if only one child then there is no parallelisation).
	 * \see remove(prefixtree::node *, int)
	 *
	 * \date 28/6/12 add quasi frequent computation
	 */
	void remove(const itemset &is, bool pqf, int val =1);

	/**
	 * \brief add the itemset s to the tree structure
	 * \param s itemset to add
	 *
	 * In the Phase 1, a tree structure is constructed from s,
	 * then (Phase 2), the tree structure is merge into all the
	 * node (from leaves to the root). Position of the tree are
	 * prefixed by the position of the merged nodes. And finally
	 * (Phase 3), the tree structure is merged to the root
	 * (whithout prefix).
	 *
	 * The function may be parallelized (activate OPENMP)
	 * If parallelism is activated then Phase 2 is parallelized according to the
	 * childs of the root (if only one child then there is no parallelisation).
	 * This way, we ensure to merge from leaves to node.
	 * Otherwise, phase 2 and 3 are ensure thanks to the add function applied to the tree root.
	 *
	 * The function does not check if the size of position lists _lpos is lower than the frequency threshold. As a consequence, any nodes are pruned here.
	 *
	 * \pre each node of _tree has a _lpos whose size is greater than _fmin
	 * \pre antimonotony of _lpos.size() wrt deep
	 * \post each node of _tree has a _lpos whose size is greater than _fmin
	 * \post antimonotony of _lpos.size() wrt deep
	 * \post Correctness : each maximal pattern instances are taken into account in the tree
	 * \see _tree
	 * \see add(prefixtree::node *, prefixtree::node *)
	 */
	void add(itemset &s, vector<itemset> &sequence, int pos);


	/**
	 * \brief Phase 2 implementation
	 * \param ps tree to merge into T
	 * \param T tree in which ps is merged
	 * \pre descendants of T have been merged
	 *
	 * The function merges the node ps iterativelly on each node of T except quasi-frequent nodes
	 */
	void add(prefixtree::node *ps, prefixtree::node *T);


	/**
	 * \brief removes nodes from ps if nodes (itemset) are not frequent in the updated window
	 * \param ps (in/out) tree of the itemset to add to T
	 * \param T frequent patterns on the old window
	 * \see TREEREDUCE
	 *
	 * The function complete the positions list for the new itemsets. This completion is next
	 *  removed from the tree to merge the tree to all nodes.
	 * The completion for this itemsets will be done twice.
	 * We choose to do the completion twice for few nodes instead of testing each node if
	 * completion is required or not
	 */
	void simplify_tree(prefixtree::node *ps, prefixtree::node *T, vector<itemset> &sequence, int pos);


	/**
	 * \brief prune all the node whose _lpos has a size lower to _fmin
	 * \param s
	 *
	 * This function can be used to force pruning a subtree from its unfrequent nodes.
	 * \deprecated may not be used in incomplete window (bootstrap)
	 * \see prefixtree::node::prune(unsigned int fmin)
	 */
	void prune(prefixtree::node *n);


	/**
	 * \brief Construct a tree based on a simple itemset (no sequential relationship)
	 * \param s itemset
	 * \return the constructed tree
	 * \pre itemset must be ordered
	 * \see create_tree_rec
	 */
	prefixtree::node *create_tree(const itemset &s);

	/**
	 * \brief Recursive construction of the tree of an itemset : add only composition childs
	 * \param s itemset
	 * \param pred pointer on the current node to complete by composing it with the remaining of the itemset. The pointer is modified by the function.
	 * \param it iterator on the itemset which indicates what's remaining
	 * \param p position
	 */
	void create_tree_rec(const itemset &s, prefixtree::node *pred, itemset::const_iterator &it, const prefixtree::position &p);


	/**
	 * assert that the whole tree has nodes with attribute _tocomplete at the value val !
	 */
	int assert_tocomplete(prefixtree::node *n, bool val);
};


#endif // SEQSTREAM_H_INCLUDED
