/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file ClosedSeqStream.cpp
 * \author Guyet Thomas, Inria
 * \version 0.1
 * \date 25 nov 2021
 */


#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <cassert>
#include <string>

#include "ClosedSeqStream.h"
#include "debug.h"

using namespace std;

void ClosedSeqStream::launch()
{
	#ifdef TIME_PROBE
	clock_t start,end;

	fstream timefile( "times.txt", ios::out );
	if( timefile.fail() ) {
		cerr << "The TIME_PROBE file ('times.txt') can not be open !" << endl;
		exit(EXIT_FAILURE);
	}
	timefile << "pos time tree_size tree_leaves tree_depth"<<endl;
	#endif

    if( _s==NULL ) {
        cerr << "No stream\n";
        return;
    }

    if(_fmin==0) {
    	_fmin=1;
    	cerr << "Warning : fmin was null ! it has been set to 1 to avoid errors"<<endl;
    }

#if DEBUG > 1
    cout << "   _fmin : "<<_fmin << "\n";
    cout << "   _ws : "<< _ws << "\n";
    cout << "   _mg : "<<_gap_max << "\n";
#endif

    //initialisation before starting the stream processing
    _cur_pos=0;
    _s->init();
    _tree.root()->clean();
    _sequence.clear();

    //bootstrap sur la première fenêtre : on ne fait que des ajouts
    while( !_s->isend() && _s->posid()!=_ws ) {
		#ifdef TIME_PROBE
    	start= clock();
    	#endif

        _sequence.push_back( _s->next() );

        #if DEBUG > 1
        cout << "======================\n";
        cout << "   received " << _sequence.back() << ", pos: " << _cur_pos << "\n";
        cout << "======================\n";
        #endif

        _cur_pos++;
        add( _sequence.back(), _sequence, 1 );

		#if DEBUG > 2
        cout << " *** Do completion ***"<<endl;
		#endif
    	_tree.root()->complete(_sequence, 1, _fmin, _gap_max);

        #if DEBUG > 2
        _tree.print();
        #endif

        //Après complétion, tous les arcs sont correctement flaggés avec des
        // information sur la possibilité de clore ou non !

        /*
		#if DEBUG > 2
		cout << " *** Do closure ***"<<endl;
		#endif
		_tree.close();

		#if DEBUG > 2
		_tree.print();
		#endif
		//*/

		#if DEBUG < 1
        if( _output ) {
			printf("%.2lf%%", 100*(float)_cur_pos/(float)_s->size());
			fflush(stdout);
			printf("\r");
        }
		#endif

		#ifdef TIME_PROBE
		end = clock();
		timefile << _cur_pos << " " << (double)(end-start)/CLOCKS_PER_SEC << " " << _tree.size() << " " << _tree.leaves() << " " << _tree.depth() << std::endl;
		#endif
    }

    while( !_s->isend() ) {
		#ifdef TIME_PROBE
		start=clock();
		#endif

        #if DEBUG > 1
        cout << "======================\n";
        cout << "   deleted ("<< _cur_pos-_ws+1 <<") " << _sequence[0] << "\n";
        cout << "======================\n";
        #endif

        //decalage de la sequence :
        for(int i=0; i<_ws-1; i++) {
            _sequence[i]=_sequence[i+1];
        }
        _sequence[_ws-1] = _s->next();
        //Mise à jour de la structure de données par suppression

        remove( _sequence[_ws-1], _cur_pos-_ws+1 );

        #if DEBUG > 2
        _tree.print();
        #endif

        #if DEBUG > 1
        cout << "======================\n";
        cout << "   received " << _sequence[_ws-1] << ", pos: " << _cur_pos<< "\n";
        cout << "======================\n";
        #endif
        //Mise à jour de la structure de données par ajout
        _cur_pos++;
        add( _sequence[_ws-1], _sequence, _cur_pos-_ws+1 );


		#if DEBUG > 2
		cout << " *** Do completion ***"<<endl;
		#endif
        _tree.root()->complete(_sequence, _cur_pos-_ws+1, _fmin, _gap_max);
        #if DEBUG > 2
        _tree.print();
        #endif

        //Après complétion, tous les arcs sont correctement flaggés avec des
        // information sur la possibilité de clore ou non !

        /*
		#if DEBUG > 2
		cout << " *** Do closure ***"<<endl;
		#endif
		_tree.close();

		#if DEBUG > 2
		_tree.print();
		#endif
		//*/

		#if DEBUG < 1
        if( _output ) {
			printf("%.2lf%%", 100*(float)_cur_pos/(float)_s->size());
			fflush(stdout);
			printf("\r");
        }
		#endif

		#ifdef TIME_PROBE
		end = clock();
		timefile << _cur_pos << " "<< (double)(end-start)/CLOCKS_PER_SEC << " " << _tree.size() << " " << _tree.leaves() << " " << _tree.depth() << std::endl;
		#endif
    }


	#if DEBUG < 1
    if( _output ) {
		printf("[COMPLETE]\n");
		fflush(stdout);
    }
	#endif
	#ifdef TIME_PROBE
    timefile.close();
	#endif
}

/**
 * Quasifrequent node at level one are not take into account
 */
void ClosedSeqStream::remove(const itemset &is, int val)
{
	remove( _tree.root(), is, val);
}

/**
 * \pre val est la plus petite valeur utilisée dans les listes de position
 * \pre toutes les listes de position sont ordonnées et vérifient les contraintes MinOcc
 * \post toutes les listes de position sont ordonnées et vérifient les contraintes MinOcc
 */
void ClosedSeqStream::remove(ClosedPrefixTree::node *n, const itemset &is, int val)
{
    list<ClosedPrefixTree::node *>::iterator it = n->_Childs.begin();
    while( it != n->_Childs.end() ) {

#if DEBUG >4
	cout << "remove instances of ";(*it)->print_pattern();cout <<endl;
#endif

		//HACK : par les contraintes MinOcc, il ne peut exister qu'un seul élément avec comme début val
		//		 par contrainte d'ordre des listes de position, une position utilisant val est nécessairement positionné en début de liste
		list<ClosedPrefixTree::position>::iterator itp = (*it)->_lpos.begin();
		if( (*itp).min_pos()==val ) {
			(*it)->_lpos.erase( itp );

#if DEBUG >4
	cout << "\t remove "<< (*itp) <<endl;
#endif
		} else {
            /** HACK OPTIM: si un élément de la séquence n'est
            * présent dans aucune des positions de n, alors
            * il ne le sera pas dans ses fils : on arrête !
            */
            it++;
            continue;
		}
#if DEBUG >4
		if((*it)->_lpos.size()==_fmin-1) {
			(*it)->print_pattern();
			printf("quasi-frequent ?? %d ??\n", is.holds( (*it)->_s ));
		}
#endif

#if DEBUG >4
	cout << "\t remaining instances : " << (*it)->_lpos.size() << "/"<<_fmin <<endl;
#endif
        if( (*it)->_lpos.size() <_fmin-1 ) {

#if DEBUG >4
	cout << "\t delete node" <<endl;
#endif
            ClosedPrefixTree::node *todelete = *it;
            it = n->_Childs.erase( it );
            delete( todelete );
        } else if( (*it)->_lpos.size()==_fmin-1  ) {

				#if DEBUG >4
					cout << "\t delete pqf node" <<endl;
				#endif
				ClosedPrefixTree::node *todelete = *it;
				it = n->_Childs.erase( it );
				delete( todelete );
        } else {
            remove(*it, is, val);
            it++;
        }
    }
}

void ClosedSeqStream::prune(ClosedPrefixTree::node *n)
{
    list<ClosedPrefixTree::node *>::iterator it = n->_Childs.begin();
    while( it != n->_Childs.end() ) {
        if( (*it)->_lpos.size() < _fmin ) {
            ClosedPrefixTree::node *todelete = *it;
            it = n->_Childs.erase( it );
            delete( todelete );
        } else {
            prune(*it);
            it++;
        }
    }
}

/**
 * \pre s doit être ordonné
 */
ClosedPrefixTree::node *ClosedSeqStream::create_tree(const itemset &s) {

	ClosedPrefixTree::node *ps=new ClosedPrefixTree::node(-1);

	//Attention : les itemset doivent être ordonnés (ordre lexicographique)
	itemset::const_iterator it=s.begin();
	while( it!= s.end() ) {
		//Ajout d'un noeud pred, successeur de ps
		ClosedPrefixTree::node *pred=new ClosedPrefixTree::node(*it);

	    //Ajout d'une position à pred
		ClosedPrefixTree::position p(_cur_pos);
	    pred->_lpos.push_back( p );
	    ps->append( pred );

	    // Pas de recursion dans cette version closed sans items "paralleles", on commente cette ligne
	    //create_tree_rec(s, pred, it, p);
	    it++;
	}
    return ps;
}


void ClosedSeqStream::add(ClosedPrefixTree::node *ps, ClosedPrefixTree::node *T)
{
    ClosedPrefixTree::node *nc;
    list<ClosedPrefixTree::node *> nodes;
    T->nodes(nodes);

    /*! HACK : Il faut commencer par remplir par la fin de la liste (qui correspond
     aux noeud les plus profonds)
     Dans le cas inverse, on ajoute des positions intermédiaires dans le sous-arbre
     qui sont réutilisés à tort dans la suite des copies sur les noeuds inférieurs
    */

    list<ClosedPrefixTree::node *>::reverse_iterator itn = nodes.rbegin();
    while( itn != nodes.rend() ) {

        //Copie du noeud racine sur le noeud (*itn) (qui ne peut pas être la racine)
    	//#pragma omp single //protection de la copie lors de la parallelisation (? même si que en lecture !!)
		// si on utilise ce single, il y a un interblocage !

        nc = new ClosedPrefixTree::node( ps );

        //We add the last prefix to the positions of nc
        if( !(*itn)->_lpos.empty() ) {
        	nc->prefix( &((*itn)->_lpos.back())  );
        }

        #if DEBUG > 4
		#pragma omp critical(print)
        {
			cout << "prefixed tree : \n";
			nc->print(0);
			cout << "finprefixed tree : \n";
		}
        #endif

        //On fusionne cette copie
        (*itn)->merge( nc, _gap_max );

		#if DEBUG > 4
		#pragma omp critical(print)
		{
			cout << "resultat prefixed tree : \n";
			_tree.print();
			cout << "resultat finprefixed tree : \n";
		}
		#endif
        delete( nc );
        itn++;
    }

}


void ClosedSeqStream::add(itemset &s, vector<itemset> &sequence, int pos)
{
    //Phase 1 : on créé un ClosedPrefixTree ps à partir de l'itemset

	unsigned int taille = s.size();
	s.sort(); //< pour être sûr de la pré-condition !
	s.unique();
	if( taille != s.size() ) {
		cerr << "Warning : redundant items in an itemset have been removed"<<endl;
	}
    ClosedPrefixTree::node *ps = create_tree(s);


    #if DEBUG > 3
    cout <<"-----\nAdd itemset\n-----\n";
    cout << "Itemset :\n";
    ps->print(0);
    cout << endl;
    #endif

	//Phase 2 and 3 : on ajoute systématiquement une copie de ps sous chaque noeud de l'arbre principal
	add(ps, _tree.root());

	#if DEBUG > 3
	cout <<"Copying result:\n";
	_tree.print();
	#endif


    //ps n'est plus utile : on le delete
    delete(ps);
    #if DEBUG > 3
    cout << "-----\n\n";
    #endif
}


