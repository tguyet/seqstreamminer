/*
 *    This file is part of SeqStreamMiner.
 *
 *    SeqStreamMiner is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    SeqStreamMiner is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file main.cpp
 * \author Guyet Thomas, AGROCAMPUS-OUEST/IRISA
 * \brief Programme principal pour l'exécution des algorithmes d'extraction de motifs fréquents dans une longue séquence.
 * \version 0.4
 * \date 21 février 2012
 *
 * Ce programme permet d'exécuter deux algorithmes (en utilisant ou non le paramètre -sm) de fouille de motifs fréquents dans une longue séquence.
 * Le premier algorithme fonctionne par batches (traités indépendamment les uns des autres) tandis que le second fonctionne incrémentalement.
 *
 * Les autres paramètres qui peuvent être passés au programme permettent de choisir le type de fichier d'entrée (IBM ou perso)
 * et de définir les paramètres des algorithmes (fréquence minimum et taille des fenêtres).
 */

/*!
\page Page_TODO TODO and Done page

\section À faire

<ul>
<li> amélioration du code : encore et toujours ... (vectorisation ?)</li>
<ul>
    <li> Paralléliser update history</li>
    <li> Vectoriser le décalage des séquences (seqstream.cpp:94)</li>
    <li> L'utilisation des const ralentit l'exécution !!!!</li>
    <li> Tester Melinda (Benjamin Negrevergne, LIG) ??</li>
</ul>
<li> version future : Traiter plusieurs séquences en parallèles (À paralléliser sur le traitement des séquences différentes ??)</li>
<li> interfacage matlab / R</li>
</ul>


\section DONE
<ul>
	<li> 1/08/11
		<ul>
	    	<li> Traitement des positions en ordre (suppression tous les sort() des listes de position)
		</ul>
	</li>
	<li> 17/07/11 (après quelques jours acharnés sur le code !!)
		<ul>
	    	<li> LES ALGORITHMES PSStream et SeqStream donnent enfin la même chose !!
			<li> test valgrind : parfait !
			<li> quelques tests de comparaison de temps ... pas super glorieux ! A poursuivre  (beaucoup de temps dans l'update_history)
		</ul>
	</li>
	<li> 27/04/11
		<ul>
	    	<li> test valgrind : parfait !
    		<li> Parallélisation
			<li> Meilleurs performances en mono-processeur qu'en bi-processeur
			<li> Le quad-proc boost un peu
		</ul>
	</li>
	<li> 28/04/2011 : tentative de mise en conformité mais ca ne fonctionne pas très bien.
		<ul>
	    	<li> Le problème existait autant dans PSStream que dans SeqStream
			<li> Les problèmes sont théoriques : dans la mesure où la méthode de comptage n'est pas monotone entre les motifs de taille 1 et 2, il y a trop de code particulier à faire pour le gérer !!! Et c'est pas beau !!
			<li> PSStream semble en conformité avec la méthode de comptage théorique
			<li> GROS PB PERSISTANT à la suppression : si un item de taille 1 est supprimé, tous ces descendants le son avec le risque de supprimer des motifs de taille 2 bien fréquents !! Ce problème n'a pas été résolus. Séquence de test <aabcbabaa>
			<li> De plus, les tailles d'arbres sont beaucoup plus importants avec cette méthode de comptage !
		</ul>
	</li>
	<li> 29/04/2011 :
		<ul>
	    	<li> retour à l'ancienne version améliorée : comptage plus classique, mais complet et correct !
			<li> correction de la classe HistoryManager : l'utilisation d'une map avec comme clé une list<itemset> était  sans solution (impossibilité de donnée un ordre strict total sur l'ensemble des motifs !). Je me suis rabattu sur une liste de paires avec utilisation d'une égalité pour retrouver les éléments de de mon historique !
		</ul>
	</li>
	<li> 30/04/2011 :
		<ul>
	    	<li> tests par comparaison de résultat entre SM et SeqStream pour débusquer des erreurs ... et il y en a pas mal
			<li> grosses modifications :
			<ul>
				<li> pas beaucoup de ligne de modifiées, mais des détails très importants !
				<li> pas possibilité d'énuméré !
				<li> effet d'avant-arrière de modification dangeureux : il faudra refaire une passe plus formelle !!
			</ul>
		</ul>
	</li>
	<li> 01/05/2011 :
		<ul>
	    	<li> modification de seqstream::create_tree pour la passer en récursif (ne fonctionnait qu'avec des itemsets de taille 2)
			<li> ajout d'une suppression des items redondants lors de l'ajout d'un itemset (seqstream::add) : affiche un warning !  -> provoquait des assertions fausses ! :) c'est bien les assert !! sur le test : (aabc)(aab)(babc)(dabe)(aac)
			<li> dans seqstream::launch : ajout d'un warning pour les cas de fmin==0 qui conduit à des erreurs (listes d'instance vide, alors que plus tard ces listes sont supposées non vides !)
			<li> modification de node::complete pour le cas des listes d'instances qui sont des singletons !
			<li> suppression de vieux code commenté (disponible dans saved, prefixé par la date !)
			<li> correction de PSStream -> full projection : la construction du lastitem était erronnée
		</ul>
	</li>
	<li> 01/05/2011
		<ul>
	    	<li> complete : ajout de FULL COMPLETION pour les cas de composition
			<li> PSStream : modification de l'endroit où je faisais le sort des _lpos : maintenant juste avant la récursion, et ca marche beaucoup mieux !
			<li> création de la fonction node::authorizedoverlap pour tester si l'avant dernier itemset contient le dernier !
		</ul>
	</li>
	<li> 02/05/2011
		<ul>
	    	<li> modification de complete
		</ul>
	</li>
	<li> 03/05/2011
		<ul>
	    	<li> modification de complete : utilisation d'une modification du max pour éviter des overlap (avec inversion dans le parcours des positions parentes : reverve_iterator)
		</ul>
	</li>
	<li> 3/11/2011 <ul>
			<li>Création d'une classe abstraite SequenceMiner pour abstraire les deux algorithmes</li>
			<li>Ajout des possibilités de représentation d'un historique sous la forme d'un arbre</li>
			<li>Modification des includes du main pour la bonne conformité C++</li>
		</ul>
	</li>
	<li> 21/2/2012 <ul>
		<li>Ajout de la gestion des tailles limites d'arbre pour faire des comparaisons avec MinEPI</li>
		<li>Ajout du paramètre -mws (--max_win_size) pour spécifier la profondeur maximale des arbres (reste 0 par défaut, soit infinie)</li>
		</ul>
	</li>
	<li> 24/02/2012 - 27/02/2012<ul>
		<li>Modification de la représentation de la liste des instances pour optimisation des recherches</li>
		<li>Suppression de quelques fonctions deprecated pour clarifier le code</li>
		<li>Suppression de l'étape de remise à jour des attributs _tocomplete à faux + assertion pour vérification de la non regression !</li>
		<li>Simplification de la fonction itemset::holds(itemset &)</li>
	</ul>
	</li>
	<li>28/02/2012<ul>
		<li>Amélioration de la fonction remove(prefixtree::node *, int)</li>
	</ul>
	</li>
	<li>12/06/2012<ul>
		<li>Ajout de la contrainte max_gap dans les fonctions de merge et de completion + main</li>
		<li>Correction de la fonction Merge qui ne reportait pas les paramètres dans les récursions (en particulier de limite de taille d'arbre, cette fonctionnalité n'était donc pas efficace !)</li>
	</ul>
	</li>
	<li>27/06/2012<ul>
		<li>Modification de la gestion de l'historique pour passer à la gestion en ligne des intervalles. Les anciennes fonctions ont été mises en deprecated.</li>
		<li>Version sauvegardée</li>
	</ul>
	</li>
	<li>30/06/2012 - 3/07/2012<ul>
		<li>Réduction du nombre de complétion par l'utilisation de quasi-fréquent : utilisation du flag QFREQ pour activer la prise en compte des quasi-fréquents</li>
		<li>Réduction du nombre de complétion (2) par la réduction a priori des tailles d'arbre</li>
		<li>Transformation de la structure du projet en CMake</li>
		<li>3/7/2012 : IL Y A UNE ERREUR DANS LE CALCUL DES QUASI-FREQUENT : je n'ai pas exactement la même chose !!! (cf note) On maintenant néanmoins cette différence parce que la version avec Quasi-fréquent retourve moins de motifs (il en manque quelques uns, uniquement ceux qui incluent des répétitions d'items) et est pourtant plus lente.</li>
	</ul>
	</li>
	<li>12/07/2012<ul>
		<li>Ajout d'une option pour l'affichage de l'arbre final (contient la liste des instances)</li>
		<li>Ajout d'une partie Gtkmm incluant une classe pour la visualisation d'un arbre dans une interface graphique</li>
	</ul>
	</li>
	<li>23/07/2012-24/07/2012<ul>
		<li>Ajout d'une option ORDERNODES assurant un maintenant à jour d'un ordre des _seqChilds et _compoChilds d'un noeud et utilisant cette ordre dans les recherches. Cette version n'améliore pas les temps de calcul.</li>
		<li>Rétablissement de la parallelisation avec OpenMP, en incluant quelques modifications:<ul>
			<li>Ajout de la parallelisation de la completion : la parallelisation se fait ici à tous les niveaux de la récursion (schedule dynamic)</li>
			<li>ajout de barrière dans la fonction principale pour éviter les problèmes ! </li>
			<li>parallelisation de HistoryManager::updateI(const prefixtree::node *n, int cur_pos)</li>
			<li>test des parallélisations sur des séquences IBM</li>
			</ul>
		</li>
	</ul>
	</li>
	<li>12/10/2012<ul>
		<li>Début de la création d'une version avec des motifs séquentiels clos : version plutôt simple, sans items paralleles et sans les différentes tests optimisations (TREEREDUCTION, PQF).<br>
		En pratique, il s'agit simplement de créer des classes ClosedPrefixTree (avec ses propres noeuds et positions) et ClosedSeqStream en repartant des classes existantes.
		</li>
		<li>ClosedPrefixTree
		<ul>
			<li>Suppression des depth et length qui n'avait plus lieu d'être (perte de correspondance avec les attributs de motifs)</li>
			<li>Définition d'un attribut supplémentaire _doclose permettant de marqué la possibilité de cloture avec un prédécesseur</li>
			<li>Définition de positions complète en utilisant un pointeur sur une occurrence d'un antécédent : on a accès à toute l'information nécessaire pour les clos (en continuant à maintenir le min/max) et on n'explose pas l'usage mémoire</li>
			<li>Chaque noeud possède maintenant la description complète du pattern : nécessaire pour les clos puisque cette information n'est pas contenue dans l'antécédence</li>
		</ul>
		</li>
	</ul>
	</li>
	<li>23/10/2012 : SinglePassSeqStream et SinglePassTree</li>
</ul>
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sys/resource.h>
#include <sys/time.h>

#include "seqstream.h"
#include "ClosedSeqStream.h"
#include "ClosedPrefixTree.h"
#include "SinglePassSeqStream.h"
#include "SinglePassTree.h"
#include "PSStream.h"
#include "stream.h"
#include "debug.h"

using namespace std;

void usage(const char *comm);

int main(int argc, char **argv)
{
	enum {SEQSTREAM, SIMPLE_MINING, CLOSEDSEQSTREAM, SINGLEPASS};
	stream f;
	ifstream is;
	ofstream ohs;
	char *file=NULL, *historyfile=NULL, *lasttreefile=NULL;
	int w =3;
	int fmin =1, mws=0, mg=0;
	int fileformat =FILE_FORMAT_PERSO;
	int algo = SEQSTREAM;


	if(argc<=1) {
		cerr << "An input file name is required\n";
		usage( argv[0] );
		return EXIT_FAILURE;
	}

	for(int i=1;i<argc;i++)
	{
		if( !strcmp(argv[i],"-h") ) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		} else if(!strcmp(argv[i],"-w") || !strcmp(argv[i],"--windows-size")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&w) || w<=0 ) {
				cerr << "Parameter error: Expected strictly positive integer after --windows-size (-w)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-f") || !strcmp(argv[i],"--fmin")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&fmin) || fmin<0 ) {
				cerr << "Parameter error: Expected positive integer after --fmin (-f)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-mws") || !strcmp(argv[i],"--max_win_size")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&mws) || mws<0 ) {
				cerr << "Parameter error: Expected positive integer after --max_win_size (-mws)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-mg") || !strcmp(argv[i],"--max_gap")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&mg) || mg<0 ) {
				cerr << "Parameter error: Expected positive integer after --max_gap (-mg)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-o")) {
			i++;
			historyfile = argv[i];
		} else if(!strcmp(argv[i],"-lo")) {
			i++;
			lasttreefile = argv[i];
		} else if(!strcmp(argv[i],"-sm")) {
			algo = SIMPLE_MINING;
		} else if(!strcmp(argv[i],"-ibm")) {
			fileformat=FILE_FORMAT_IBM;
		} else if(!strcmp(argv[i],"-c") || !strcmp(argv[i],"--closed")) {
			algo = CLOSEDSEQSTREAM;
		} else if(!strcmp(argv[i],"-sp")) {
			algo = SINGLEPASS;
		} else {
			//C'est un fichier à charger
			if( file ) {
				cout << "Warning : a file name as already be given (check parameters)\n";
			}
			file = argv[i];
		}
	}

	if( !file ) {
		cerr << "No input file\n";
		usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	is.open(file);
	if( fileformat==FILE_FORMAT_IBM ) {
		f.read_ibm( is );
	} else {
		f.read( is );
	}
	is.close();

#if DEBUG > 1
	f.print();
#endif

	SequenceMiner *miner=NULL;
	if( algo == SIMPLE_MINING ) {
		miner = new PSStream(w,fmin);
	} else if( algo == CLOSEDSEQSTREAM ) {
		miner = new ClosedSeqStream(w,fmin);
		( (ClosedSeqStream*)miner )->set_maxdepth(mws);
		( (ClosedSeqStream*)miner )->set_maxgap(mg);
	} else if( algo == SINGLEPASS ) {
		miner = new SinglePassSeqStream(w,fmin);
	} else {
		miner = new seqstream(w,fmin);
		( (seqstream*)miner )->set_maxdepth(mws);
		( (seqstream*)miner )->set_maxgap(mg);
	}

	miner->setstream(&f);
	miner->launch();

	if( historyfile ) {
		ohs.open(historyfile);
		miner->print_history(ohs);
		ohs.close();
	} else {
		miner->print_history(ohs);
	}


#if TIME_PROBE
	//Calcul de l'usage des ressources
	struct rusage ru;
	getrusage(RUSAGE_SELF, &ru);
	tm *timeinfo = localtime ( &ru.ru_utime.tv_sec );
	printf("User time %s.%d, Memory usage %ld", asctime (timeinfo), ru.ru_utime.tv_usec, ru.ru_maxrss);
#endif

	if( algo==SEQSTREAM && lasttreefile ) {
		const prefixtree *tree = ((seqstream*)miner)->get_tree();
		ohs.open(lasttreefile);
		tree->print_instances(ohs);
		ohs.close();
	} else if ( algo==CLOSEDSEQSTREAM && lasttreefile ) {
		const ClosedPrefixTree *tree = ((ClosedSeqStream*)miner)->get_tree();
		ohs.open(lasttreefile);
		tree->print_instances(ohs);
		ohs.close();
	}

	if(miner) delete(miner);

	return EXIT_SUCCESS;
}

/**
 * \brief Fonction d'information sur l'usage du programme
 */
void usage(const char *comm)
{
	printf("*******************\n");
	printf("SeqStreamMiner Miner program, v.%s\n\n", VERSION);
	printf("Brief : Incremental mining of minimal occurrences of sequential patterns in a sequence of itemsets\n");
	printf("Author : Guyet Thomas, AGROCAMPUS-OUEST\n");
	printf("Year : 2012\n");
	printf("*******************\n");
	cout << "version information (compilations optimisation options)" <<endl;
#if ONEPASS
	cout << "\t* ONE PASS activated"<<endl;
#else
	cout << "\t* ONE PASS disabled"<<endl;
#endif
#if _OPENMP
	cout << "\t* OPENMP activated"<<endl;
#else
	cout << "\t* OPENMP disabled"<<endl;
#endif
#if QFREQ
	cout << "\t* QFREQ activated"<<endl;
#else
	cout << "\t* QFREQ disabled"<<endl;
#endif
#if TREEREDUCE
	cout << "\t* TREEREDUCE activated"<<endl;
#else
	cout << "\t* TREEREDUCE disabled"<<endl;
#endif
#if ORDERNODES
	cout << "\t* ORDERNODES activated"<<endl;
#else
	cout << "\t* ORDERNODES disabled"<<endl;
#endif

	cout << "other options" <<endl;
#if DEBUG>0
	cout << "\t* DEBUG LEVEL:"<< DEBUG<<endl;
#else
	cout << "\t* DEBUG disabled"<<endl;
#endif
#if TIME_PROBE
	cout << "\t* TIME_PROBE activated"<<endl;
#else
	cout << "\t* TIME_PROBE disabled"<<endl;
#endif
	printf("*******************\n\n");
	printf("command : %s [-w %%d] [-f %%d] [-mws %%d] [-o %%s] [-ibm] [-sm] datafile\n\n", comm);
	printf("Options :\n");
	printf("\t --windows-size (-w) : windows size (ie the maximum length between events dates\n");
	printf("\t --fmin (-f) : frequency threshold\n");
	printf("\t -o file : save the sequence history in the file\n");
	printf("\t -lo file : save the last tree, with pattern instances, in a file (only for incremental algorithm)\n");
	printf("\t -sm : perform simple mining (no incremental processing) : may be used to compare processing efficiencies\n");
	printf("\t -ibm : uses an ibm file format\n");
	printf("\t --max_win_size (-mws) : maximal size of patterns (no limit if null) (default null)\n");
	printf("\t --max_gap (-mg) : maximal gap between two itemsets of one instances (no constraint if null) (default null)\n");
	printf("\t --closed (-c) : use closed sequential pattern extraction (default false)\n");
	printf("*******************\n");
#if ORDERNODES
	printf("\t# uses order nodes\n");
#endif
#if QFREQ
	printf("\t# uses quasi frequent itemsets\n");
#endif
#if TREEREDUCE
	printf("\t# uses tree reduction\n");
#endif
}
