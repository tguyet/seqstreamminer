/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file main.cpp
 * \author Guyet Thomas, Inria
 * \brief Programme de comparaison
 * \version 0.3
 * \date 25 nov 2021
 *
 * Ce programme réalise la fouille de motifs sequentiels fréquents dans une longue séquence en utilisant successivement
 * les deux algorithmes (en batch ou en séquence) et compare les motifs extraits.
 * Ce programme sert à véfifier que les deux algorithmes produisent bien le même résultat d'extraction.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>

#include "seqstream.h"
#include "PSStream.h"
#include "stream.h"

void usage(const char *comm);

int main(int argc, char **argv)
{
    stream f;
    ifstream is;
	ofstream ohs;
    char *file=NULL;
    int w =3;
    int fmin =0;
    int fileformat =FILE_FORMAT_PERSO;

    if(argc<=1) {
        cerr << "An input file name is required\n";
        usage( argv[0] );
        return EXIT_FAILURE;
    }

    for(int i=1;i<argc;i++)
    {
        if( !strcmp(argv[i],"-h") ) {
            usage(argv[0]);
            return EXIT_SUCCESS;
        } else if(!strcmp(argv[i],"-w") || !strcmp(argv[i],"--windows-size")) {
            i++;
            if( i<0 || 1!=sscanf(argv[i],"%d",&w) || w<=0 ) {
                cerr << "Parameter error: Expected strictly positive integer after --windows-size (-w)\n";
                usage( argv[0] );
                exit( EXIT_FAILURE );
            }
        } else if(!strcmp(argv[i],"-f") || !strcmp(argv[i],"--fmin")) {
            i++;
            if( i<0 || 1!=sscanf(argv[i],"%d",&fmin) || fmin<0 ) {
                cerr << "Parameter error: Expected positive integer after --fmin (-f)\n";
                usage( argv[0] );
                exit( EXIT_FAILURE );
            }
        } else if(!strcmp(argv[i],"-ibm")) {
          fileformat=FILE_FORMAT_IBM;
        } else {
          //C'est un fichier à charger
          file = argv[i];
        }
    }

    if( !file ) {
        cerr << "No input file\n";
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    is.open(file);
    if( fileformat==FILE_FORMAT_IBM ) {
    	f.read_ibm( is );
    } else {
    	f.read( is );
    }
    is.close();

#if DEBUG>1
    cout << "************\nlaunch SEQQSTREAM\n************\n\n";
#endif

	seqstream s(w,fmin);
	s.setstream(&f);
	s.launch();

#if DEBUG>1
    cout << "************\n launch PSSTREAM\n************\n\n";
#endif
	PSStream sm(w,fmin);
	sm.setstream(&f);
	sm.launch();


#if DEBUG>1
    cout << "********************\n COMPARE RESULTS\n********************\n\n";
    cout <<"en trop"<<endl;
#endif
    s.history().cmp(sm.history());
    cout <<"manque" << endl;
    sm.history().cmp(s.history());
#if DEBUG>1
    cout << "********************\n\n";
#endif

	ohs.open("history.txt");
	s.print_history(ohs);
	ohs.close();
	ohs.open("history_sm.txt");
	sm.print_history(ohs);
	ohs.close();

    return EXIT_SUCCESS;
}

/**
* \brief Fonction d'information sur l'usage du programme
*/
void usage(const char *comm)
{
  printf("*******************\n");
  printf("SeqStreamMiner comparison program\n\n");
  printf("Brief : Compare computing performance of algorithms SeqStream and PSStream \n");
  printf("Author : Guyet Thomas, AGROCAMPUS-OUEST\n");
  printf("Year : 2012\n");
  printf("*******************\n\n");
  printf("command : %s [-w %%d] [-f %%d] [-ibm] datafile\n\n", comm);
  printf("Options :\n");
  printf("\t --windows-size (-w) : windows size (ie the maximum length between events dates\n");
  printf("\t --fmin (-f) : frequency threshold\n");
  printf("\t -ibm : uses an ibm file format\n");
}
