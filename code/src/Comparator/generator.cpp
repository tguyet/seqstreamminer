/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>

#include "stream.h"
#include "StreamGenerator.h"

using namespace std;

void usage(const char *comm);

typedef enum {BERNOULLI, UNIFORM, POISSON} distribution;

int main(int argc, char **argv)
{
	ofstream os;
    stream f;
    int l =300, n =50;
    double p=0.1;
    distribution d = BERNOULLI;
    char *outputfile=NULL;

	for(int i=1;i<argc;i++)
	{
		if( !strcmp(argv[i],"-h") ) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		} else if(!strcmp(argv[i],"-l") || !strcmp(argv[i],"--length")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&l) || l<=0 ) {
				cerr << "Parameter error: Expected strictly positive integer after --length (-l)" <<endl;
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-p")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%lf",&p) ) {
				cerr << "Parameter error: Expected real number after -p" <<endl;
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-n")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&n) || n<=0 ) {
				cerr << "Parameter error: Expected strictly positive integer after -n" <<endl;
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if (!strcmp(argv[i],"-b")){
			d = BERNOULLI;
		} else if (!strcmp(argv[i],"-u")){
			d = UNIFORM;
		} else if (!strcmp(argv[i],"-f")){
			d = POISSON;
		} else if(!strcmp(argv[i],"-o")) {
			i++;
			outputfile = argv[i];
		}
	}

	switch(d) {
	case BERNOULLI:
		if( p<=0 || p>=1) {
			cerr << "Parameter error: Expected probability after -p, real number between 0 and 1, for Bernoulli distribution"<<endl;
			exit( EXIT_FAILURE );
		}
		f=StreamGenerator::bernoulli(l, n, p);
		break;
	case UNIFORM:
		f=StreamGenerator::generate(l, n);
		break;
	case POISSON:
		if( p<=0) {
			cerr << "Parameter error: Expected strictly positive value for -p, lambda parameters for Poisson distribution"<<endl;
			exit( EXIT_FAILURE );
		}
		f=StreamGenerator::poisson(l, n, p);
		break;
	default:
		f=StreamGenerator::bernoulli(l, n, p);
		break;
	}

	if( !outputfile ) {
		os.open("sequence.seq");
	} else {
		os.open( outputfile );
	}
	os << f;
	os.close();

    return EXIT_SUCCESS;
}

/**
* \brief Fonction d'information sur l'usage du programme
*/
void usage(const char *comm)
{
  printf("*******************\n");
  printf("Stream generator program\n\n");
  printf("Brief : Generates a sequence of itemsets\n");
  printf("Author : Guyet Thomas, AGROCAMPUS-OUEST\n");
  printf("Year : 2012\n");
  printf("*******************\n\n");
  printf("command : %s [-l %%d] [-n %%d] [-p %%d] [-b/u] [-o datafile]\n\n", comm);
  printf("Options :\n");
  printf("\t --length (-l) : sequence length (# of itemsets) (default 300)\n");
  printf("\t -n : vocabulary size, ie the number of different items (default 20)\n");
  printf("\t -p : probability (between 0 and 1) (default 0.1)\n");
  printf("\t -o : name of the generated file (default 'sequence.seq')\n");
  printf("\t -b : uses Bernouilli distribution for items, it generates a sequence of itemsets (default)\n");
  printf("\t -u : generates sequence of items with equiprobabilities between the n-th items\n");
  printf("\t -f : uses Poisson distribution for items, it generates a sequence of itemsets, -p defines the lambda parameters of the distribution\n");
  printf("\t -ibm : uses an ibm file format\n");
}
