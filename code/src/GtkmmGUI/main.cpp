/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */


#include <gtkmm.h>
#include <string>
#include "SequenceTreeView.h"
#include "HistoryManager.h"

/**
 * \brief SequenceHistoryTreeView example
 * \author T. Guyet, Inria
 * \date 25 nov 2021
 */
int main(int argc, char **argv)
{
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "");

	std::string filename;
	Gtk::Window window;

	window.set_title("Sequence history viewer");

	if( argc==1 ) {

		Gtk::FileChooserDialog *dialog=new Gtk::FileChooserDialog("Please choose a filer", Gtk::FILE_CHOOSER_ACTION_OPEN);

		dialog->add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
		dialog->add_button("Select", Gtk::RESPONSE_OK);

		Glib::RefPtr<Gtk::FileFilter> filter_any = Gtk::FileFilter::create();
		filter_any->set_name("Any files");
		filter_any->add_pattern("*");
		dialog->add_filter(filter_any);

		int result=dialog->run();

		if( result== Gtk::RESPONSE_OK ) {
			filename = dialog->get_filename();
			delete(dialog);
		} else {
			return 1;
		}
	} else {
		filename = argv[1];
	}

	SequenceHistoryTreeView *treeview=new SequenceHistoryTreeView();
	HistoryManager::HistoryTree *tree=HistoryManager::load(filename);

	if( !tree ) {
		std::cerr << "Tree loading error" << std::endl;
		return 1;
	}

	treeview->setHistory(tree);

	//Composition de la fenêtre avec un scoller
	Gtk::ScrolledWindow scroller;
	scroller.add( *treeview );
	window.add( scroller );
	window.show_all();

	window.set_size_request(400,300);

	//Shows the window and returns when it is closed.
	return app->run(window);
}
