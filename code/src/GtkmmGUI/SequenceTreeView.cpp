/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file SequenceHistoryTreeView.cpp
 * \author Guyet Thomas, Inria
 * \date 25 nov 2021
 */


#include <SequenceTreeView.h>

SequenceHistoryTreeView::SequenceHistoryTreeView() : Gtk::TreeView()
{
	m_refTreeModel = Gtk::TreeStore::create(m_Columns);
	set_model(m_refTreeModel);
	append_column("Pattern", m_Columns.m_col_pattern);
	append_column("Nb", m_Columns.m_col_nbnewpositions);
	append_column("Nb total", m_Columns.m_col_nbpositions);
	append_column("Positions", m_Columns.m_col_positions);

	set_headers_visible();
	set_headers_clickable(false);
}

SequenceHistoryTreeView::~SequenceHistoryTreeView()
{
}

/**
 * \brief Replot the history tree content from scratch
 */
void SequenceHistoryTreeView::updatetree()
{
	//Suppression de tout !
	m_refTreeModel->clear();

	addHistoryTreeNode(NULL,m_history->getroot());
}

/**
 * \brief Recursively add a nodes to the parentrow
 *
 * if parentrow is NULL the a new row is created
 */
void SequenceHistoryTreeView::addHistoryTreeNode(Gtk::TreeModel::Row *parentrow,  const HistoryManager::HistoryITNode *node)
{
	if(!node) return;

	Gtk::TreeModel::Row row;
	if(parentrow==NULL) {
		row = *(m_refTreeModel->append());
	} else {
		row = *(m_refTreeModel->append(parentrow->children()));
	}

	row[m_Columns.m_col_pattern]=patternToStr(node->_pattern);
	row[m_Columns.m_col_nbnewpositions] = node->_newPositions.size();
	row[m_Columns.m_col_nbpositions] = node->_positions.size();
	row[m_Columns.m_col_positions] = intervalsToStr(node->_intervals);

	list<HistoryManager::HistoryITNode*>::const_iterator itf = node->_S.begin();
	while(itf != node->_S.end()) {
		addHistoryTreeNode(&row, *itf);
		itf++;
	}
	itf = node->_C.begin();
	while(itf != node->_C.end()) {
		addHistoryTreeNode(&row, *itf);
		itf++;
	}
}

/**
 * \brief Construct the ustring of the pattern
 */
Glib::ustring SequenceHistoryTreeView::patternToStr(const list<itemset> &pattern) const
{
	Glib::ustring str;
	list<itemset>::const_iterator it=pattern.begin();
	while(it!=pattern.end()) {
		str+="(";
		itemset::const_iterator iti = (*it).begin();
		str+= Glib::ustring::compose("%1",*iti);
		iti++;
		while(iti!=(*it).end()) {
			str+= Glib::ustring::compose(",%1",*iti);
			iti++;
		}
		str+=")";
		it++;
	}
	return str;
}

/**
 * \brief Construct the ustring of the list of positions
 */
Glib::ustring SequenceHistoryTreeView::positionToStr(const list<int> &positions) const
{
	Glib::ustring str;
	if(positions.size()==0) return str;

	list<int>::const_iterator it=positions.begin();
	str+= Glib::ustring::compose("%1",*it);
	it++;
	while(it!=positions.end()) {
		str+= Glib::ustring::compose(",%1",*it);
		it++;
	}
	return str;
}

/**
 * \brief Construct the ustring of the list of intervals
 */
Glib::ustring SequenceHistoryTreeView::intervalsToStr(const list<HistoryManager::Interval> &intervals) const
{
	Glib::ustring str;
	if(intervals.size()==0) return str;

	list<HistoryManager::Interval>::const_iterator it=intervals.begin();
	str+= Glib::ustring::compose("[%1 %2]",(*it).debut,(*it).fin);
	it++;
	while(it!=intervals.end()) {
		str+= Glib::ustring::compose(", [%1 %2]",(*it).debut,(*it).fin);
		it++;
	}
	return str;
}



