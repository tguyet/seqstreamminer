/*
	Copyright (C) 2012 : Thomas Guyet
	All rights reserved.

 	This file is part of SeqStreamMiner.

    SeqStreamMiner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>

    Any feedback is very welcome.
    email: thomas guyet @ inria fr (remove space and add dots)
 */

/**
 * \file SequenceHistoryTreeView.h
 * \author Guyet Thomas, Inria
 * \date 25 nov 2021
 */


#ifndef SEQUENCETREEVIEW_H_INCLUDED
#define SEQUENCETREEVIEW_H_INCLUDED

#include <gtkmm.h>
#include <list>

#include "stream.h"
#include "HistoryManager.h"

using std::list;

/**
* \brief Frequent sequence tree widget
*/


class SequenceModelColumns : public Gtk::TreeModelColumnRecord
{
public:
	SequenceModelColumns() { add(m_col_pattern); add(m_col_positions); add(m_col_nbpositions); add(m_col_nbnewpositions); };
	virtual ~SequenceModelColumns(){};
	Gtk::TreeModelColumn<Glib::ustring> m_col_pattern;
	Gtk::TreeModelColumn<Glib::ustring> m_col_positions;
	Gtk::TreeModelColumn<unsigned int> m_col_nbnewpositions;
	Gtk::TreeModelColumn<unsigned int> m_col_nbpositions;
};


class SequenceHistoryTreeView : public Gtk::TreeView {

protected:
	SequenceModelColumns m_Columns;
	Glib::RefPtr<Gtk::TreeStore> m_refTreeModel;
	HistoryManager::HistoryTree *m_history;
public:
	SequenceHistoryTreeView();
	virtual ~SequenceHistoryTreeView();

	void setHistory(HistoryManager::HistoryTree *history) {
		 m_history=history;
		 updatetree();
	}
protected:
	void updatetree();
	void addHistoryTreeNode(Gtk::TreeModel::Row *parentrow,  const HistoryManager::HistoryITNode *node);
	Glib::ustring patternToStr(const list<itemset> &pattern) const;
	Glib::ustring positionToStr(const list<int> &positions) const;
	Glib::ustring intervalsToStr(const list<HistoryManager::Interval> &intervals) const;
};

#endif
