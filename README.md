# IncSeq

Author: Thomas Guyet <thomas.guyet@inria.fr>

## What is IncSeq
IncSeq is a software that implements the IncSeq algorithm. The algorithm extracts frequent patterns from a sliding windows over a stream of itemsets.
An history of the frequent itemsets is maintain in order to sum up the overall stream.

## Installation

### Requirements

- g++ or another C++ compiler (Linux, Mac or Windows)
- CMake
- make
- eventually openmp for distributed implementation
- eventually Gtkmm3 for GUI bunch
- eventually R (statistical software)
- eventually doxygen (for documentation generation)

###  How to compile and install

IncSeq uses CMake autoconfiguration tool. CMake enables cross-plateform compilation routines. For more details, see [http://www.cmake.org].

Under Linux:
```
mkdir build
cd build
cmake ..
make
```

### Compilation options
While generating the `Makefile`, it is possible to setup some options. The option are some code optimisations that can be activated or not. 
Activation is at the compilation process to have very efficient optimisations (and make them fairly comparable).

See `config.h.cmake` for compiling options. 

To modify compiling options :
- modify the `config.h.make` file
- run `cmake ..` from the build directory
- run `make clean` then `make`

See although different compilation options :
- use "cmake -D OPENMP_ENABLE=ON .." to enable compilation with **openmp** (parallelized implementation)

## Documentation
See `doc` for technical documentation. 
If the repository doesn't exists, the documentation is automatically generated using *doxygen*



